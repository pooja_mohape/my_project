<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Latest News
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php 
                       $attributes = array("method" => "post", "id" => "frmnews", "name" => "frmnews");
                       echo form_open_multipart('home/add_section',$attributes);?>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Heading</label>
                        <div class="col-lg-4">                           
                            <input name="heading" type="text" id="heading" class="form-control" placeholder="Enter Heading">
                               
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">

                                <input name="content" type="text" id="content" class="form-control" placeholder="Enter Content">
                            </div>
                        </div>   
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Image</label>
                            <div class="col-lg-4">
                               
                              <input type="file" name="userfile" size="20"/>
                            </div>
                        </div>  
                        <br><br>        
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Save</button> 
                               
                                  <a href="<?php echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         </form>
                         <br><br>
                         <div style="overflow-x:auto;">
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Heading</th>
                                            <th>Image</th>
                                            <th>Text</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; foreach ($response as $key => $value) {?>
                        <tr>
                            <td><?php echo $i;?></td>
                           <td><?php echo $value->title;?></td>
                           <td><img src="<?php echo base_url();?>uploads/<?php echo $value->img;?>" style="width:70px; height: 60px;"></td>
                           <td><?php echo $value->txt;?></td>
                            <td><a href="<?php echo base_url(); ?>home/delete_section/<?php echo $value->id;?>">Delete</a>|<a href="<?php echo base_url();?>home/update_section/<?php echo $value->id;?>">Update</a></td>
                        <tr>
                        <?php $i++;} ?>
                                     </tbody>
                                </table>
                               </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
          

<script type="text/javascript">
    $(document).ready(function(){
        $('#frmnews').validate({
            errorClass: 'errors',
            rules:{
                heading:{
                    required:true
                },
                content:{
                    required:true
                },
                userfile:{
                    required:true
                }
            },
            messages:{
                heading:{
                    required:"Please Enter Heading Of News"
                },
                content:{
                    required:"Please Enter News Content"
                },
                userfile:{
                    required:"Please Select Image"
                }
            }
        });
    });
</script>