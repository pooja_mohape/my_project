<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Edit Shortlist Data
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php echo form_open_multipart('home/update_shortlist');?>

                        <input type="hidden" name="id" value="<?php echo $data[0]->id;?>">

                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                        <div class="col-lg-4">                           
                            <input name="title" type="text" id="title" class="form-control"  placeholder="Enter Title" value="<?php echo $data[0]->title;?>" required="">
                               
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">

                                
                                <textarea name="content" type="text" id="content" class="form-control" rows="3" value="" required=""  placeholder="Enter Content"><?php echo $data[0]->content;?></textarea>

                            </div>
                            
                        </div>   
                               
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Update</button> 
                               
                                  <a href="<?php echo base_url().'home/shortlist_data'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         </form>
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->


