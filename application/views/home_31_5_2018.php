<?php  $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/slider');?>
<!-- <?php //$this->load->view('Award _categories/people_categories');?> -->

<section class="section background-color-light border-0 my-0" id="logo_section">
		<div class="container">
			<div class="row appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1300">
				<div class="col">
					<div class="owl-carousel owl-theme stage-margin custom-carousel-style-1 mb-0" data-plugin-options="{'items': 5, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
						<?php foreach ($logo as $key => $value) {?>
							<div>
							<img class="img-fluid rounded mx-auto d-block" src="<?php echo base_url();?>/uploads/logo/<?php echo $value->img;?>" style="/*width:198px;*/ width: auto; height:73px; margin-right:10px;">
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<hr class="solid custom-opacity-2 mt-0">
	<section class="section background-color-light custom-padding-3 border-0 my-0" id="award_section">
		<div class="container">
			<?php foreach ($award as $key => $value) {?>
			<div class="row text-center">
				
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter"><?php echo $value->heading?></h2>
					<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200"><?php echo $value->title;?></p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400"><?php echo $value->paragraph;?></p>
				</div>
				
			</div>
			<?php } ?>
		</div>
	</section>
	<section id="counter_section">
		<div class="container">
			<hr style=" border:1px solid #fff">
			<div class="row text-center">
				<div class="col">			
				<div class="row counters">
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->No_of_Event) ? $counters[0]->No_of_Event : '';?>">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">No.of<br>Event</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->successful_event) ? $counters[0]->successful_event : '';?>" data-append="%" data-append="%">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful Event<br>Ratio</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->successful_years) ? $counters[0]->successful_years : '';?>" data-append="+">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful<br>Years</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->dedicated_client) ? $counters[0]->dedicated_client : '';?>">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Clients</label>
						</div>
					</div>
				</div>
				</div>
			</div>
			<hr style=" border:1px solid #fff">
		</div>
	</section>
	<section class="section background-color-light custom-padding-3 border-0 my-0" id="award_methodology">
		<div class="container">
			<div class="row">
			 <?php $c=0; foreach ($block as $key => $value) {?>
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter"><?php echo $value->title;?></h2>
					<div class="align-self-center custom-primary-font custom-fontsize-2 font-weight-normal appear-animation animated fadeInUpShorter appear-animation-visible">
						<p><?php echo $value->content; ?></p>
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</section>
	<section class="section background-color-quaternary custom-padding-3 border-0 my-0" id="winners_list">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					<h2 class="align-self-center custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">Winners List</h2>
					<div class="row align-items-center">
						<div class="col-lg-2 mb-4 mb-lg-0">
							<div class="tabs tabs-vertical tabs-left tabs-navigation custom-tabs-navigation-1 border-0 mb-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<ul class="nav nav-tabs col-sm-3 sampurna">
							 <?php foreach($year as $yer){?>
									<li class="nav-item mb-0 year" pgt1="<?php echo $yer->year;?>" style="cursor: pointer;">
										<a class="nav-link custom-primary-font text-center"  data-toggle="tab"><?php echo $yer->year;?></a>
									</li>
									<?php }?>
								</ul>
							</div>
						</div>
						<div class="col-lg-10 mb-4 mb-lg-0">
							<div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabsNavigation1">
								<div class="row align-items-center" id="tabsNavigation">
									<?php foreach($winner_list as $list){?>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">

												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/uploads/<?php echo $list->img;?>" class="rounded-circle img-center avatar" alt=""/>
													</div>
													<div class="col-lg-9">
														<blockquote >
															<p class="align-self-center mb-3 img-center"><?php echo substr($list->content, 0,150);?></p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0"><?php echo $list->name;?></strong>
														</div>
													</div>
												</div>
											</div>
											
										</div>	
									</div>
								<?php }?>
									
								</div>

							</div>
								   <div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabs_nav" style="display:none;">
								   </div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group col-md-12 col-md-offset-6">
				<input type="button" value="View All" class="btn btn-secondary custom-btn-style-1 text-uppercase img-center all_view"
				data-toggle="modal" pgt="<?php echo $winner_list[0]->year;?>">
			</div>
		</div>
	</section>	
<!--  model5 start -->
<div class="modal fade" id="myModal5" role="dialog">
 <?php if(count($year) > 1){?>
<div class="paddle previous all_view all1" pgt="">
    <a role="button" tabindex="0">
        <div class="headline">
            <div>
                <div class="truncate previous1"></div>
            </div>
        </div>
    </a>
</div>
<?php }if(count($year) > 1){ ?>
<div class="paddle next all2 all_view" pgt="">
    <a role="button" tabindex="0">
        
        <div class="headline ">
            <div>
                <div class="truncate next1"></div>
            </div>
        </div>
    </a>
</div>
<?php }?>
<div class="modal-dialog" id="list-modal">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title winlist"></h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="row" id="test1">
                
            </div>
        </div>
        <div class="modal-footer">
            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->

	<section class="section background-color-light custom-padding-1 border-0 my-0" id="awards_shortlist">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter" style="color: green">Awards Shortlist</h2>
					<div class="owl-carousel owl-theme stop" data-ride="carousel" id="demo-carousel" data-plugin-options="{'items': 4, 'autoplay': true, 'autoplayTimeout': 3000}">
						<?php $c=0; foreach ($shortlist as $key => $value) {?>
						<div class="abc">
							<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0"><?php echo $value->title;?></h2>

							<div class="feature-box-info pl-0 pr-3 pqr_data">
								<p class="align-self-center mb-3" ><?php echo substr($value->content,0,250);?></p>
								<?php if(strlen($value->content)>250) {?>
								<button type="button" class="btn btn-link pull-right view_more_data" id="view" pgt="<?php echo $value->content;?>">View more <i class="fas fa-long-arrow-alt-right"></i></button>
								<?php }?>
							</div>
							<div class="zmr_data" id="zmr" style="display: none;">
	                   		
		                    <p class="align-self-center mb-3"><?php echo $value->content;?></p> 
		                    <button type="button" class="btn btn-link pull-right view_less_data">Less<i class="fas fa-long-arrow-alt-top"></i></button>
                		
          			</div>
				</div>
					<?php }?>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="parallax section section-parallax parallax-background custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" id="contact">
	<div id="googleMap" style="position: relative;overflow: hidden;position: absolute;top: 0px;left: 0px;width: 100%;height: 120%;transform: translate3d(0px, -62.5402px, 0px);background-position-x: 50%;">
	</div>
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-md-9 col-lg-7 col-xl-6" id="disp">
				<div class="card shadow background-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
						<!-- <div class="modal-header con"> Get Touch us On MAP
       				 	<button type="button" class="close" id="button">-</button>
    					</div> -->
    				<div id="cont">
					<form action="<?php echo base_url();?>home/contact_us" method="post" id="box">
						<div class="card-body p-5">
							<h2 class="custom-primary-font font-weight-normal text-6 line-height-sm mb-3">Contact Us</h2>
							<div class="row">
								<div class="col-md-6"><p class="pb-2 mb-4"><?php echo $contact_us[0]->address;?></p></div>		
								<div class="col-md-6 mb-4">
									<button type="button" class="btn btn-primary text-uppercase custom-btn-style-1 shadow card" data-toggle="modal" data-target="#venue_info">Travel Info</button>
								</div>
							</div>
			
								<div class="input-group mb-3">
									<div class="input-group-prepend">
									    <span class="input-group-text"><i class="fas fa-user"></i></span>
									</div>
									  <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Name" aria-label="Username" required>
								</div>

								<div class="input-group mb-3">
									<div class="input-group-prepend">
								    	<span class="input-group-text"><i class="fas fa-envelope"></i></span>
									</div>
									  <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email" aria-label="Email" required>
								</div>

								<div class="input-group mb-3">
									<div class="input-group-prepend">
									    <span class="input-group-text"><i class="fas fa-edit"></i></span>
									</div>
									  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
								</div>

								<div class="input-group">
									<div class="input-group-prepend">
									    <span class="input-group-text"><i class="fas fa-comment-alt"></i></span>
									</div>
									  <textarea class="form-control" aria-label="With textarea" name="message"  id="message" placeholder="Write Your Message" required></textarea>
								</div>
							
								<br>
								<div class="background-color-light border-0">
									  <button type="submit" id="send" class="btn btn-outline-primary rounded pull-right mb-2 shadow">Send</button>
								</div>
						</div>
					<!-- form -->
						<div class="card-footer background-color-primary border-0 custom-padding-2">
							<div class="row">
								<div class="col-md-6 mb-5 mb-md-0">
									<div class="feature-box align-items-center">
										<div class="feature-box-icon background-color-tertiary">
											<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
										</div>
										<div class="feature-box-info">
											<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
											<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->email;?></a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="feature-box align-items-center">
										<div class="feature-box-icon background-color-tertiary">
											<i class="fas fa-phone text-3"></i>
										</div>
										<div class="feature-box-info">
											<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
											<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->call;?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal-header con"> Get Touch us On MAP
    <button type="button" class="close" id="button">-</button>
</div>
<div class="card-footer background-color-primary border-0 custom-padding-2" id="map_footer" style="display: none;">
<div class="row">
	<div class="col-md-6 mb-5 mb-md-0">
		<div class="feature-box align-items-center">
			<div class="feature-box-icon background-color-tertiary">
				<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
			</div>
			<div class="feature-box-info">
				<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
				<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->email;?></a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="feature-box align-items-center">
			<div class="feature-box-icon background-color-tertiary">
				<i class="fas fa-phone text-3"></i>
			</div>
			<div class="feature-box-info">
				<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
				<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->call;?></a>
			</div>
		</div>
	</div>
</div>
</div>


<!-- cordinate for contact_page-->
<input type="hidden" name="" id="contact_lng" value="<?php echo isset($mod1[0]->longitude) ? $mod1[0]->longitude :'';?>">
<input type="hidden" name="" id="contact_lat" value="<?php echo isset($mod1[0]->latitude) ? $mod1[0]->latitude : '';?>">

<section class="section background-color-quaternary custom-padding-3 border-0 my-0" id="latest_news">
	<div class="container">
		<div class="row mb-4">
			<div class="col">
				<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-0 appear-animation" data-appear-animation="fadeInUpShorter">LATEST NEWS</h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php foreach ($data as $key => $value) {?>
				<div class="col-md-6 col-lg-4 mb-5 mb-lg--1 xyz">
					<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation card shadow" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
						<div class="thumb-info-wrapper m-1">
							<img src="<?php echo base_url();?>uploads/<?php echo $value->img;?>" class="img-fluid" alt="" style="width:390px; height: 180px;">
						</div>
					<div class="pqr">
						<div class="thumb-info-caption custom-padding-4 d-block cls_hd visible_text" id="visible_text" >
							<span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo $value->title;?></span>
							<span class="thumb-info-caption-text line-height-md text-3 p-0 m-0"><?php echo substr($value->txt,0,21);?>  
							<?php if(strlen($value->txt)>21) {?>
							<button type="button" class="btn btn-secondary custom-btn-style-1 view_more" id="view" pgt="<?php echo $value->txt;?>">view more</button></span>
							<?php }?>
						</div>
					</div>

					<div class="zmr" id="zmr" style="display: none;">
	                   	<div class="thumb-info-caption custom-padding-4 d-block hidden_text" style="">
		                    <span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo $value->title;?></span>
		                    <span class="thumb-info-caption-text line-height-md text-3 p-0 m-0"><?php echo $value->txt;?></span>
		                    <center>
		                    <button type="button" class="btn btn-secondary custom-btn-style-1 view_less">less</button></center>
                		</div>
          			</div> 

					</article>
				</div>
				<?php } ?>
			</div>
			<div class="row text-center mt-5">
				<div class="col">
					<a href="#" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-2">VIEW ALL</a>
				</div>
			</div>
		</div>
</section>
	

<!-- modal start for award Category-->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<!--1st right Side Click(project) -->

		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal1s">
				<div class="headline">
					<div>
						<div class="truncate text-uppercase">Project Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
				<?php foreach ($people_cat as $key => $people_cat) {?> 
				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $people_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
							<?php echo $people_cat->content;?>
							
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!-- php end here -->
				<?php }?>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal1" role="dialog" style="overflow-y: auto;">
		<!--2nd right Side Click(company) -->

		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal2s">
				<div class="headline">
					<div>
						<div class="truncate text-uppercase">company Category</div>
					</div>
				</div>
			</a>
		</div>

		<!--1st left Side Click(people) -->

		<div class="paddle previous">
			<a role="button" tabindex="0" id="myModal3s">

				<div class="headline ">
					<div>
						<div class="truncate text-uppercase">People Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
			<?php foreach ($proj_cat as $key => $proj_cat) {?>
				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $proj_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
							<?php echo $proj_cat->content;?>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
	<!--Modal-->
	<div class="modal fade" id="myModal2" role="dialog" style="overflow-y: auto;">
		<!--3rd right Side Click(people) -->


		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal4s">

				<div class="headline">
					<div>
						<div class="truncate text-uppercase">People Category</div>
					</div>
				</div>
			</a>
		</div>

		<!--2nd left Side Click(project) -->


		<div class="paddle previous">
			<a role="button" tabindex="0" id="myModal5s">

				<div class="headline">
					<div>
						<div class="truncate text-uppercase">Project Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
			<?php foreach ($company_cat as $key => $company_cat) {?>
				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $company_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
							<?php echo $company_cat->content;?>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>


	<!-- modal start  for venue_info -->
<div class="modal fade" id="venue_info" role="dialog">
	<div class="modal-dialog"> 
		<!-- Modal content-->
		<div class="modal-content">
		<?php foreach ($traval_info as $key => $traval_info) {?>
			<div class="modal-header">
				<h4 class="modal-title"><?php echo $traval_info->title;?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				
				<?php echo $traval_info->content;?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			<?php }?>
		</div>
	</div>
</div>
<!-- modal end for venue_info -->

	<!-- modal for nomination form -->
<!-- nomi Modal -->
<input type="hidden" name="" id="modal_map_lng" value="<?php echo isset($mod2[0]->longitude) ? $mod2[0]->longitude :'';?>">
<input type="hidden" name="" id="modal_map_lat" value="<?php echo isset($mod2[0]->latitude) ? $mod2[0]->latitude : '';?>">



<!-- modal for nomination form -->
	<!-- nomi Modal -->
	<div class="modal fade" id="nomi" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><span style="color:#013975;"><?php echo $mod1[0]->title; ?></span></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<h4 style="color:#007bff"><?php echo $mod1[0]->heading;?></h4>

					<div class="appear-animation animated fadeInUpShorter appear-animation-visible">
						<div class="shade">
							<div id="mygMap" style="width:100%;height:200px;"></div>
						</div>	
					</div>
					<br>
					<div class="shade">
						<div class="row mx-2 appear-animation animated fadeInUpShorter appear-animation-visible">
							<div class="col text-center vertical-align">
								<i class="fas fa-map-marker fa-5x"></i>
							</div>
							<div class="text-center col-sm-10 col-md-10 mt-4">
								 
									<p><?php echo $mod1[0]->content;?></p>
									<p><?php echo $mod1[0]->nomination_content;?> <a href="#">here</a></p>
								
							</div>
						</div>
						<div class="card-footer background-color-primary border-0">
						<div class="row">
							<div class="col-sm-5 mb-0 py-2">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
										<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2"><?php echo $mod1[0]->email;?></a>
									</div>
								</div>
							</div>
							<div class="col-sm-5 mb-0 py-2">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="fas fa-phone text-3"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
										<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2"><?php echo $mod1[0]->phone;?></a>
									</div>
								</div>
							</div>
							<div class="col-md-2 mb-0 py-2" style="position: relative; text-align: center;">
								<button type="button" id="nomi_rule" class="btn btn-outline-light rounded" data-target="#rule_nom" data-toggle="modal">Next</button>
							</div>
						</div>
					</div>
					</div>
				</div>
<!-- <div class="modal-footer">
<button type="button" id="nomi_rule" class="btn btn-success pull-left" data-target="#rule_nom" data-toggle="modal">NEXT</button>
</div> -->
</div>
</div>
</div>
<!-- nomi_f form -->
<div class="modal fade" id="rule_nom" role="dialog" style="overflow-y:auto;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><strong><?php echo $mod2[0]->title; ?></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="shade">
						<div class="p-3">
							<p><span><?php echo $mod2[0]->heading;?></span></p>

							<ul class="custom-primary-font appear-animation animated fadeInUpShorter appear-animation-visible">
								<?php foreach ($mod2 as $key) {?>
									<li><?php echo $key->content;?></li>
								<?php	
								}?>
							</ul>

							<p><span><a href="#"></a><?php echo $mod2[0]->nomination_content;?></span></p>
						</div>
						<div class="modal-footer" style="background-color: #1b9dff;">
							<button type="button" id="nomi_detail" class="btn btn-outline-light rounded"  data-target="#nomi_form" data-toggle="modal">Start Fill Details</button>
						</div>
					</div>
				</div>

				<!-- <div class="modal-footer" style="background-color: #1b9dff;">
					<button type="button" id="nomi_detail" class="btn btn-outline-light rounded"  data-target="#nomi_form" data-toggle="modal">Start Fill Details</button> -->

					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				<!-- </div> -->
			</div>
		</div>
	</div>
</div>
<!-- nomi details -->
<div class="modal fade" id="nomi_form" role="dialog" style="overflow-y:auto;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nomination Form</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body card-body">
				<div class="appear-animation animated fadeInUpShorter appear-animation-visible">
					<form id="formRender" action="<?= base_url(); ?>survey/formdata" method="post" enctype="application/json">
			
					</form>
				</div>
			</div>

			<!-- <div class="modal-footer">
				 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
			</div> -->
		</div>
	</div>
</div>
<!-- modal for statistics -->
<div class="modal fade" id="statistics" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">statistics View</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row text-center">		
					<div class="row counters">
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="28">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">No.of<br>Event</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="99" data-append="%">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Successful Event<br>Ratio</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-sm-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="18" data-append="+">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Successful<br>Years</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="1200">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Clients</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- modal end for nomination form -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="http://formbuilder.online/assets/js/form-builder.min.js"></script>

<!-- Fill Survey Form -->
		
<script type="text/javascript">
	
	var jsonData = <?php echo $dt['0']->questionary?>;
	var survey_id = <?php echo $dt['0']->id?>;
	//alert(jsonData+' '+survey_id);
</script>

<!-- <script type="text/javascript">
    $(document).ready(function(){
        var owl = $('.stop');
         owl.owlCarousel({
             items:4,
   loop:true,
   margin:10,
   autoplay:true,
   autoplayTimeout:3000,
   autoplayHoverPause:true
         });
        $('.view_more_data').click(function(){
            owl.trigger('stop.owl.autoplay');
        });

        $('.view_less_data').click(function(){
            owl.trigger('play.owl.autoplay');
        });
    });
</script> -->



<!-- 
<style type="text/css">
	@media (max-width:2560px){
	.con{
		display:none;
	}
}

@media (max-width:991px){
	.con{
		display:block;
	}
}
</style> -->
<script type="text/javascript">
// 	$("#button").click(function(){
//     if($(this).html() == "-"){
//         $(this).html("+");
//     }
//     else{
//         $(this).html("-");
//     }
//     $("#box").slideToggle();
// });
</script>