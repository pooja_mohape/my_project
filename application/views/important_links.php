<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Links
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div style="overflow-x:auto;">
                             <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;Award categories
                                </h3>     
                            </section>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; foreach ($awards as $key => $value) {?>
                        <tr>

                            <td><?php echo $i;?></td>
                           <td><?php echo $value->category;?></td>
                            <td><a class="btn btn-info" href="<?php echo base_url();?>home/update_awardcat/<?php echo $value->id;?>" title="Update"><i class="fa fa-edit"></i></a></td>
                        <tr>
                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            <div style="overflow-x:auto;">
                             <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;Quick Links
                                </h3>     
                            </section>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Quick Links</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; foreach ($links as $key => $value) {?>
                        <tr>

                            <td><?php echo $i;?></td>
                           <td><?php echo $value->quick_links;?></td>
                            <td><a class="btn btn-info" href="<?php echo base_url();?>home/links/<?php echo $value->id;?>" title="Update"><i class="fa fa-edit"></i></a></td>
                        <tr>
                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script>