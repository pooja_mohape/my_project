<!-- <style type="text/css">
  table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style> -->
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Add Category
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                         <br><br>
                         <div style="overflow-x:auto;">
                        <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;People Category
                                </h3>     
                            </section>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1; foreach ($people_cat as $key => $cat) {
                                        ?>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo $cat->title;?></td>
                                      <td class="content"><?php echo $cat->content;?></td>
                                      <td><a class="btn btn-info" href="<?php echo base_url();?>home/edit_people_category/<?php echo $cat->id;?>" title="Edit"><i class="fa fa-edit"></i></a></td>
                                     <?php $i++;}?>
                                    </tbody>
                                </table>
                               </div>
                               <br><br>

                         <div style="overflow-x:auto;">
                              <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;Project Category
                                </h3>     
                              </section>
                                <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1; foreach ($proj_cat as $key => $cat) {
                                        ?>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo $cat->title;?></td>
                                      <td><?php echo $cat->content;?></td>
                                      <td><a class="btn btn-info" href="<?php echo base_url();?>home/edit_project_category/<?php echo $cat->id;?>" title="Edit"><i class="fa fa-edit"></i></a></td>
                                     <?php $i++;}?>
                                    </tbody>
                                </table>
                            </div>

                            
                           <div style="overflow-x:auto;">
                            <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;company Category
                                </h3>     
                            </section>
                                <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1; foreach ($company_cat as $key => $cat) {
                                        ?>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo $cat->title;?></td>
                                      <td><?php echo $cat->content;?></td>
                                      <td><a class="btn btn-info" href="<?php echo base_url();?>home/edit_company_category/<?php echo $cat->id;?>" title="Edit"><i class="fa fa-edit"></i></a></td>
                                     <?php $i++;}?>
                                    </tbody>
                                </table>
                          </div> 

                           <div style="overflow-x:auto;">
                        <section class="content-header">
                                <h3 style="margin-left:15px;">
                                    &nbsp;Traval Info
                                </h3>     
                            </section>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i=1; foreach ($traval_info as $key => $cat) {
                                        ?>
                                      <td><?php echo $i;?></td>
                                      <td><?php echo $cat->title;?></td>
                                      <td><?php echo $cat->content;?></td>
                                      <td><a class="btn btn-info" href="<?php echo base_url();?>home/edit_traval_info/<?php echo $cat->id; ?>" title="Edit"><i class="fa fa-edit"></i></a></td>
                                     <?php $i++;}?>
                                    </tbody>
                                </table>
                               </div>
                    </div>
                </div>
            </div>
        </div>
    </section>