
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >       
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Update Nomination
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php 
                        $attributes = array("method" => "POST", "id" => "nomination", "name" => "nomination", "class" => "form-group");
                       echo form_open_multipart('home/nomination_update',$attributes);?>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                            <div class="col-lg-4">
                               <input type="text" id="title" name="title" class="form-control" placeholder="Enter title" value="<?php echo $data[0]->title?>" required=""> 
                            </div>
                        </div>  

                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Heading</label>
                        <div class="col-lg-4">                           
                            <input type="text" id="heading" name="heading" class="form-control" placeholder="Enter Heading" value="<?php echo $data[0]->heading?>" required="">
                               
                        </div>
                    </div>   
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Email</label>
                            <div class="col-lg-4">

                                <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email" value="<?php echo $data[0]->email?>" required="">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Phone</label>
                            <div class="col-lg-4">

                                <input type="text" id="phone" name="phone" class="form-control" placeholder="Enter Phone" value="<?php echo $data[0]->phone?>" required>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $data[0]->id;?>">
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Latitude</label>
                            <div class="col-lg-4">

                                <input type="text" id="latitude" name="latitude" class="form-control" placeholder="Enter Latitude" value="<?php echo $data[0]->latitude?>" required="">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Longitude</label>
                            <div class="col-lg-4">

                                <input type="text" id="longitude" name="longitude" class="form-control" placeholder="Enter Longitude" value="<?php echo $data[0]->longitude?>" required="">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">
                               <textarea id="article" name="article" rows="8" class="form-control"><?php echo $data[0]->content;?></textarea>
                            </div>
                        </div>  
                        <br><br>        
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" value="Upload" type="submit">Update</button> 
                               
                                  <a href="<?php echo base_url().'home/nomination'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>
                         <?php echo form_close();?> 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="<?php echo base_url();?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: "link image"
         });
          
        </script>