<?php

$data=$this->db->get('awards')->result();


 ?>
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Awards
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php 
                        $attributes = array("method" => "POST", "id" => "award_form", "name" => "award_form", "class" => "form-group");
                       echo form_open_multipart('home/add_award',$attributes);?>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Heading</label>
                        <div class="col-lg-4">                           
                            <input name="heading" type="text" id="heading" class="form-control" placeholder="Enter Heading" required="">
                               
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                            <div class="col-lg-4">

                                <input name="title" type="text" id="title" class="form-control" placeholder="Enter Title" required="">
                            </div>
                        </div>   
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Paragraph</label>
                            <div class="col-lg-4">
                               
                              <textarea name="paragraph" id="paragraph" placeholder="Enter Paragraph" rows="4" cols="35"></textarea>
                            </div>
                        </div>  
                        <br><br>        
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" value="Upload" type="submit">Save</button> 
                               
                                  <a href="<?php echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         <?php echo form_close();?>
                         <br><br>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Heading</th>
                                            <th>Title</th>
                                            <th>Paragraph</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; 
                                        foreach ($data as $key => $value) {?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->heading;?></td>
                                           <td><?php echo $value->title;?></td>
                                           <td><?php echo $value->paragraph;?></td>
                                            <td><a class="btn btn-danger" onclick="checkDelete()" href="<?php echo base_url(); ?>home/delete_awards/<?php echo $value->id;?>" title="Delete"><span class="glyphicon glyphicon-trash"></span></a></a>|<a class="btn btn-info" href="<?php echo base_url();?>home/update_awards/<?php echo $value->id;?>" title="Update"><i class="fa fa-edit"></i></a></td>
                                        <tr>
                                        <?php $i++;} ?>
                                     </tbody>
                                </table>
                               
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
          

<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script>          
<script type="text/javascript">
    $(document).ready(function(){
        $('#award_form').validate({
            errorClass: 'errors',
            rules:{
                heading:{
                    required:true
                },
                title:{
                    required:true
                },
                paragraph:{
                    required:true
                }
            },
            messages:{
                heading:{
                    required:'Please Enter Heading'
                },
                title:{
                    required:'Please Enter Title'
                },
                paragraph:{
                    required:'Please Enter paragraph'
                }
            }
        });
    });
</script>