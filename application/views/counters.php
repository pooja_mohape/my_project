<div class="box" style="display: none;">
	<div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
		  
        </div>
	</div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Counters
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/counters/save_counters', $attributes);
                       ?>
                       

                         <div class="form-group">

                            <label class="col-lg-3 control-label" for="name"></label>
                            <div class="col-lg-4">
                                <button class="btn btn-info pull-right" id="update_data" type="button">Edit</button> 
                            </div>

                            <div class="col-lg-4">
                            <a href="<?php echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> 
                            </a>
                            </div>

                        </div>
                                                  <div class="clearfix" style="height: 10px;clear: both;"></div>

                          <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">No.of
Event</label>
                            <div class="col-lg-4">

                               
                                
                                <?php if(!$exist) {?>
                                <input name="no_of_event" type="number" id="No_of_Event" class="form-control" value="" required="" min="0">
                                <?php }else{?>
                                <input type="hidden" name="id" value="<?php echo $counters[0]->id?>">
                                <input name="no_of_event" type="number" id="No_of_Event" class="form-control" value="<?php echo $counters[0]->No_of_Event?>" required="" min="0" disabled >
                                   <?php } ?>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Successful Event
Ratio</label>
                            <div class="col-lg-4">
                              <?php if(!$exist) {?>
                                <input name="successful_event" type="number" id="successful_event" class="form-control" value="" required="" min="0">
                               <?php }else{?>
                                <input name="successful_event" type="number" id="successful_event" class="form-control" value="<?php echo $counters[0]->successful_event?>" required="" min="0" disabled="">
                               <?php } ?>
                            </div>
                        </div>   
                         <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Successful
Years</label>
                            <div class="col-lg-4">
                                <?php if(!$exist) {?>
                                <input name="successful_years" type="number" id="successful_years" class="form-control" value="" required="" min="0">
                                <?php }else{?>
                                 <input name="successful_years" type="number" id="successful_years" class="form-control" value="<?php echo $counters[0]->successful_years?>" required="" min="0" disabled>
                                <?php }?>

                               
                            </div>
                        </div> 
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Dedicated
Clients</label>
                            <div class="col-lg-4">
                                <?php if(!$exist) {?>
                                <input name="dedicated_client" type="number" id="dedicated_client" class="form-control" value="" required="" min="0">
                                <?php }else{?>
                                <input name="dedicated_client" type="number" id="dedicated_client" class="form-control dedicated_client" value="<?php echo $counters[0]->dedicated_client?>" required="" min="0" disabled>
                                <?php }?>
                            </div>
                        </div>                   
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <?php if(!$exist){?>
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <?php }?>   
                                  <button class="btn btn-info" id="update_group_data" name="save_group_data" type="submit" style="display: none;">Update</button> 
                                  <!-- <a href="<?php //echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a> -->
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->


<script type="text/javascript">

    $(document).ready(function() { 
        $("#update_data").click(function(){
            $('#No_of_Event').attr('disabled',false);
            $('#successful_years').attr('disabled',false);
            $('#successful_event').attr('disabled',false);
            $('#dedicated_client').attr('disabled',false);
            $('#update_group_data').show();
       }); 
    });
</script>

                <!-- end:content -->
          
