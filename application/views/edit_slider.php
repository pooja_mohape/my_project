<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Edit Slider
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php echo form_open_multipart('home/update');?>

                        <input type="hidden" name="id" value="<?php echo $data[0]->id;?>">

                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                        <div class="col-lg-4">                           
                            <input name="heading" type="text" id="heading" class="form-control" value="<?php echo $data[0]->title;?>" placeholder="Enter Title">
                               
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">
                                <textarea name="content" type="text" id="content" class="form-control"  placeholder="Enter Content" rows="3" value=""  maxlength="250"><?php echo $data[0]->content;?></textarea>
                            </div>

                            
                        </div> 

                         <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Button Name</label>
                        <div class="col-lg-4">                           
                            <input name="btn" type="text" id="btn" class="form-control" placeholder="Enter Button Name" value="<?php echo $data[0]->button_name;?>">
                               
                        </div>
                    </div>  
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Image</label>
                            <div class="col-lg-4">
                             <label><img src="<?php echo base_url().'uploads/'. $data[0]->img;?>" width="150" height="100" >
                             <label><?php echo $data[0]->img;?></label>
                             </label>
                              <input type="file" name="userfile" placeholder="Select Image" size="20" />
                            </div>
                        </div>          
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Update</button> 
                               
                                  <a href="<?php echo base_url().'home/slider'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         </form>
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->


