<!-- start:left sidebar -->
<?php

$this->db->select('*');
$this->db->from('section_master');
$section_master = $this->db->get()->result();
$ses_uid        = $this->session->userdata('username');
?>

        <aside class="left-side sidebar-offcanvas">

            <section class="sidebar">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img alt="" src="<?php echo base_url();?>assets/newasset/image/profile.png" class="img-circle" height="50px" width="50px">
                            &nbsp;
&nbsp;
<?php echo $this->session->userdata('first_name')." ".$this->session->userdata('last_name');?>
                        </a>
                    </li>
                    <hr style="width: 220px; border-top: 1px solid;">
                </ul>

                    <div class="form-group">
                        <div class="pull-left" style="padding-left: 20px;   ">
                            <a href="<?php echo base_url()?>survey/surveyview/" class="btn btn-info btn-sm" role="button" style="padding-right: 20px; border-radius: 50px !important;">Create Survey</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url();?>home/logout/"><i class="fa fa-power-off fa-2x" style="padding-right: 20px"></i></a>
                        </div>
                    </div>
                <!-- <div class="profile_pic_div" id="user">
                     <div class="image">
                        <img alt="" src="<?php echo base_url();?>assets/newasset/image/Avatar_girl_face.png" class="img-circle img-responsive">
                    </div>


                      <div class="info">
                        <p>Hello, <?php echo $this->session->userdata('first_name')." ".$this->session->userdata('last_name');?></p> -->

                       <!-- <small style="color: #eee;">Welcome Back</small> -->
                       <!-- <p class="p_logout"><i class="fa fa-log"></i> LogOut</p> -->
                    <!-- </div>
                 </div> -->
                 <div class="clearfix"></div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <!-- <li class="treeview active">
                         <a href="">
                            <i class="fa fa-dashboard"></i>
                            <span>Survey</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                       <ul class="treeview-menu">

                             <li><a href="<?php echo base_url()?>survey/surveyview/"><i class="fa fa-minus"></i>Create Survey</a></li>

                              <li><a href="<?php echo base_url()?>survey/survey/"><i class="fa fa-minus"></i>Survey List</a></li>
                              <li><a href="<?php echo base_url()?>survey/surveyanswer/"><i class="fa fa-minus"></i>Survey Answers</a></li>
                        </ul>
                    </li> -->

                    <li class="treeview active" style="overflow-y: auto;">
                         <a class="left_nav" href="">
                            <i class="fa fa-dashboard"></i>
                            <span>Sections</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                       <ul class="treeview-menu">

                            <li><a class="left_nav" href="<?php echo base_url()?>home/slider/<?php echo $section_master[0]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[0]->section_id;
?></a></li>

                            <li><a class="left_nav" href="<?php echo base_url()?>home/section_logo/<?php echo $section_master[1]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[1]->section_id;
?></a></li>

                           <li><a class="left_nav" href="<?php echo base_url()?>home/award/<?php echo $section_master[3]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[2]->section_id;
?></a></li>

                           <li><a class="left_nav" href="<?php echo base_url()?>counters"><i class="fa fa-minus"></i><?php echo $section_master[3]->section_id;?></a></li>

                            <li><a class="left_nav" href="<?php echo base_url()?>home/Methodology/<?php echo $section_master[4]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[4]->section_id;
?></a></li>

                            <li><a class="left_nav" href="<?php echo base_url()?>home/winners_list/<?php echo $section_master[5]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[5]->section_id;
?></a></li>

                            <li><a class="left_nav" href="<?php echo base_url()?>home/shortlist_data/<?php echo $section_master[6]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[6]->section_id;
?></a></li>

                            <!-- <li><a class="left_nav" href="<?php echo base_url()?>home//<?php echo $section_master[7]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[7]->section_id;
?></a></li>  -->

                            <!-- <li><a class="left_nav" href="<?php echo base_url()?>home//<?php echo $section_master[8]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[8]->section_id;
?></a></li> -->


                             <li><a class="left_nav" href="<?php echo base_url()?>home/edit_section/<?php echo $section_master[9]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[9]->section_id;
?></a></li>
                             <li><a class="left_nav" href="<?php echo base_url()?>home/imp_links"><i class="fa fa-minus"></i><?php echo "Links";?></a></li>
                             <li><a class="left_nav" href="<?php echo base_url()?>home/footer/<?php echo $section_master[10]->id;?>"><i class="fa fa-minus"></i><?php echo $section_master[10]->section_id;
?></a></li>

                             <li><a class="left_nav" href="<?php echo base_url()?>home/nomination/"><i class="fa fa-minus"></i>Nomination</a></li>

                             <li><a class="left_nav" href="<?php echo base_url()?>home/add_button/"><i class="fa fa-minus"></i>Button</a></li>

                             <li><a class="left_nav" href="<?php echo base_url()?>home/Category/"><i class="fa fa-minus"></i>Category</a></li>

                             <li><a class="left_nav" href="<?php echo base_url()?>home/contact_list"><i class="fa fa-minus"></i>Contact list</a></li>



                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
         <script>
    $(window).bind("load", function() {
        var urlmenu = window.location.pathname;
        if(urlmenu.substr(-1) == '/') {
            urlmenu = urlmenu.substr(0, urlmenu.length - 1);
        }
        var rest_url = urlmenu.substring(0, urlmenu.lastIndexOf("/") + 1);

        var url_last_part = urlmenu.substring(urlmenu.lastIndexOf("/") + 1, urlmenu.length);
        $('.treeview-menu li a').each(function(){
            var sidebar_href = $(this).attr("href");
            if(sidebar_href.substr(-1) == '/') {
                sidebar_href = sidebar_href.substr(0, sidebar_href.length - 1);
            }

            var sidebar_rest_url = sidebar_href.substring(0, sidebar_href.lastIndexOf("/") + 1);
            var sidebar_url_last_part = sidebar_href.substring(sidebar_href.lastIndexOf("/") + 1, sidebar_href.length);
            if(sidebar_url_last_part == url_last_part)
            {
                $(this).parent("li").css("background-color", "#16a085");
                //$(this).parents("li").addClass('active');
                $(this).parent("li").parents("ul").css({"display":"block"});
            }
        });
    })
</script>
        <!-- end:left sidebar -->
        <!-- <style type="text/css">
            .lef{
                background-color: #16a085;
            }
        </style>
        <script type="text/javascript">

            $(document).ready(function(){
               $('.left_nav').click(function(){
                 alert('hii');
               // $(".nav-link").removeClass("lef");
                $(this).addClass("lef");
               });

            });
        </script> -->