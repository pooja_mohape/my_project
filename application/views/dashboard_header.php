
<?php $this->load->view("top_application"); ?>

<body class="cl-default fixed">

    <!-- start:navbar top -->
	<?php $this->load->view("navtop"); ?>
	<!-- end:navbar top -->

    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">
       
		<?php $this->load->view('left_sidebar'); ?>
		
        <!-- start:right sidebar -->
        <aside class="right-side">
            <section class="content">