<?php

$result=$this->db->get('model_content')->result();
?>
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Nomination
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                         <br><br>
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Heading</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>latitude</th>
                                            <th>longitude</th>
                                            <th>Content</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; 
                                        foreach ($result as $key => $value) 
                                        {
                                           
                                            ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->title;?></td>
                                            <td><?php echo $value->heading;?></td>
                                           
                                           <td><?php echo $value->email;?></td>
                                           <td><?php echo $value->phone;?></td>
                                            <td><?php echo $value->latitude;?></td>
                                           <td><?php echo $value->longitude;?></td>
                                           <td><?php echo $value->content;?></td>
                                            <td>   
                                                <a class="btn btn-info" href="<?php echo base_url();?>home/update_nomination/<?php echo $value->id;?>" title="Update"><i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php $i++; }?>
                                     </tbody>
                                </table>
                    </div>
                </div>
            </div>
        </div>
    </section>