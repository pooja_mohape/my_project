<!-- <style type="text/css">
	#edit {
    border-radius: 50%;
    border: 2px solid #609;
    /*text-align: center;    */
}
#container {
    height: 200px;
    line-height: 200px;
}

#container img {
    vertical-align: middle;
}
</style> -->


<section class="parallax section section-parallax" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}">
		<div class="container">
			<!-- Slider start -->
			<div class="row justify-content-center">
				<div class="col-md-10 col-sm-offset-1">
					<h2 class="custom-primary-font font-weight-normal text-6 line-height-sm mb-3" id="slider" style="border-left: 4px solid blue;background-color: lightgrey;">Change Slider
						<button type="submit" class="btn" data-toggle="modal" data-target="#myModal" id="add" style="position: absolute;right: 10px;top: -10px;">Add Slider</button></h2>
							<form action="#" method="post">
								  <table class="table">
								    <thead>
								      <tr>
								        <th>Slider Image</th>
								        <th>Title</th>
								        <th>Action</th>
								      </tr>
								    </thead>
								    <tbody>
								      <tr>
								        <td>John</td>
								        <td>Doe</td>
								        <td>
								        	<button type="submit" class="btn" id="edit">EDIT</button>
								        	<button type="submit" class="btn" id="delete">DELETE</button>
								        </td>
								      </tr>
								    </tbody>
								  </table>
							</form>
				</div>
			</div>
			<!-- Slider end -->

			<!-- Content start -->
			<div class="row justify-content-center">
				<div class="col-md-10 col-sm-offset-1">
					<h2 class="custom-primary-font font-weight-normal text-6 line-height-sm mb-3">Change Content
						<button type="submit" class="btn" data-toggle="modal" data-target="#myModal" id="add" style="position: absolute;right: 10px;top: -10px;">Add Content</button></h2>
							<form action="#" method="post">
								  <table class="table">
								    <thead>
								      <tr>
								        <th>Content Image</th>
								        <th>Title</th>
								        <th>Action</th>
								      </tr>
								    </thead>
								    <tbody>
								      <tr>
								        <td>John</td>
								        <td>Doe</td>
								        <td>
								        	<button type="submit" class="btn" id="edit">EDIT</button>
								        	<button type="submit" class="btn" id="delete">DELETE</button>
								        </td>
								      </tr>
								    </tbody>
								  </table>
							</form>
				</div>
			</div>
			<!-- Content end -->

			<div class="container">
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <?php
                            $attributes = array("method" => "POST", "id" => "modal_form", "name" => "modal_form");
                            echo form_open(base_url().'Home/add_slider', $attributes);
                        ?>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Slider</h4>
                        </div>
                        <div class="modal-body">
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label">Image</label>
                                <div class="col-md-9">
                    				<label>
                    					<img src="<?php //echo base_url().'upload/profileImages/'. $img[0]->profile_pic;?>" id="image" width="150" height="100">
                    				</label>
                    				<input type="file" name="profile_pic" id="profile_pic" required="required">
                				</div>
                            </div>
                            <div class="col-md-4 col-md-offset-4">
                      			<div class="form-group">
                          			<button type="submit" class="btn btn-primary">Image Change</button>
                      			</div>
                    		</div>
                    		<div class="form-group">
                                <label class="col-md-3 control-label">Title</label>
                                	<div class="col-md-9">
                                    <!-- <input type="hidden" name="uid" id="uid"> -->
                                    	<input type="text" class="form-control" name="title" id="title" required>
                                	</div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Text</label>
                                	<div class="col-md-9">
                                    <!-- <input type="hidden" name="uid" id="uid"> -->
                                    	<input type="text" class="form-control" name="text" id="text" required>
                                	</div>
                            </div>
                        </div>
                        <div class="modal-footer" style="border-top:none !important">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                <button type="submit" id="submit_btn" class="btn red update pull-left">Add</button>
            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
		</div>
</section>