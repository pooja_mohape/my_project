<footer id="footer" class="footer-reveal custom-background-color-1 footer-reveal border-top-0 m-0">
				<div class="container">
					<div class="row">
						
						<div class="col-lg-5">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">ABOUT US</h2>
							<h4 class="custom-primary-font text-color-light mb-3"><?php echo $about[0]->heading; ?></h4>
							<p class="pr-5"><?php echo $about[0]->content; ?></p>
						</div>

						<div class="col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">Award categories</h2>
							<ul class="list list-unstyled"> 
                                <li><a data-toggle="modal" data-target="#myModal"><?php echo $awards[0]->category; ?></a></li>
                                <li><a data-toggle="modal" data-target="#myModal1"><?php echo $awards[1]->category; ?></a></li>
                                <li><a data-toggle="modal" data-target="#myModal2"><?php echo $awards[2]->category; ?></a></li>
							</ul>
						</div>

						<div class="col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">Quick Links</h2>
							<ul class="list list-unstyled"> 
                                <li><a href=""><?php echo $links[0]->quick_links; ?></a></li>
                                <li><a href="#"><?php echo $links[1]->quick_links; ?></a></li>
                                <li><a href="#award_methodology"><?php echo $links[2]->quick_links; ?></a></li>
                                <li><a href="#winners_list"><?php echo $links[3]->quick_links; ?></a></li>
                                <li><a href="#awards_shortlist"><?php echo $links[4]->quick_links; ?></a></li>
                                <li><a href="#contact"><?php echo $links[5]->quick_links; ?></a></li>
							</ul>
						</div>
						
						<div class="col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">CONTACT US</h2>
							<p><?php echo $contact_us[0]->address; ?></p>
							<span class="d-block text-color-light custom-fontsize-5">Call: <a href="tel:+1234567890" class="text-decoration-none text-color-light custom-fontsize-5"><?php echo $contact_us[0]->call; ?></a></span>
							<span class="d-block text-color-light custom-fontsize-5">Email: <a href="mailto:you@domain.com" class="text-decoration-none text-color-light custom-fontsize-5"><?php echo $contact_us[0]->email; ?></a></span>
							<ul class="social-icons social-icons-transparent social-icons-icon-light mt-4">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter mx-4"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>

						<div class="col-lg-2">
							<a data-toggle="modal" data-target="#nomi" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-1 ml-3"><?php echo $re[0]->button_name;?></a>
						</div>

					</div>
				</div>
				<div class="footer-copyright custom-background-color-1 border-top-0 mt-0">
					<div class="container">
						<hr class="solid custom-opacity-1 mb-0">
						<div class="row">
							<div class="col mt-4 mb-3">
								<p class="text-center mb-0"><a href="https://bdsserv.com/">Copyright © 2018 BDS STZ Techno Systems Pvt. Ltd. All Rights Reserved.</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
	<?php $this->load->view('layout/footer_script'); ?>


	