<!DOCTYPE html>
<!-- <html data-style-switcher-options="{'changeLogo': false, 'borderRadius': 0, 'colorPrimary': '#1b9dff', 'colorSecondary': '#3bb452', 'colorTertiary': '#0a83df', 'colorQuaternary': '#e8eef3'}">
 -->	
<!-- Mirrored from preview.oklerthemes.com/porto/6.2.0/demo-insurance.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 04 May 2018 11:37:29 GMT -->
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Awards</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content=" Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url();?>/assets/img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?php echo base_url();?>/assets/img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<!-- <link href="<?php //echo base_url();?>/assets/https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css"> -->

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/animate/animate.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-elements.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-blog.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/demos/demo-insurance.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/skins/skin-insurance.css">		
	<!-- <script src="master/style-switcher/style.switcher.localstorage.js"></script>-->
	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/custom.css">

	<!-- Head Libs -->
	<script src="<?php echo base_url();?>/assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
<body>

	<div class="body">
	<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0px', 'stickyChangeLogo': false}">
		<div class="header-body" >
			<div class="header-container container">
				<div class="header-row">
					<div class="header-column">
					<?php $c=0; foreach ($logo_img as $key => $value) {?>
						<div class="header-row">
							<div class="header-logo py-3">
								<a href="<?php echo base_url();?>">
								<img class="avt logo" alt="Porto" width="84" height="41" src="<?php echo base_url();?>uploads/<?php echo $value->img; ?>">
								</a>
							</div>
						</div>
						<?php }?>
					</div>
					<div class="header-column justify-content-end">
						<div class="header-row">
							<div class="header-nav header-nav-stripe">
								<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
									<!-- <nav class="collapse">
										<ul class="nav" id="mainNav">
											<li>
												<a class="nav-link active" href="#home">
													Home
												</a>
											</li>

											<li class="dropdown">
                                                <a class="nav-link dropdown-toggle" href="#">categories
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a data-toggle="modal" data-target="#myModal" class="dropdown-item">People categories</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModal1" class="dropdown-item">project categories</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModal2" class="dropdown-item">Company categories</a></li>
                                                </ul>
                                            </li>
											
											<li>
												<a class="nav-link" href="#award_methodology">Methodology
												</a>
											</li>
											
											<li>
												<a class="nav-link" href="#winners_list">Winners
												</a>
											</li>
											
											<li>
												<a class="nav-link" href="#awards_shortlist">Shortlist
												</a>
											</li>
											
											<li>
												<a class="nav-link" href="#contact">
													Contact
												</a>
											</li>
										</ul>
									</nav> -->
									<nav class="collapse">
										<ul class="nav" id="mainNav">
                                            <li>
												<a class="nav-link l active" href="#slider_section">
													Home
												</a>
											</li>

											<?php 
                                             $get_menu = $this->db->order_by('seq_no')->get('menu')->result();

                                              if($get_menu){
                                             	foreach ($get_menu as $key => $value) {	
											  ?>
                                               <?php if($value->parent_id == '0' && empty($value->child)){
                                               	?>
												    <li>
												       <a class="nav-link l" href="<?php echo $value->link; ?>"><?php echo $value->name; ?>
												        </a>
											        </li>
											   <?php }else{?>
				                               <?php if($value->child == '1'){?> 
				                                    <li class="dropdown">
														<a class="nav-link dropdown-toggle" href="#">
															<?php echo $value->name; ?>
														</a>
														<ul class="dropdown-menu">
															<?php 
													         $get_child = $this->db->where('parent_id',$value->id)->get('menu')->result();
													         if($get_child){
													        foreach ($get_child as $ckey => $cvalue) {
													        ?>
													        <li><a data-toggle="modal" data-target="<?php echo $cvalue->link; ?>" class="dropdown-item l"><?php echo $cvalue->name; ?></a></li>
													       <?php }?>
													    <?php }?>
													    </ul>
													</li>
										      <?php }?>

                                                         
											    <?php }?>

											  <?php }}?>
											<!-- <li>
												<a class="nav-link active" href="<?php echo base_url();?>">
													Home
												</a>
											</li>
											<li class="dropdown">
												<a class="">
													categories
												</a>
												<ul class="dropdown-menu">
                                                    <li><a data-toggle="modal" data-target="#myModal" class="dropdown-item">People categories</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModal1" class="dropdown-item">project categories</a></li>
                                                    <li><a data-toggle="modal" data-target="#myModal2" class="dropdown-item">Company categories</a></li>
                                                </ul>
											</li>
											<li>
												<a class="nav-link" href="#award_methodology">
													Shortlist
												</a>
											</li>
											<li>
												<a class="nav-link" href="#winners_list">Winners
												</a>
											</li>
											<li>
												<a class="nav-link" href="#award_methodology">Methodology
												</a>
											</li>
											<li>
												<a class="nav-link" href="#latest_news">News</a>
											</li>
											<li>
												<a class="nav-link" href="#contact">
													Contact
												</a>
											</li> -->
										</ul>
									</nav>
								</div>
								<?php $c=0; foreach ($re as $key => $value) {?>
								<a data-toggle="modal" data-target="#nomi" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-1 ml-3 shadow card"> <?php echo $value->button_name;?></a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<button class="btn header-btn-collapse-nav ml-0" data-toggle="collapse" data-target=".header-nav-main nav"><i class="fas fa-bars"></i>
			</button>
		</div>
	</header>

<script type="text/javascript">
	$('.navbar-nav>li>a').on('click', function(){
   $('.navbar-collapse').collapse('hide');
});
</script>