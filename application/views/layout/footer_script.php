<!-- Vendor -->
		<!-- <script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script> -->

		<script src="<?php echo base_url();?>/assets/vendor/jquery.appear/jquery.appear.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery-cookie/jquery-cookie.min.js"></script><!--

		<script src="<?php echo base_url();?>/assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-insurance.less"></script> -->

		<script src="<?php echo base_url();?>/assets/vendor/popper/umd/popper.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/common/common.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.validation/jquery.validation.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/isotope/jquery.isotope.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/vide/vide.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>/assets/js/theme.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url();?>/assets/js/demos/demo-insurance.js"></script>

		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>/assets/js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>/assets/js/theme.init.js"></script>
		<script src="<?=base_url();?>assets/formBuilder/form-render.min.js"></script>
	</body>
</html>

<!--css end for side arrow award and winner-list-->
<style type="text/css">
	.paddle a {
	    opacity: .6;
	    background: #e5e5e5;
	    position: fixed;
	    top: 50%;
	    z-index: 2147483637;
	    height: 10rem;
	    touch-action: none;
	    text-decoration: none;
	}


	.paddle.previous a::before {
	    float: left;
	}

	.paddle.next a::before {
	    float: right;
	}

	.paddle.previous a::before {
		    font-size: 3rem;
		    font-weight: bold;
		    position: relative;
		    color: #fff;
		    content: "\f053";
		    font-size: 50px;
		    /*font-family: ps_g;*/
		    font-family: Font Awesome\ 5 Free;
		}

	.paddle.next a::before {
	    font-size: 3rem;
	    font-weight: bold;
	    position: relative;
	    color: #fff;
	    font-size: 50px;
	    content: "\f054";
	    /*font-family: ps_g;*/
	    font-family: Font Awesome\ 5 Free;
	}

	.paddle a::before {
	    display: block;
	    background: #666;
	    height: 100%;
	    padding-top: 3.9rem;
	    text-align: center;
	    width: 3rem;
	}

	.paddle.previous a .headline {
	    border-right: none;
	}

	.paddle.next a .headline {
	    border-right: none;
	}
	.paddle.previous a .headline {
	    border-right: .4rem solid #624f87;

	}

	.paddle.next a .headline {
	    border-left: .4rem solid #624f87;

	}
	.paddle a .headline {
	    display: inline-block;
	    font-family: 'Segoe UI';
	    font-family: "Segoe UI Bold","Segoe UI","Segoe WP","Arial","Sans-Serif";
	    font-weight: 700;
	    font-size: 30px;
	    line-height: 7.2rem;
	    letter-spacing: normal;
	}
	.paddle a .headline {
	    color: #333;
	    display: inline-block;
	    font-size: 1.8rem;
	    text-align: center;
	    height: 100%;
	    overflow: hidden;
	    position: relative;
	    transition: width .5s cubic-bezier(.1,.9,.2,1);
	    width: 0;
	}

	.paddle a:focus .headline, .paddle a:hover .headline, .paddle a.expand .headline {
	    padding: 0;
	    width: 20rem;
	}
	.paddle a:focus .headline, .paddle a:hover .headline, .paddle a.expand .headline {
	    padding: 1rem;
	    width: 20rem;
	}

	.paddle.next a {
	    right: 0;
	}

	.paddle a:hover, .paddle a.expand {
	    opacity: 1;
	    text-decoration: none;
	}
/*css end for side arrow award and winner-list*/
	 .errors{
        color:red;
    }
/*winner-list*/
	.avatar
	{
		max-width: 110px;
		max-height: 110px;
		width: 90px;
		height: 90px;
		background-position: center center;
		background-size: cover;

	}

	.avt
	{
		max-width: 100px;
		max-height: 40px;
		background-size: cover;
		width: auto;
		transform: scale(1.5);
	}
/*location font-awsome-icon middle */
	.vertical-align{
		margin-top: auto;
		margin-bottom: auto;
	}
/*animation on card*/
	.card-body:hover
    {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }

    .card
	{
		box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }

	.shadow:hover
    {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }

    .shade
    {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }

/*logo*/
    .logo
    {
        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
    }

    .logo:hover
    {
       box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
		transition: all 0.3s cubic-bezier(.25,.8,.25,1);
    }

/*award short list word wrap*/
    #exactFit
    {
    word-wrap: break-word;
	}

/*on map view contact_page*/
@media (max-width:2560px){
	.con{
		display:none;
	}
}

@media (max-width:991px){
	.con{
		display:block;
		background-color:#185481;
    	font-size: 22px;
    	font-weight: bolder;
    	color: white;
}

}
/*contact_us*/
.hide{
	opacity: 0;
	z-index: -999999;
}
.show{
	opacity: 1;
	/*z-index: 9999999;*/
}
/*latest_news*/
.fadeshow {
    opacity: 1;
    display: block;
}
.fadehide {
    opacity: 0;
    display: none;
}
/*award shortlist*/
.animation{
	transition: all 0.75s ease-in-out;
}
.h-30{
max-height:150px;
overflow-y:hidden;
text-overflow:ellipsis;
}

.h-50{
max-height:500px;
overflow-y:auto;
}
</style>

<!-- Confirmation Alert -->
<!-- <script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script> -->

<script src="<?php base_url();?>assets/js/jsonify.js"></script>
<script>
$(document).ready(function(){
	//award category
	$("#myModal1s").click(function(){
		$("#myModal").modal('hide');
		$("#myModal1").modal('show');
	});


	$("#myModal3s").click(function(){
		$("#myModal1").modal('hide');
		$("#myModal").modal('show');
	});


	$("#myModal2s").click(function(){
		$("#myModal1").modal('hide');
		$("#myModal2").modal('show');
	});


	$("#myModal5s").click(function(){
		$("#myModal2").modal('hide');
		$("#myModal1").modal('show');
	});


	$("#myModal4s").click(function(){
		$("#myModal2").modal('hide');
		$("#myModal").modal('show');
	});


//winerlist
	$("#demo1").click(function(){
   		$("#myModal5").modal("hide");
   		$("#myModal6").modal("show");
   });

   $("#demo2").click(function(){
   		$("#myModal6").modal("hide");
   		$("#myModal5").modal("show");
   });

   $("#demo3").click(function(){
   		$("#myModal6").modal("hide");
   		$("#myModal7").modal("show");
   });

   $("#demo4").click(function(){
   		$("#myModal7").modal("hide");
   		$("#myModal6").modal("show");
   });

   $("#demo5").click(function(){
   		$("#myModal7").modal('hide');
   		$("#myModal8").modal('show');
   });

   $("#demo6").click(function(){
   	$("#myModal8").modal("hide");
   	$("#myModal7").modal("show");
   });

   $("#demo7").click(function(){
   		$("#myModal8").modal('hide');
   		$("#myModal5").modal('show');
   });

   	//nomination form
	$("#nomi_rule").click(function(){
		$("#nomi").modal('hide');
		$("#rule_nom").modal('show');
	});

	$("#nom").click(function(){
		// alert("Do You Really Want to quite ?");
	});

	$("#nomi_detail").click(function(){

		$("#rule_nom").modal('hide');
		$("#nomi_form").modal('show');
		//alert("oh oh jane jana");
		var x 	= lcl_storage;
		var y 	= localStorage.getItem("user_id");
		var s_id = localStorage.getItem("sid");
		var chk=1;
		var formData='';
		// console.log(x+'new='+y+'survey_id='+survey_id+' sid='+s_id);
		// toast msg
		  var msg_type = "<?php echo $this->session->flashdata('msg_type');?>";
		  var msg = "<?php echo $this->session->flashdata('msg');?>";

		  if(msg_type != "" && msg != "")
		  {
		     $.bootstrapGrowl(msg, { type: msg_type });
		  }
		  //toast msg end

		if(y!='' && s_id==survey_id)
		 {
		 	x=y;
		 }

		if (typeof(Storage) !== "undefined")
		{

			if(y!='' && s_id!=survey_id)

			{
				localStorage.removeItem("user_id");
				localStorage.removeItem("s_id");
			}

			if(y!='' && s_id==survey_id)
			{
			var formData ='';

			$.ajax({
					url: '<?=base_url();?>survey/fetchDraftAns',
					 	type: 'post',
					 	data:{survey_id:survey_id,x:x},
					 	success:function(res)
					 	{	//alert('hello');
					 		formData = res;
			 			}
				});
		  	 	formData = jsonData;
			}
			else
			{

				localStorage.setItem("user_id", x);
				localStorage.setItem("s_id", survey_id);
			}
		}

		else
		{	//local Storage not supported
		}

		 formData = jsonData;

	 	  formRenderOpts =
	 	  {
		      dataType: 'json',
		      formData: formData
	      };

	  $('#formRender').empty();
	  var renderedForm = $('<div>');
	  renderedForm.formRender(formRenderOpts);
	  $('#formRender').append(renderedForm.html());
	  $('#formRender').append("<input type='hidden' name='survey_id' value='"+survey_id+"'>");
	  $('#formRender').append("<input type='hidden' name='lcl_storage' value='"+x+"'>");
	  $('#formRender').append("<input type='submit'name='survey_btn' id='survey_btn' value='Submit Form' class='btn btn-secondary text-uppercase pull-left'>");
	  // id='static' data-toggle='modal' data-target='#statistics'
		//alert("oh oh jane jana");

		$('#nom').click(function() {

			dataString = $('#formRender').serialize();

			if(dataString!='' || dataString==undefined)
			{
				$.ajax({
						url: '<?=base_url();?>survey/formDataDraft',
					 	type: 'post',
					 	data:dataString,
					 	success:function(res)
					 	{
					 	   var rsp	    = JSON.parse(res);
				           var msg_type = obj.msg_type;
				           var msg      = obj.msg;

				           if(msg_type && msg)
				           {
				                $.bootstrapGrowl(msg, { type: msg_type });
				           }
			 			}
					});
			 // return false;
			}
		    else {}

		});

		$("form").submit(function() {

			dataString = $('#formRender').serialize();
			$.ajax({
					url: '<?=base_url();?>survey/formData',
				 	type: 'post',
				 	data:dataString,
				 	success:function()
					 	{
					 	   var rsp	    = JSON.parse(res);
				           var msg_type = obj.msg_type;
				           var msg      = obj.msg;

				           if(msg_type && msg)
				           {
				                $.bootstrapGrowl(msg, { type: msg_type });
				           }
				 		}
					});
			return false;

			});

		//post on beforeunload

			$(window).on('beforeunload', function() {

				if(survey_id!='0' || survey_id!=undefined)
				{
					dataString = $('#formRender').serialize();
						if(dataString!='' || dataString==undefined)
						{
							$.ajax({
									url: '<?php echo base_url();?>survey/formDataDraft',
								 	type: 'post',
								 	data:dataString,
								 	success:function()
								 	{
								 		alert('form submitted1');
						 			}
								});
						 return false;
						}
						else
						{}
	             return 'Are you sure you want to leave?';
	         }
			});
	});

	// $('#formRender').append('input["type=submit"]').click(function(){
		$("form").submit(function() {
		//alert('dil di ya galla');
		$("#nomi_form").modal('hide');
    	$("#statistics").modal('show');
	});

	//nomi form
	$("#domain_type1").on('change',function(){
		$("#des_nom").html($(this).val());
	});


	//mobile on click nav hide
	// $(".nav li a").click(function(){
	// 	$('.collapse').collapse('hide');
	// });
		$('.nav').find('.l').click(function(){
		//alert("mr.india");
		$('.collapse').collapse('hide');
	});
	//navbar active

	$(".nav-link").click(function(){
		$(".nav-link").removeClass("active");
		$(this).addClass("active");
	});

// award_shortlist_view more
    $(".view_shortlist").click(function(){
 // debugger
          // console.log($(this));
          var btn = $(this).closest('.award_list').find('.list_data');
          // console.log(btn);
        var h50 = btn.hasClass('h-50')
              if(h50){
          		btn.toggleClass('h-50');
                	$(this).html('View more <i class="fas fa-long-arrow-alt-right"></i>');
           		}
           		else if($('.list_data').removeClass('h-50')){
           			btn.toggleClass('h-50');
                	$(this).html('Show Less <i class="fas fa-long-arrow-alt-right"></i>');
           		}
           		else{
               		$('.list_data').removeClass('h-50');
          			btn.toggleClass('h-50');
					$(this).html('View more <i class="fas fa-long-arrow-alt-right"></i>');
			           btn = $(this).closest('.award_list').find('.list_data');
			          // console.log(btn);
					}

       });

// winerlist
	$(".all_view").click(function(){
		var year = $(this).attr('pgt');

		$.ajax({
			url:"<?php echo base_url();?>Home/all_list",
			type:"POST",
			data:{year:year},
			success:function(result){
				var data = JSON.parse(result);
				$(".winlist").html(data.head);
				$(".all1").attr('pgt',data.prev);
				$(".all2").attr('pgt',data.next);
				$('.next1').html(data.next);
				$(".previous1").html(data.prev);
				$("#test1").html(data.sam);
				$("#myModal5").modal('show');
			}
		})
	});


	$(".sampurna li a").first().addClass("active");


	$(".year").click(function(){
		var year = $(this).attr('pgt1');

		$.ajax({
			url:'<?php echo base_url();?>Home/get_content',
			type:'POST',
			data:{year:year},
			success:function(result){
				var data = JSON.parse(result);
				$("#tabsNavigation").hide();
				$("#tabs_nav").html(data);
				$("#tabs_nav").css('display','inline');
			}
		})
	});
//latest News
        $(".view_more_news").click(function(){
        var pgt=$(this).attr('pgt');
        var pgt1 = $(this).attr('pgt1');
        var nam = $(this).attr('pgt2');
        var img = "<?php echo base_url();?>uploads/"+pgt1+"";

        $("#text_id").html(pgt);
     	$("#img").attr('src',img);
     	$("#nme").html(nam);
		$("#modal_news").addClass('fadeshow')

        });
         $(".btn-close").on("click", function(){

		$('#modal_news').removeClass('fadeshow');


		});

// conatct_page show hide
	$("#button").click(function(){
		   //alert( $(this).html());
	    if($(this).html() == "-"){
	    	$("#disp").addClass('hide');
	        	$(this).html("+");
	    }
	    else{
	        $(this).html("-");
	         $("#disp").removeClass('hide');
	    }
	    // $("#box").slideToggle();
	    $("#map_footer").slideToggle();

		});
	//$("#map_footer").show();
	});
</script>
<!-- map -->
	<script>

	var setCenterWithOffset = function(map,latlng, offsetX, offsetY) {
    var map = map;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
    }
    ov.draw = function() {};
    ov.setMap(map);
};
		function myMap() {
			var lng = document.getElementById("contact_lng").value;
			var lat = document.getElementById("contact_lat").value;

			var lng1 = document.getElementById("modal_map_lng").value;
			var lat1 = document.getElementById("modal_map_lat").value;

			var myLatlng =  new google.maps.LatLng(lat,lng)
			var myLatlng1 =  new google.maps.LatLng(lat1,lng1)


		var mapProp= {
		    center:myLatlng,
		    zoom:18,
		    disableDefaultUI: true,

		};
		var mapProp1= {
		    center:myLatlng1,
		    zoom:19,
		    disableDefaultUI: true,

		};

		var marker = new google.maps.Marker({
		   position: myLatlng,
		   animation: google.maps.Animation.DROP,
		   title:"Hello World!"
		});



		var marker1 = new google.maps.Marker({
		   position: myLatlng1,
		   animation: google.maps.Animation.DROP,
		});



		var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
		var map1=new google.maps.Map(document.getElementById("mygMap"),mapProp1);
		setCenterWithOffset(map,myLatlng, 250, 0);


			marker.setMap(map);
			marker1.setMap(map1);


		}

	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZj_qzHLQS44-TLqT6QGQHcWqPGOZUB0M&callback=myMap"></script>

