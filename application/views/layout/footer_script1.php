		<!-- Vendor -->
		<!-- <script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script> -->

		<script src="<?php echo base_url();?>/assets/vendor/jquery.appear/jquery.appear.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery-cookie/jquery-cookie.min.js"></script><!-- 

		<script src="<?php echo base_url();?>/assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-insurance.less"></script> -->

		<script src="<?php echo base_url();?>/assets/vendor/popper/umd/popper.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/common/common.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.validation/jquery.validation.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/isotope/jquery.isotope.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>/assets/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url();?>/assets/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>/assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>/assets/js/theme.init.js"></script>
		<script src="<?= base_url();?>assets/formBuilder/form-render.min.js"></script>
	</body>
</html>
<!-- Css slide -->
<style type="text/css">
	.paddle a {
	    opacity: .6;
	    background: #e5e5e5;
	    position: fixed;
	    top: 50%;
	    z-index: 2147483637;
	    height: 10rem;
	    touch-action: none;
	    text-decoration: none;	
	}


	.paddle.previous a::before {
	    float: left;
	}

	.paddle.next a::before {
	    float: right;
	}

	.paddle.previous a::before {
		    font-size: 3rem;
		    font-weight: bold;
		    position: relative;
		    color: #fff;
		    content: "\f053";
		    font-size: 50px;
		    /*font-family: ps_g;*/
		    font-family: Font Awesome\ 5 Free;
		}

	.paddle.next a::before {
	    font-size: 3rem;
	    font-weight: bold;
	    position: relative;
	    color: #fff;
	    font-size: 50px;
	    content: "\f054";
	    /*font-family: ps_g;*/
	    font-family: Font Awesome\ 5 Free;
	}

	.paddle a::before {
	    display: block;
	    background: #666;
	    height: 100%;
	    padding-top: 3.9rem;
	    text-align: center;
	    width: 3rem;
	}

	.paddle.previous a .headline {
	    border-right: none;
	}

	.paddle.next a .headline {
	    border-right: none;
	}
	.paddle.previous a .headline {
	    border-right: .4rem solid #624f87;

	}

	.paddle.next a .headline {
	    border-left: .4rem solid #624f87;
	    
	}
	.paddle a .headline {
	    display: inline-block;
	    font-family: 'Segoe UI';
	    font-family: "Segoe UI Bold","Segoe UI","Segoe WP","Arial","Sans-Serif";
	    font-weight: 700;
	    font-size: 30px;
	    line-height: 7.2rem;
	    letter-spacing: normal;
	}
	.paddle a .headline {
	    color: #333;
	    display: inline-block;
	    font-size: 1.8rem;
	    text-align: center;
	    height: 100%;
	    overflow: hidden;
	    position: relative;
	    transition: width .5s cubic-bezier(.1,.9,.2,1);
	    width: 0;
	}

	.paddle a:focus .headline, .paddle a:hover .headline, .paddle a.expand .headline {
	    padding: 0;
	    width: 20rem;
	}
	.paddle a:focus .headline, .paddle a:hover .headline, .paddle a.expand .headline {
	    padding: 1rem;
	    width: 20rem;
	}

	.paddle.next a {
	    right: 0;
	}

	.paddle a:hover, .paddle a.expand {
	    opacity: 1;
	    text-decoration: none;
	}
}

</style>

<script>
$(document).ready(function(){
	//award category
	$("#myModal1s").click(function(){
		$("#myModal").modal('hide');
		$("#myModal1").modal('show');
	});


	$("#myModal3s").click(function(){
		$("#myModal1").modal('hide');
		$("#myModal").modal('show');
	});


	$("#myModal2s").click(function(){
		$("#myModal1").modal('hide');
		$("#myModal2").modal('show');
	});


	$("#myModal5s").click(function(){
		$("#myModal2").modal('hide');
		$("#myModal1").modal('show');
	});


	$("#myModal4s").click(function(){
		$("#myModal2").modal('hide');
		$("#myModal").modal('show');
	});


//winerlist
	$("#demo1").click(function(){
   		$("#myModal5").modal("hide");
   		$("#myModal6").modal("show");
   });

   $("#demo2").click(function(){
   		$("#myModal6").modal("hide");
   		$("#myModal5").modal("show");
   });

   $("#demo3").click(function(){
   		$("#myModal6").modal("hide");
   		$("#myModal7").modal("show");
   });

   $("#demo4").click(function(){
   		$("#myModal7").modal("hide");
   		$("#myModal6").modal("show");
   });

   $("#demo5").click(function(){
   		$("#myModal7").modal('hide');
   		$("#myModal8").modal('show');	
   });

   $("#demo6").click(function(){
   	$("#myModal8").modal("hide");
   	$("#myModal7").modal("show");
   });

   $("#demo7").click(function(){
   		$("#myModal8").modal('hide');
   		$("#myModal5").modal('show');
   });

   	//nomination form
	$("#nomi_rule").click(function(){
		$("#nomi").modal('hide');
		$("#rule_nom").modal('show');
	});
	$("#nomi_detail").click(function(){
		$("#rule_nom").modal('hide');
		$("#nomi_form").modal('show');
		//alert("oh oh jane jana");
		
		var x = lcl_storage;
		var y = localStorage.getItem("user_id");
		// alert(y);
		if(y)
		 {
		 	x=y;
		 }

		if (typeof(Storage) !== "undefined") 
		{	

			if(y) 
			{
			var formData ='';
		
			$.ajax({
					url: '<?= base_url(); ?>survey/fetchDraftAns',
					 	type: 'post',
					 	data:{survey_id:survey_id,x:x},
					 	success:function(res)
					 	{	//alert('hello');				 		
					 		formData = res;
			 			}
				});
		  	 	formData = jsonData;	 
			}
			else 
			{
				localStorage.setItem("user_id", x);
			}
		}

		else 
		{	//local Storage not supported
		}

		 formData = jsonData;	 

	 	  formRenderOpts = 
	 	  {
		      dataType: 'json',
		      formData: formData
	      };

	  $('form').empty();
	  var renderedForm = $('<div>');
	  renderedForm.formRender(formRenderOpts);
	  $('form').append(renderedForm.html());
	  $('form').append("<input type='hidden' name='survey_id' value='"+survey_id+"'>");
	  $('form').append("<input type='hidden' name='lcl_storage' value='"+x+"'>");
	  $('form').append("<input type='submit' id='survey_btn' value='Submit Form' class='btn btn-secondary text-uppercase pull-left' id='static' data-toggle='modal' data-target='#statistics'>");
		//alert("oh oh jane jana");

		$('#nomi_close').click(function() {

			dataString = $('#formRender').serialize();
			
			if(dataString!='' || dataString==undefined) 
			{
				$.ajax({
						url: '<?= base_url(); ?>survey/formDataDraft',
					 	type: 'post',
					 	data:dataString,
					 	success:function()
					 	{
					 		alert('form submitted_nm');
			 			}
					});
			 // return false;
			}
		    else {}

		});

		$("form").submit(function() {
			
			dataString = $('#formRender').serialize();
			$.ajax({
					url: '<?= base_url(); ?>survey/formData',
				 	type: 'post',
				 	data:dataString,
				 	success:function()
					 	{
					 		alert('form submitted');
				 		}
					});
			return false;
			
			});

		//post on beforeunload

			$(window).on('beforeunload', function() {
				
				dataString = $('#formRender').serialize();
					if(dataString!='' || dataString==undefined) 
					{
						$.ajax({
								url: '<?php echo base_url(); ?>survey/formDataDraft',
							 	type: 'post',
							 	data:dataString,
							 	success:function()
							 	{
							 		alert('form submitted1');
					 			}
							});
					 return false;
					}
					else 
					{}
	             return 'Are you sure you want to leave?';
			});

		});
	//browser close
	
	$("#static").click(function(){
		$("#nomi_form").modal('hide');
		$("#statistics").modal('show');
		//alert("oh oh jane jana");
	});
	//nomi form
	$("#domain_type1").on('change',function(){
		$("#des_nom").html($(this).val());
	});


	//mobile on click nav hide
	// $(".nav li a").click(function(){
	// 	$('.collapse').collapse('hide');
	// });
		$('.nav').find('.l').click(function(){
		//alert("mr.india");
		$('.collapse').collapse('hide');
	});
	//navbar active

	$(".nav-link").click(function(){
		$(".nav-link").removeClass("active");
		$(this).addClass("active");
	});
	//highlight nav on ID
	$("#home").mouseenter(function(){
        $("#nav1").css("color", "#1b9dff");
    });
    $("#home").mouseleave(function(){
        $("#nav1").css("color", "#000");
    });

    $("#award_methodology").mouseenter(function(){
        $("#nav2").css("color", "#1b9dff");
    });
    $("#award_methodology").mouseleave(function(){
        $("#nav2").css("color", "#000");
    });

    $("#winners_list").mouseenter(function(){
        $("#nav3").css("color", "#1b9dff");
    });
    $("#winners_list").mouseleave(function(){
        $("#nav3").css("color", "#000");
    });

    $("#awards_shortlist").mouseenter(function(){
        $("#nav4").css("color", "#1b9dff");
    });
    $("#awards_shortlist").mouseleave(function(){
        $("#nav4").css("color", "#000");
    });

    $("#latest_news").mouseenter(function(){
        $("#nav5").css("color", "#1b9dff");
    });
    $("#latest_news").mouseleave(function(){
        $("#nav5").css("color", "#000");
    });

    $("#contact").mouseenter(function(){
        $("#nav6").css("color", "#1b9dff");
    });
    $("#contact").mouseleave(function(){
        $("#nav6").css("color", "#000");
    });

	$(".all_view").click(function(){
		var year = $(this).attr('pgt');

		$.ajax({
			url:"<?php echo base_url();?>Home/all_list",
			type:"POST",
			data:{year:year},
			success:function(result){
				var data = JSON.parse(result);
				$(".winlist").html(data.head);
				$(".all1").attr('pgt',data.prev);
				$(".all2").attr('pgt',data.next);
				$('.next1').html(data.next);
				$(".previous1").html(data.prev);
				$("#test1").html(data.sam);
				$("#myModal5").modal('show');
			}
		})
	});
	
});
</script>
<!-- map -->
	<script>
		function myMap() {
			var abc = $("#hid1").val();
			var xyz = $("#hid2").val();
		var mapProp= {
		    center:new google.maps.LatLng(51.5010,-0.1291),
		    zoom:18,
		   
		};
		var mapProp1= {
		    center:new google.maps.LatLng(abc,xyz),
		    zoom:18,
		   
		};
		var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
		var map=new google.maps.Map(document.getElementById("mygMap"),mapProp1);


		}
		
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZj_qzHLQS44-TLqT6QGQHcWqPGOZUB0M&callback=myMap"></script>
	

<style type="text/css">
    .errors{
        color:red;
    }
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$(".year").click(function(){
			var year = $(this).attr('pgt1');

			$.ajax({
				url:'<?php echo base_url();?>Home/get_content',
				type:'POST',
				data:{year:year},
				success:function(result){
					var data = JSON.parse(result);
					$("#tabsNavigation").hide();
					$("#tabs_nav").html(data);
					$("#tabs_nav").css('display','inline');
				}
			})
		});
	});
</script>


<script type="text/javascript">
    $(document).ready(function(){    

        $(".view_more_data").click(function(){
           $(this).closest('.abc').find('.pqr_data').hide();
           $(this).closest('.abc').find('.zmr_data').slideDown('slow');
        });

        $(".view_less_data").click(function(){
           $(this).closest('.abc').find('.pqr_data').show();
           $(this).closest('.abc').find('.zmr_data').slideUp('slow');
        });
    });
</script>
<script type="text/javascript">

</script>
