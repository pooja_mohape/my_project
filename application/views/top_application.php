<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- New CSS -->
	<title>Admin-Panel</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/css/bootstrap-reset.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/css/dashboard_style.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/css/table-responsive.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/css/tasks.css" />
	<link rel="stylesheet" href="cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
	
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/arjuna.js"></script>
	
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/auto_complete/jquery.autocomplete.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/auto_complete/jquery.tokeninput.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/draggable-portlet.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/editable-table.js"></script>
	<!-- <script src="<?php echo base_url(); ?>assets/vendorpanel/js/form-validation-script.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/jquery.stepy.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/jquery.validate.min.js"></script>
	
	<!-- data table dynamic -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendorpanel/plugins/data-tables/DT_bootstrap.css">
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/advanced-datatable/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/data-tables/DT_bootstrap.js"></script>
    
    
    <!--- ajaxForm & ajaxSubmit -->
    <script src="<?php echo base_url(); ?>assets/newasset/js/jquery.form.js"></script>
	
    <!--<script src="<?php echo base_url(); ?>assets/vendorpanel/js/nestable.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/owl-carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/slider.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/js/tasks.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/advanced.component.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendorpanel/js/morris-script.js"></script>-->
	
	<script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/jquery.flot.grow.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/flot/demo.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendorpanel/plugins/owl.carousel/owl-carousel/owl.carousel.js"></script>
	
    <!-- Datatable js -->
    <script src=""></script>
    <!-- <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script> -->
	<style type="text/css">
		    .errors{
		        color: red;
		    }
    </style>
	<!--- COOKIE LOCAL STORAGE -->
	<script src="<?php echo base_url(); ?>assets/newasset/js/halma-localstorage.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
  <?php //} ?>


</head>

<!-- End Google Tag Manager (noscript) -->
<noscript>
	<div style="height:30px;border:3px solid #6699ff;text-align:center;font-weight: bold;padding-top:10px">
		Java script is disabled , please enable your browser java script first.
	</div>
</noscript>