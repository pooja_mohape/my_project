<style type="text/css">
  table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style>
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Add Slider
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <div class="form-group" >

                        <label class="col-lg-3 control-label" for="name">Card</label>
                        <div class="col-lg-4">                           
                            <div class="btn-group" id="toggle_event_editing">
                          <button type="button" class="btn btn-info locked_active" id="off" value="off">OFF</button>
                          <button type="button" class="btn btn-default unlocked_inactive" id="on" value="on">ON</button>
                        </div>
                        <div class="" id="switch_status">
                          <?php if($card[0]->status == 1){?>
                             <b>Card is currently Visible.</b>
                          <?php }else{?>
                             <b>Card is currently Hidden.</b> 
                          <?php }?>
                        </div>
                        </div>
                    </div>

                      <div class="clearfix" style="height: 15px;clear: both;"></div>
                      <hr>

                    <?php
                       $attributes = array("method" => "POST", "id" => "slider_form", "name" => "slider_form", "class" => "form-group");
                        echo form_open_multipart('home/upload_slider',$attributes);?>
                     
                      <div class="form-group">
                        <label class="col-lg-3 control-label" for="name">Title</label>
                        <div class="col-lg-4">                           
                            <input name="heading" type="text" id="heading" class="form-control" value="" placeholder="Enter Title">
                              
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">
                                <textarea name="content" type="text" id="content" class="form-control" rows="3" value="" placeholder="Enter Content" maxlength="250"></textarea>
                            </div>
                        </div> 

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Button Name</label>
                            <div class="col-lg-4">
                                <input name="btn" type="text" id="btn" class="form-control" value="" placeholder="Enter Button Name">
                            </div>
                        </div> 

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Image</label>
                            <div class="col-lg-4">
                               
                              <input type="file" name="userfile" id="userfile" size="20" placeholder="Select Image" required="" />
                            </div>
                        </div>  
                        <br><br>        
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Save</button> 
                               
                                  <a href="<?php echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         </form>
                         <br><br>
                       <div style="overflow:auto;" >
                            <table id="example" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                            <th>Button</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php

      $query = $this->db->query("SELECT * from slider");
      $result = $query->result_array();
      $count = 1; 

      foreach ($result as $val){ ?>
      <tr>
       <td><?php echo $count ?></td>
        <td><img src="<?php echo base_url();?>uploads/<?php echo $val['img'];?>" style="width:70px; height: 60px;"></td>
       
       <td><?php echo $val['title']; ?></td>
       <td><?php echo $val['content']; ?></td>
       <td><?php echo $val['button_name'];?></td>
       <td><a class="btn btn-danger" onclick="return checkDelete()" href="<?php echo base_url(); ?>home/delete/<?php echo $val['id'];?>" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>|<a class="btn btn-info" href="<?php echo base_url();?>home/edit_slider/<?php echo $val['id'];?>" title="Update"><i class="fa fa-edit"></i></a></td>
      </tr>
      <?php
      $count++;
     }
     ?>
                                     </tbody>
                                </table>
                                 </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div>
    </section>

<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script>
<script type="text/javascript">
  $.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');

jQuery(function ($) {
    "use strict";
    $('#slider_form').validate({
      errorClass: 'errors',
        rules: {
            heading: {
                required: true,
                maxlength: 20
            },
            content: {
                required: true,
                maxlength: 250
            },
            userfile: {
                required: true,
                extension: "jpg,jpeg,png",
                filesize: 5,
            }
        },
         messages:{
          heading:{
            required:"Please Enter Title"
          },
          content:{
            required:"Please Enter Content",
            maxlength:"Cannot enter more than 250 charecter"
          },
          userfile:{
            required:"Please Select Valid Image Type"
          }
      }
    });
});
</script>
<script type="text/javascript">
  var status = "<?php echo $card[0]->status;?>";

  $(document).ready(function(){

    $('#toggle_event_editing button').click(function(){
  if($(this).hasClass('locked_active') || $(this).hasClass('unlocked_inactive')){
    /* code to do when unlocking */
        var on = $('#on').val();
        $.ajax({
          type:'post',
          data:{on:on},
          url:'<?php echo base_url();?>home/card_update',
          success:function(res){
            $('#switch_status').html('<b>Card is currently Visible.</b>');
          }
        });
  }else{
    /* code to do when locking */
        var off = $('#off').val();
        $.ajax({
          type:'post',
          data:{off:off},
          url:'<?php echo base_url();?>home/card_update',
          success:function(res){
           $('#switch_status').html('<b>Card is currently Hidden.</b>');
          }
        });
  }
  
  if(status == 0)
  {
      
  }
  else
  {
     
  } 
  $('#toggle_event_editing button').eq(1).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');
   $('#toggle_event_editing button').eq(0).toggleClass('locked_inactive locked_active btn-default btn-info');
});
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
        // "scrollY": 200,
        // "scrollX": true
} );
</script>