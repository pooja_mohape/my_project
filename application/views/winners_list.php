<style type="text/css">
table {
    table-layout: fixed;
    word-wrap: break-word;
}
</style>
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >

        </div>
    </div>
</div>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h3 style="margin-left:15px;">
        &nbsp;List Of Winner
    </h3>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div><!-- /.box-header -->
                <div class="box-body">
<?php
$attributes = array("method" => "post", "id" => "winner_form", "name" => "winner_form");
echo form_open_multipart('Home/upload_winners', $attributes);?>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                        <div class="col-lg-4">
                            <input name="title" type="text" id="title" class="form-control" value="" placeholder="Enter Title">

                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Name</label>
                        <div class="col-lg-4">
                            <input name="name" type="text" id="name" class="form-control" value="" placeholder="Enter Name">

                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>

                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Year</label>
                        <div class="col-lg-4">
                            <input name="year" type="text" id="year" class="form-control" value="" placeholder="Enter Year">

                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                        <div class="col-lg-4">
                            <textarea name="content" type="text" id="content" rows="4" class="form-control" name="content" placeholder="Enter Content"></textarea>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Image</label>
                        <div class="col-lg-4">
                            <input type="file" name="userfile" size="20" />
                        </div>

                    </div>
                    <br><br>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Company Logo</label>
                        <div class="col-lg-4">
                            <input type="file" name="logo" size="20"/>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                    <div class="col-lg-offset-4">
                        <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Save</button>
                        <a href="<?php echo base_url().'home/dashboard'?>">
                            <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                        </div>
                    </div>

<?php echo form_close();?>
<br><br>
                    <div style="overflow:auto;" >
                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Title</th>
                                    <th>Name</th>
                                    <th>Year</th>
                                    <th>Content</th>
                                    <th>Image</th>
                                    <th>Logo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
<?php

$query  = $this->db->query("SELECT * from list_of_winners");
$result = $query->result_array();
$count  = 1;

foreach ($result as $val) {?>
	                                <tr>
	                                    <td><?php echo $count?></td>
	                                    <td><?php echo $val['title'];?></td>
	                                    <td><?php echo $val['name'];?></td>
	                                    <td><?php echo $val['year'];?></td>
	                                    <td><?php echo $val['content'];?></td>
	                                    <td><img src="<?php echo base_url();?>uploads/<?php echo $val['img'];?>" style="width:70px; height: 60px;"></td>
	                                    <td><img src="<?php echo base_url();?>uploads/<?php echo $val['w_logo'];?>" style="width:70px; height: 60px;"></td>
	                                    <td><a class="btn btn-danger" onclick="return checkDelete();" href="<?php echo base_url();?>home/winners_delete/<?php echo $val['id'];?>" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>|<a class="btn btn-info" href="<?php echo base_url();?>home/edit_winners/<?php echo $val['id'];?>" title="Update"><i class="fa fa-edit"></i></a></td>
	                                </tr>
	<?php
	$count++;
}
?>
                        </tbody>
                    </table>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</section>


<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
        var con = confirm('Are you sure want to DELETE!!!');
        if(con){
            return true;
        }else{
            return false;
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#winner_form').validate({
            errorClass: 'errors',
            rules:{
                title:{
                    required:true
                },
                name:{
                    required:true
                },
                year:{
                    required:true
                },
                content:{
                    required:true
                },
                userfile:{
                    required:true
                }
            },
            messages:{
                title:{
                    required:"Please Enter Title Of Winner List"
                },
                name:{
                    required:"Please Enter Winner Name"
                },
                year:{
                    required:"Please Enter Year of Winner"
                },
                content:{
                    required:"Please Enter Content"
                },
                userfile:{
                    required:"Please Select Image"
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
// "scrollY": 200,
// "scrollX": true
} );
</script>