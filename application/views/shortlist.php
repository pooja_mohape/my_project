<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Add Shortlist Data
        </h3>     
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
              </div><!-- /.box-header -->
                  <div class="box-body">
                       <?php 
                       $attributes = array("method" => "post", "id" => "frmshortlist", "name" => "frmshortlist");
                       echo form_open_multipart('home/upload_shortlist',$attributes);?>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Title</label>
                        <div class="col-lg-4">                           
                            <input name="title" type="text" id="title" class="form-control" value="" required="" placeholder="Enter Title">
                              
                        </div>
                      </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">
                                <textarea name="content" type="text" id="content" class="form-control" rows="3" value="" required="" placeholder="Enter Content"></textarea>
                            </div>
                        </div>   
                             
                  </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" name="save_group_data" value="Upload" type="submit">Save</button> 
                               
                                  <a href="<?php echo base_url().'home/dashboard'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>
                         <?php echo form_close();?>
                        <div style="overflow:auto;" >
                          <table id="example" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>Sr.no</th>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php

                                $query = $this->db->query("SELECT * from shortlist_data");
                                $result = $query->result_array();
                                $count = 1; 

                                foreach ($result as $val){ ?>
                                <tr>
                                   <td><?php echo $count ?></td>
                                   <td><?php echo $val['title']; ?></td>
                                   <td><?php echo $val['content']; ?></td>
                                   <td><a class="btn btn-danger" onclick="return checkDelete()" href="<?php echo base_url(); ?>home/delete_shortlist/<?php echo $val['id'];?>" title="Delete"> <span class="glyphicon glyphicon-trash"></span></a>|<a class="btn btn-info" href="<?php echo base_url();?>home/edit_shortlist/<?php echo $val['id'];?>" title="Update"><i class="fa fa-edit"></i></a></td>
                                </tr>
                                <?php
                                $count++;
                                }
                               ?>
                            </tbody>
                          </table>  
                        </div>
                </div>
            </div>
      </div>
    </section>
<br><br>
                            
<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#frmshortlist').validate({
            errorClass: 'errors',
            rules:{
                title:{
                    required:true
                },
                content:{
                    required:true
                }
            },
            messages:{
                title:{
                    required:"Please Enter Title"
                },
                content:{
                    required:"Please Enter Content"
                }
            }
        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
        // "scrollY": 200,
        // "scrollX": true
} );
</script>
