<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;List Of Winner
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Title</th>
                                            <th>Name</th>
                                            <th>Year</th>
                                            <th>Content</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php

      $query = $this->db->query("SELECT * from list_of_winners");
      $result = $query->result_array();
      $count = 1; 

      foreach ($result as $val){ ?>
      <tr>
       <td><?php echo $count ?></td>
       <td><?php echo $val['title']; ?></td>
       <td><?php echo $val['name']; ?></td>
         <td><?php echo $val['year']; ?></td>
       <td><?php echo $val['content']; ?></td>
        <td><img src="<?php echo base_url();?>uploads/<?php echo $val['img'];?>" style="width:70px; height: 60px;"></td>
       <td><a href="<?php echo base_url(); ?>home/winners_delete/<?php echo $val['id'];?>">Delete</a>|<a href="<?php echo base_url();?>home/edit_winners/<?php echo $val['id'];?>">Update</a></td>
      </tr>
      <?php
      $count++;
     }
     ?>
                                     </tbody>
                                </table>
                               
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
          



<script type="text/javascript">
 function checkDelete(){
  return confirm('Are you sure want to DELETE ?');
 }
</script>