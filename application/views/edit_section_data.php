<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Latest News
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php echo form_open_multipart('home/edit_section_data');?>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Heading</label>
                        <div class="col-lg-4">                           
                            <input name="heading" type="text" id="heading" class="form-control" value="<?php echo $data[0]->title;?>" required="">
                               
                        </div>
                    </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Content</label>
                            <div class="col-lg-4">
                                <textarea name="content" type="text" id="content" class="form-control" style="height: 90px"><?php echo $data[0]->txt;?></textarea>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $data[0]->id;?>">
                        </div> 
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                        <label class="col-lg-3 control-label" for="name">Image</label>
                            <div class="col-lg-4">

                                <img src="<?php echo base_url();?>uploads/<?php echo $data[0]->img;?>" style="width:70px; height: 60px;"> 
                                 <?php echo $data[0]->img;?> 
                            </div>
                            <input type="hidden" name="id" value="<?php echo $data[0]->id;?>">
                        </div>  

                        <div class="clearfix" style="height: 10px;clear: both;"></div>   
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Image</label>
                            <div class="col-lg-4">
                               
                              <input type="file" name="userfile" size="20" />
                            </div>
                        </div>          
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                              
                                   <button class="btn btn-info" id="save_group_data" value="Upload" type="submit">Update</button> 
                               
                                  <a href="<?php echo base_url().'home/edit_section'?>">
                                  <button class="btn btn-danger back" id="back_data" type="button">Back</button> </a>
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
          