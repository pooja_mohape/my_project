<?php 
$response=$this->db->get('logo')->result();
 ?>
<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Logo
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3>Add Logo</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php 
                       $attributes = array("method" => "POST", "id" => "logo_form", "name" => "logo_form");
                       echo form_open_multipart('home/add_logo',$attributes);?> 
                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div>   
                            <label>Logo Upload</label>
                        </div>
                        <div class="form-group" align="center">
                            <input type="file" name="userfile" size="20" required/> 
                        </div>  
                        <br><br> 
                        <div class="col-lg-offset-5">
                              
                                   <button class="btn btn-info" id="save_group_data" value="Upload" type="submit">Save</button> 
                            </div>       
                    </div>
                         <?php echo form_close();?>
                         <br><br>
                         <div style="overflow-x: auto;">
                            <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Image</th>
                                                
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; foreach ($response as $key => $value) {?>
                        <tr>
                            <td><?php echo $i;?></td>
                          
                           <td><img src="<?php echo base_url();?>uploads/logo/<?php echo $value->img;?>" style="height: 60px;"></td>
                           
                            <td><a class="btn btn-danger" onclick="return checkDelete()" href="<?php echo base_url(); ?>home/delete_logo/<?php echo $value->id;?>" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>|<a class="btn btn-info" href="<?php echo base_url();?>home/update_logo/<?php echo $value->id;?>" title="Update"><i class="fa fa-edit"></i></a></td>
                        <tr>
                        <?php $i++;} ?>
                                     </tbody>
                                </table>
                            </div>
                               
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->
<!-- Confirmation Alert -->
<script type="text/javascript">
    function checkDelete(){
     var con = confirm('Are you sure want to DELETE!!!');
     if(con){
      return true;
     }else{
      return false;
     }
}
</script>