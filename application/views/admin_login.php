<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" dir="#" lang="#" xml:lang="#">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Admin Login</title>


<script type="text/javascript" > var site_url = 'https://www.teluscare.com/';</script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sitepanel/css/stylesheet.css" />

<script type="text/javascript" src="<?php echo base_url();?>assets/sitepanel/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sitepanel/js/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="<?php echo base_url();?>assets/sitepanel/js/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/sitepanel/js/jquery/tabs.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sitepanel/js/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/sitepanel/js/common.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js">

</script>

</head>

<body>
<div id="container">
   
  <div class="content"> 
  
<div class="box" style="width: 325px; min-height: 500px; margin-top: 40px; margin-left: auto; margin-right: auto;">

   
  <div class="heading"> 
      <h1><img src='<?php echo base_url()?>assets/sitepanel/image/lockscreen.png'>Please enter your Login details.</h1>
    </div>
  
    
  <div class="content" id="login" style="min-height: 150px;">
   <h1>Login</h1>
    
  <form action="<?php echo base_url().'home/login_authenticate'?>" method="post" accept-charset="utf-8">		
      
    <table style="width: 100%;">
      <tr>
        <td colspan="2" align="center"></td>
      </tr>
       <tr>
         <td rowspan="4" style="text-align: center;"><img src="<?php echo base_url();?>assets/sitepanel/image/login.png" alt="Please enter your login details." /></td>
      </tr>
        
      <tr>
        <td width="80%">Username:<br />
          <input type="text" name="username" value="" style="margin-top: 4px;" placeholder="username *" required="" />
		  <br />
          Password:<br />
          <input type="password" name="password" value="" style="margin-top: 4px;" placeholder="password*" required="" />
		  		  </td>
      </tr>
      <tr>
        <td align="left" >
          <input type="hidden" value="login" name="action"> 
          <input type="submit" name="sss" value="Login"  class="button2"/>
                    
        </td>
      </tr>
        
    </table>
      
 </form>    
</div>
</div> 
</div>
</div> 
</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>