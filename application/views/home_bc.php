<?php  $this->load->view('layout/header'); ?>
<?php $this->load->view('layout/slider');?>
<!-- <?php //$this->load->view('Award _categories/people_categories');?> -->
<section class="section background-color-light border-0 my-0" id="logo_section">
		<div class="container">
			<div class="row appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1300">
				<div class="col">
					<div class="owl-carousel owl-theme stage-margin custom-carousel-style-1 mb-0" data-plugin-options="{'items': 5, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
						<?php foreach ($logo as $key => $value) {?>
							<div>
							<img class="img-fluid" src="<?php echo base_url();?>/uploads/logo/<?php echo $value->img;?>" style="width:200px; height:80px;">
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<hr class="solid custom-opacity-2 mt-0">
	<section class="section background-color-light custom-padding-3 border-0 my-0" id="award_section">
		<div class="container">
			<?php foreach ($award as $key => $value) {?>
			<div class="row text-center">
				
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter"><?php echo $value->heading?></h2>
					<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200"><?php echo $value->title;?></p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400"><?php echo $value->paragraph;?></p>
				</div>
				
			</div>
			<?php } ?>
		</div>
	</section>
	<section id="counter_section">
		<div class="container">
			<hr style=" border:1px solid #fff">
			<div class="row text-center">
				<div class="col">			
				<div class="row counters">
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->No_of_Event) ? $counters[0]->No_of_Event : '';?>">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">No.of<br>Event</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->successful_event) ? $counters[0]->successful_event : '';?>" data-append="%" data-append="%">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful Event<br>Ratio</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->successful_years) ? $counters[0]->successful_years : '';?>" data-append="+">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful<br>Years</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo isset($counters[0]->dedicated_client) ? $counters[0]->dedicated_client : '';?>">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Clients</label>
						</div>
					</div>
				</div>
				</div>
			</div>
			<hr style=" border:1px solid #fff">
		</div>
	</section>
	<section class="section background-color-light custom-padding-3 border-0 my-0" id="award_methodology">
		<div class="container">
			<div class="row">
			 <?php $c=0; foreach ($block as $key => $value) {?>
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter"><?php echo $value->title;?></h2>
					<div class="align-self-center custom-primary-font custom-fontsize-2 font-weight-normal appear-animation animated fadeInUpShorter appear-animation-visible">
						<p><?php echo $value->content; ?></p>
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</section>
	<section class="section background-color-quaternary custom-padding-3 border-0 my-0" id="winners_list">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					<h2 class="align-self-center custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">Winners List</h2>
					<div class="row align-items-center">
						<div class="col-lg-2 mb-4 mb-lg-0">
							<div class="tabs tabs-vertical tabs-left tabs-navigation custom-tabs-navigation-1 border-0 mb-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<ul class="nav nav-tabs col-sm-3">
							 <?php foreach($year as $yer){?>
									<li class="nav-item mb-0 year" pgt1="<?php echo $yer->year;?>" style="cursor: pointer;">
										<a class="nav-link custom-primary-font text-center"  data-toggle="tab"><?php echo $yer->year;?></a>
									</li>
									<?php }?>
								</ul>
							</div>
						</div>
						<div class="col-lg-10 mb-4 mb-lg-0">
							<div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabsNavigation1">
								<div class="row align-items-center" id="tabsNavigation">
									<?php foreach($winner_list as $list){?>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">

												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/uploads/<?php echo $list->img;?>" class="rounded-circle img-center avatar" alt=""/>
													</div>
													<div class="col-lg-9">
														<blockquote >
															<p class="mb-3 img-center"><?php echo substr($list->content, 0,150);?></p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0"><?php echo $list->name;?></strong>
														</div>
													</div>
												</div>
											</div>
											
										</div>	
									</div>
								<?php }?>
									
								</div>

							</div>
								   <div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabs_nav" style="display:none;">
								   </div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group col-md-12 col-md-offset-6">
				<input type="button" value="View All" class="btn btn-secondary custom-btn-style-1 text-uppercase img-center all_view"
				data-toggle="modal" pgt="<?php echo $winner_list[0]->year;?>">
			</div>
		</div>
	</section>	
<!--  model5 start -->
<div class="modal fade" id="myModal5" role="dialog">
 <?php if(count($year) > 1){?>
<div class="paddle previous all_view all1" pgt="">
    <a role="button" tabindex="0">
        <div class="headline">
            <div>
                <div class="truncate previous1"></div>
            </div>
        </div>
    </a>
</div>
<?php }if(count($year) > 1){ ?>
<div class="paddle next all2 all_view" pgt="">
    <a role="button" tabindex="0">
        
        <div class="headline ">
            <div>
                <div class="truncate next1"></div>
            </div>
        </div>
    </a>
</div>
<?php }?>
<div class="modal-dialog" id="list-modal">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title winlist"></h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="row" id="test1">
                
            </div>
        </div>
        <div class="modal-footer">
            
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<!-- Modal -->

	<section class="section background-color-light custom-padding-1 border-0 my-0" id="awards_shortlist">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter" style="color: green">Awards Shortlist</h2>
					<div class="owl-carousel owl-theme" data-plugin-options="{'items': 4, 'autoplay': true, 'autoplayTimeout': 3000}">
						<?php $c=0; foreach ($shortlist as $key => $value) {?>
						<div class="abc">
							<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0"><?php echo $value->title;?></h2>

							<div class="feature-box-info pl-0 pr-3 pqr_data">
								<p class="mb-3"><?php echo substr($value->content,0,250);?></p>
								<?php if(strlen($value->content)>200) {?>
								<button type="button" class="btn btn-link pull-right view_more_data" id="view" pgt="<?php echo $value->content;?>">View more <i class="fas fa-long-arrow-alt-right"></i></button>
								<?php }?>
							</div>
							<div class="zmr_data" id="zmr" style="display: none;">
	                   		
		                    <p class="mb-3"><?php echo $value->content;?></p> 
		                    <button type="button" class="btn btn-link pull-right view_less_data">Less<i class="fas fa-long-arrow-alt-top"></i></button>
                		
          			</div>
				</div>
					<?php }?>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="parallax section section-parallax parallax-background custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" id="contact">
	<div id="googleMap" style="position: relative;overflow: hidden;position: absolute;top: 0px;left: 0px;width: 100%;height: 120%;transform: translate3d(0px, -62.5402px, 0px);background-position-x: 50%;">
	</div>
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-md-9 col-lg-7 col-xl-6">
				<div class="card background-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
					<div class="card-body p-5">
						<h2 class="custom-primary-font font-weight-normal text-6 line-height-sm mb-3">Contact Us</h2>
						<div class="row">
							<div class="col-md-6"><p class="pb-2 mb-4"><?php echo $contact_us[0]->address;?></p></div>		
							<div class="col-md-6 mb-4">
								<button type="button" class="btn btn-primary text-uppercase custom-btn-style-1" data-toggle="modal" data-target="#venue_info">Travel Info</button>
							</div>
						</div>
						<form action="<?php echo base_url();?>home/contact_us" method="post">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
								    <span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								  <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Name" aria-label="Username" aria-describedby="basic-addon1" required>
							</div>

							<div class="input-group mb-3">
								<div class="input-group-prepend">
							    	<span class="input-group-text"><i class="fas fa-envelope"></i></span>
								</div>
								  <input type="text" class="form-control" name="email" id="email" placeholder="Enter your Email" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
							</div>

							<div class="input-group mb-3">
								<div class="input-group-prepend">
								    <span class="input-group-text"><i class="fas fa-edit"></i></span>
								</div>
								  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
							</div>

							<div class="input-group">
								<div class="input-group-prepend">
								    <span class="input-group-text"><i class="fas fa-comment-alt"></i></span>
								</div>
								  <textarea class="form-control" aria-label="With textarea" name="message"  id="message" placeholder="Write Your Message" required></textarea>
							</div>

							<div class="input-group">
								  <button type="submit" id="send" class="btn btn-success btn-lg my-4 custom-btn-style-1 text-uppercase">Send</button> 
							</div>

						</form>
					</div>
					<div class="card-footer background-color-primary border-0 custom-padding-2">
						<div class="row">
							<div class="col-md-6 mb-5 mb-md-0">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
										<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->email;?></a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="fas fa-phone text-3"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
										<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2"><?php echo $contact_us[0]->call;?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section background-color-quaternary custom-padding-3 border-0 my-0" id="latest_news">
	<div class="container">
		<div class="row mb-4">
			<div class="col">
				<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-0 appear-animation" data-appear-animation="fadeInUpShorter">LATEST NEWS</h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php foreach ($data as $key => $value) {?>
				<div class="col-md-6 col-lg-4 mb-5 mb-lg--1 xyz">
					<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
						<div class="thumb-info-wrapper m-1">
							<img src="<?php echo base_url();?>uploads/<?php echo $value->img;?>" style="width:340px; height: 180px;">
						</div>
					<div class="pqr">
						 <div class="thumb-info-caption custom-padding-4 d-block cls_hd visible_text" id="visible_text" >
							<span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo $value->title;?></span>
							<span class="thumb-info-caption-text line-height-md text-3 p-0 m-0"><?php echo substr($value->txt,0,21);?>  
						<?php if(strlen($value->txt)>21) {?>
							<button type="button" class="btn btn-secondary pull-right view_more" id="view" pgt="<?php echo $value->txt;?>"  style="border-radius: 40px">view more</button></span>
							<?php }?>
						</div>
					</div>

					<div class="zmr" id="zmr" style="display: none;">
	                   	<div class="thumb-info-caption custom-padding-4 d-block hidden_text" style="">
		                    <span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo $value->title;?></span>
		                    <span class="thumb-info-caption-text line-height-md text-3 p-0 m-0"><?php echo $value->txt;?></span>
		                    <center>
		                    <button type="button" class="btn btn-secondary view_less" style="border-radius: 40px">less</button></center>
                		</div>
          			</div> 

					</article>
				</div>
				<?php } ?>
			</div>
			<div class="row text-center mt-5">
				<div class="col">
					<a href="#" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-2">VIEW ALL</a>
				</div>
			</div>
		</div>
</section>
	

<!-- modal start for award Category-->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<!--1st right Side Click(project) -->

		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal1s">

				<div class="headline">
					<div>
						<div class="truncate">Project Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
				 <?php foreach ($people_cat as $key => $people_cat) {?> 

				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $people_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
							<?php echo $people_cat->content;?>
						
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<!-- php end here -->
			</div>
			<?php }?>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal1" role="dialog" style="overflow-y: auto;">
		<!--2nd right Side Click(company) -->

		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal2s">
				<div class="headline">
					<div>
						<div class="truncate">company Category</div>
					</div>
				</div>
			</a>
		</div>

		<!--1st left Side Click(people) -->

		<div class="paddle previous">
			<a role="button" tabindex="0" id="myModal3s">

				<div class="headline ">
					<div>
						<div class="truncate">People Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
			 <?php foreach ($proj_cat as $key => $proj_cat) {?>
				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $proj_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
							<?php echo $proj_cat->content;?>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
	<!--Modal-->
	<div class="modal fade" id="myModal2" role="dialog" style="overflow-y: auto;">
		<!--3rd right Side Click(people) -->


		<div class="paddle next">
			<a role="button" tabindex="0" id="myModal4s">

				<div class="headline">
					<div>
						<div class="truncate">People Category</div>
					</div>
				</div>
			</a>
		</div>

		<!--2nd left Side Click(project) -->


		<div class="paddle previous">
			<a role="button" tabindex="0" id="myModal5s">

				<div class="headline">
					<div>
						<div class="truncate">Project Category</div>
					</div>
				</div>
			</a>
		</div>

		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
			 <?php foreach ($company_cat as $key => $company_cat) {?>

				<div class="modal-header">
					<h5 class="modal-title appear-animation animated fadeInUpShorter appear-animation-visible"><?php echo $company_cat->title;?></h5>
					<button type="button" class="btn btn-default justify-content-end" data-dismiss="modal">X</button>
				</div>

				<div class="container d-flex h-100">
					<div class="modal-body">
						<div class="row mb-5 px-5 justify-content-center align-self-center appear-animation animated fadeInUpShorter appear-animation-visible">
					<?php echo $company_cat->content;?>
							
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>


	<!-- modal start  for venue_info -->
<div class="modal fade" id="venue_info" role="dialog">
	<div class="modal-dialog"> 
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Venue Information</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<p><strong>Brief History</strong><br /> Headquarters of the <a href="https://www.ice.org.uk/" target="_blank">Institution of Civil Engineers</a> (ICE), this historic Grade II listed building was designed by James Miller, the world's first professional engineering body, and constructed between 1910 and 1913. There are memorials around the building for civil engineers who fought and died during the First World War. The building was used for night-time fire watching on the roof during the Second World War. It was also hit a couple of times during the Blitz; fortunately, both bombs burnt out before any damage could be done.</p>
				<p><strong>Travel Information</strong></p>
				<p><strong>By London Underground</strong><br /> Both Westminster (Exit 6) on the District, Circle and Jubilee lines, and St James's Park on the District and Circle lines, are just 5 minutes walk away. For more information, visit the <a href="https://tfl.gov.uk/" target="_blank">Transport for London website</a>.</p>
				<p><strong>By Rail</strong><br /> London's mainline stations at Waterloo, Victoria and Charing Cross are minutes away by foot or by Underground. For further information visit the <a href="http://www.nationalrail.co.uk/" target="_blank">National Rail website</a>.</p>
				<p><strong>By Bus</strong><br /> Numerous bus routes pass within a 1-2 minute walk of One Great George Street. Bus routes stopping near Parliament Square include: 3, 11, 12, 24, 53, 87, 88, 148, 159, 211 and 453. For further infor-mation visit the <a href="http://www.tfl.gov.uk/modalpages/2605.aspx">Transport for London website</a>.</p>
				<p><strong>GPS co-ordinates</strong><br /> Main entrance – 51 30.0822 N and 0 07.7500 W and Princes Mews – 51 30.0420 N and 0 07.7185 W</p>
				<p><strong>By Car</strong><br /> The nearest car parks provided by <a href="http://www.q-park.co.uk/parking/london/q-park-westminster" target="_blank">Q-Park</a> are located in Abingdon Street and on the south side of Tra-falgar Square. Limited meter parking is also available in and around adjacent streets.</p>
				<p><strong>Disabled Parking</strong><br /> The nearest Blue Badge disabled parking bay is located on Matthew Parker Street (drive down Storey's Gate to the west of the venue and take the second right). Alternatively, a little further away, there are bays on Old Queen Street and Queen Anne's Gate. These Blue Badge bays offer free parking for up to 4 hours between 08.30 and 18.30 Mon-Fri and unlimited parking at any other time. Please see <a href="https://www.westminster.gov.uk/disabled-parking" target="_blank">Westminster Council's website</a> for further information.</p>
				<p><strong>Congestion Charge</strong><br /> One Great George Street lies within the charging area. For more information, including charges, visit the <a href="https://tfl.gov.uk/modes/driving/congestion-charge?cid=pp020" target="_blank">Transport for London website</a>.</p>
				<p><strong>By River</strong><br /> A <a href="https://tfl.gov.uk/?cid=pp004" target="_blank">River Bus Service</a> runs every 20 minutes during peak time from Embankment to Woolwich Arsenal; the nearest pier to the venue is the London Eye.</p>
				<p><span style="color:#642a76;"><strong>ACCOMODATION OPTIONS:</strong></span></p>
				<p><strong><a href="http://conradhotels3.hilton.com/en/hotels/united-kingdom/conrad-london-st-james-LONCOCI/index.html" target="_blank">CONRAD LONDON ST JAMES</a></strong><br /> Conrad London St James is just a stone's throw away from One Great George Street. Book by visiting their <a style="color: #003777;" href="http://conradhotels3.hilton.com/en/hotels/united-kingdom/conrad-london-st-james-LONCOCI/index.html" target="_blank">website</a> or by telephone on +44 (0)203 301 8080 and quote 'One Great George Street'.</p>
				<p><strong><a href="http://www.sterminshotel.co.uk/" target="_blank">ST ERMIN'S HOTEL</a></strong><br /> The St. Ermin’s hotel is a four-minute walk away from One Great George Street. Book by visiting their <a style="color: #003777;" href="http://www.sterminshotel.co.uk/" target="_blank">website</a> or by telephone on +44 (0)207 227 4816 and quote 'One Great George Street'.</p>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- modal end for venue_info -->

	<!-- modal for nomination form -->
<!-- nomi Modal -->
<input type="hidden" name="" id="hid1" value="<?php echo isset($mod1[0]->longitude) ? $mod1[0]->longitude :'';?>">
<input type="hidden" name="" id="hid2" value="<?php echo isset($mod1[0]->latitude) ? $mod1[0]->latitude : '';?>">



<!-- modal for nomination form -->
	<!-- nomi Modal -->
	<div class="modal fade" id="nomi" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><span style="color:#013975;"><strong><em><?php echo $mod1[0]->title; ?></em></strong></span></h4>
					<button type="button" class="close" data-dismiss="modal">X</button>
				</div>
				<div class="modal-body">
					<h4 style="color:#007bff"><?php echo $mod1[0]->heading;?></h4>

					<div class="appear-animation animated fadeInUpShorter appear-animation-visible">	
						<div id="mygMap" style="width:100%;height:200px;"></div>
					</div>

					<div class="card-body">
						<div class="row appear-animation animated fadeInUpShorter appear-animation-visible">
							<div class="col-sm-2 col-md-2">
								<i class="fas fa-map-marker fa-5x"></i>
							</div>
							<div class="col-sm-10 col-md-10">
								<p><span><?php echo $mod1[0]->content;?></span></p>
								<p><span><?php echo $mod1[0]->nomination_content;?> <a href="#">here</a>.</span></p>
							</div>
						</div>
					</div>

					<div class="card-footer background-color-primary border-0">
						<div class="row">
							<div class="col-sm-5 mb-0 py-2">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
										<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2"><?php echo $mod1[0]->email;?></a>
									</div>
								</div>
							</div>
							<div class="col-sm-5 mb-0 py-2">
								<div class="feature-box align-items-center">
									<div class="feature-box-icon background-color-tertiary">
										<i class="fas fa-phone text-3"></i>
									</div>
									<div class="feature-box-info">
										<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
										<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2"><?php echo $mod1[0]->phone;?></a>
									</div>
								</div>
							</div>
							<div class="col-md-2 mb-0 py-2" style="position: relative; text-align: center;">
								<button type="button" id="nomi_rule" class="btn btn-outline-light rounded" data-target="#rule_nom" data-toggle="modal">Next</button>
								<!-- <div class="feature-box align-items-center">
									<button type="button" id="nomi_rule" class="btn btn-success pull-left" data-target="#rule_nom" data-toggle="modal">NEXT</button>
								</div> -->
							</div>
						</div>
					</div>
				</div>
<!-- <div class="modal-footer">
<button type="button" id="nomi_rule" class="btn btn-success pull-left" data-target="#rule_nom" data-toggle="modal">NEXT</button>
</div> -->
</div>
</div>
</div>
<!-- nomi_f form -->
<div class="modal fade" id="rule_nom" role="dialog" style="overflow-y:auto;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><strong><?php echo $mod2[0]->title; ?></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="card">
						<div class="card-body">
							<p><span style="font-size:15px"><strong><?php echo $mod2[0]->heading;?> </strong></span></p>

							<ul class="custom-primary-font appear-animation animated fadeInUpShorter appear-animation-visible">
								<?php

									foreach ($mod2 as $key) {?>
										<li><?php echo $key->content;?></li>
								<?php	
								}?>
								
							</ul>
							<p><span><a href="#"></a><?php echo $mod2[0]->nomination_content;?></span></p>

							<button type="button" id="nomi_detail" class="btn btn-outline-light rounded"  data-target="#nomi_form" data-toggle="modal">Start Fill Details</button>
						</div>
					</div>
				</div>

				<div class="modal-footer" style="background-color: #1b9dff;">
					<button type="button" id="nomi_detail" class="btn btn-outline-light rounded"  data-target="#nomi_form" data-toggle="modal">Start Fill Details</button>

					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- nomi details -->
<div class="modal fade" id="nomi_form" role="dialog" style="overflow-y:auto;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nomination Form</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="appear-animation animated fadeInUpShorter appear-animation-visible">
					<form id="formRender" action="<?= base_url(); ?>survey/formdata" method="post" enctype="application/json">
			
					</form>
				</div>
			</div>

			<div class="modal-footer">
				 <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  -->
			</div>
		</div>
	</div>
</div>
<!-- modal for statistics -->
<div class="modal fade" id="statistics" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">statistics View</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row text-center">		
					<div class="row counters">
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="28">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">No.of<br>Event</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="99" data-append="%">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Successful Event<br>Ratio</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-sm-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="18" data-append="+">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Successful<br>Years</label>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 mb-5 mb-lg-0">
							<div class="counter">
								<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="1200">0</strong>
								<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Clients</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- modal end for nomination form -->




	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Fill Survey Form -->
		
<script type="text/javascript">
	
	var jsonData = <?php echo $dt['0']->questionary?>;
	var survey_id = <?php echo $dt['0']->id?>;

</script>


<style type="text/css">
	div #exactFit {
    word-wrap: break-word;
}
</style>

<style type="text/css">

</style>