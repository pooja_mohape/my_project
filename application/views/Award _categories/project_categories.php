<section class="section background-color-light custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row text-center mb-5">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">Projects &amp; Programme Categories</h2>
				</div>
			</div>
			<div class="row mb-5">
				<p><strong>AWARD: CLEANER ENERGY INITIATIVE OF THE YEAR </strong><br /> Open to all energy companies, government ministries and other official bodies, as well as agencies, in both the public and private sectors. This award will recognise an organisation which has devised, developed and implemented an initiative to reduce its ecological footprint, either at a broad level or within a specific business unit. Entries should provide evidence of:</p>
<ul>
<li>The policies implemented and action taken</li>
<li>The results of these actions</li>
<li>Active promotion of cleaner energy, to employees, stakeholders and the wider public</li>
<li>Provisions for the reduction of pollution across operations</li>
<li>Plans for further reducing the company’s/agency’s carbon footprint</li>
</ul>
<p><strong>AWARD: CORPORATE SOCIAL RESPONSIBILITY (CSR) PROGRAMME OF THE YEAR</strong><br /> Open to all energy companies with active operations. This award will recognise an exemplary ongoing CSR programme, with demonstrable positive impact on the communities it serves. Entries should provide evidence of:</p>
<ul>
<li>Integration of CSR into the company’s business culture</li>
<li>A particular campaign or project, which exemplifies the company’s commitments to CSR</li>
<li>Full consideration of human rights, labour welfare, health and security employees, local economic and community development and environmental protection</li>
<li>Ongoing commitment to successful CSR practices in future years</li>
</ul>
<p><strong>AWARD: PROJECT OF THE YEAR </strong><br /> Open to all companies across the energy industry value chain. This award will recognise a project, brought on stream within the judging period, which has particular merit, will have an impact on the industry, involved specific expertise or sets a new standard in its sector. Companies are requested to nominate specific projects. Entries should provide evidence of:</p>
<ul>
<li>Excellence and ingenuity</li>
<li>Sustainability and careful consideration of environmental issues</li>
<li>A best-practice approach of health and safety</li>
<li>Meeting budget and time constraints</li>
</ul>
<p><strong>AWARD: STAKEHOLDER COMMUNICATION PROGRAMME OF THE YEAR </strong><br /> Open to all energy companies. This award will recognise an innovative communications programme designed primarily for stakeholders, and which has had a demonstrable impact on stakeholder relations and project investment. This includes companies where the majority stakeholder is a government entity. Entries should provide evidence of:</p>
<ul>
<li>Use of innovative strategies and methods</li>
<li>Consistency in conveying the company’s vision and values throughout the campaign(s)</li>
<li>Ensuring accessibility and accuracy of information</li>
<li>An increase in investor confidence as a result of the highlighted communication campaign(s)</li>
</ul>

			</div>
			<div class="row text-center">
				<div class="col">
					<a href="#" class="btn btn-secondary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">GET A QUOTE TODAY</a>
				</div>
			</div>
		</div>
	</section>
