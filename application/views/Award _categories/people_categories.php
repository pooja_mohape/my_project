<section class="section background-color-light custom-padding-3 border-0 my-0">
	<div class="container">
		<div class="row text-center mb-5">
			<div class="col">
				<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">People Categories</h2>
			</div>
		</div>
	<div class="row mb-5">
		<p><strong>AWARD: CHIEF EXECUTIVE OF THE YEAR </strong><br /> Open to all chief executives of energy companies. This award will recognise a chief executive who has made an outstanding contribution to the company they lead, as well as to the wider industry; candidates are respected by staff, investors, and competitors alike. Individuals can nominate themselves or be nominated by a colleague. Entries should provide evidence of the nominee’s:</p>
		<ul>
		<li>Demonstrable long-term commitment to the energy industry</li>
		<li>Excellent leadership skills</li>
		<li>Impressive company results</li>
		<li>An innovative approach to tackling business challenges</li>
		<li>Commitment to employee and community development</li>
		</ul>
		<p><strong>AWARD: ENERGY EXECUTIVE OF THE YEAR </strong><br /> Open to all industry professionals. This award will recognise an executive who has made – or who has demonstrable potential to make – a major contribution to the industry, either regionally or globally, regardless of age or position within the company. Individuals can nominate themselves or be nominated by a colleague. Entries should provide evidence of the nominee’s:</p>
		<ul>
		<li>Skills, acumen and ability</li>
		<li>Recognition and respect within their industry sector, both within and without their company</li>
		<li>Close involvement in or leadership of a range of successful projects/initiatives which have the potential to have an impact on the wider energy industry</li>
		<li>Demonstrable commitment to the energy industry’s development</li>
		</ul>
		<p><strong>AWARD: FUTURE LEADER </strong><br /> This award aims to identify the industry’s rising stars. Open to professionals across all industry sectors and disciplines, aged under 40 at the end of the scheme’s judging period, the award seeks to identify those people who are most likely to shape the industry’s future in the years ahead. Individuals can nominate themselves or be nominated by a colleague. Entries should provide evidence of the nominee’s:</p>
		<ul>
		<li>Commitment to the sustainable, long-term future of the energy industry</li>
		<li>Potential to take on a leadership role and fulfil the demands of that role</li>
		<li>Outstanding leadership skills and demonstrable understanding of the challenges facing the industry in the 21st century and willingness to tackle those challenges.</li>
		</ul>
		<p><strong>AWARD: MINISTER OF THE YEAR </strong><br /> Open to all energy ministers who have held the portfolio for at least six months by the end of the scheme’s judging period. This award will recognise an energy minister who has made a significant contribution to their country’s energy sector, earning the respect of their peers and ministry staff, as well as domestic and international companies operating in the country. Individuals can nominate themselves or be nominated by a colleague. Entries should provide evidence of the nominee’s:</p>
		<ul>
		<li>Demonstrable commitment to the development of the energy industry</li>
		<li>Sound understanding of energy issues and their impact both domestically and internationally</li>
		<li>Sound strategic thinking and commitment to sustainable policy initiatives</li>
		</ul>
		<p>Evidence of improvement and/or innovation within the country’s energy sector is also an advantage.</p>
		<p><strong>AWARD: LEGACY AWARD </strong><br /> This award will honour a person who has made an outstanding contribution to the energy industry. This award is not tied to the judging period but rather considers the individual’s commitment to, and work for, the industry throughout their career. The individual need not have spent their whole career within the industry, but must have contributed significantly to the industry’s development. Individuals, agencies, ministries and companies are able to nominate candidates. Nominations should include evidence of:</p>
		<ul>
		<li>A demonstrable long-term commitment to the energy industry</li>
		<li>Excellent leadership skills</li>
		<li>A commitment to innovation</li>
		<li>A commitment to improving operations, whether that be within a company, sector or across the sector as a whole</li>
		</ul>
	</div>
		<div class="row text-center">
			<div class="col">
				<a href="#" class="btn btn-secondary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">GET A QUOTE TODAY</a>
			</div>
		</div>
	</div>
</section>
