<!--  <section>
 	<div class="row" style="padding: 50px;">
 		<div class="col-md-10 col-sm-10 col-md-offset-1 col-sm-offset-1 col-xs-12 text-center">
 			<h3 class="text-center"><strong>Survey Form</strong></h3><hr>
 			<div class="formRender" id="formRender">
 			</div>
 			<div class="row">
 				<div class="col-md-6 col-sm-6 col-md-offset-3 ">
 					<div class="formRender" id="formRender">
 					</div>
 				</div>
 			</div> 
 		</div>
 	</div>
 </section> -->	
 <section class="section background-color-light custom-padding-3 border-0 my-0" id="survey_form" style="background-color: red">
 		<div class="container">
			<div class="row text-center">
				<div class="formRender" id="formRender">
 				</div>
			</div>
		</div>
 </section>
 <script type="text/javascript">
		
	var jsonData = <?php echo $dt['0']->questionary?>;

	$(document).ready(function(){

		 var formData = jsonData,
	 	  
	 	  formRenderOpts = {
	      dataType: 'json',
	      formData: formData
	    };
	  
	  var renderedForm = $('<div>');
	  renderedForm.formRender(formRenderOpts);
	  $('#formRender').append(renderedForm.html());
	});

 </script>