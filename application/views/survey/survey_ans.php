<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Survey Answer
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                         <br><br>
                        <div style="overflow-x:auto;width:100%;">
                            <table class="table table-bordered table-hover table-responsive" id="example">
                                <thead>
                                    <tr>
                                        <th>Sr.no</th>
                                        <th>User Id</th>
                                        <th>Survey Name</th>
                                        <th>Answer</th>
                                        <th>Draft Answer</th>
                                        <th>IP Address</th>
                                        <th>Browser</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                        
                                     <?php 

                                      $i=1;
                                      $normVal   ='';
                                      $draft_ans ='';
                                      $ids='';
                                        foreach ($ans as $key => $value) 

                                         { $ids++;
                                          $normVal='';
                                          $draft_ans='';?>
                                            <tr>
                                                <?php $ans_json = json_decode($value->answer);
                                                  $draft_json   = json_decode($value->draft_answer);
                                                
                                                if($ans_json)
                                                 {
                                                    foreach ($ans_json as $key1 => $value1) 
                                                    {

                                                        if(is_array($value1)) 
                                                        {
                                                            
                                                            foreach ($value1 as $key => $value2) 
                                                            {
                                                                
                                                                $normVal .=$value2.',';
                                                            }
                                                        }
                                                        else 
                                                        {
                                                            $normVal .=$value1.',';
                                                        }
                                                    } 
                                                  }
                                                $normVal = rtrim($normVal,',');

                                                if($draft_json)
                                                 {
                                                  foreach ($draft_json as $key1 => $value1) 
                                                      {

                                                          if(is_array($value1)) 
                                                          {
                                                              
                                                              foreach ($value1 as $key => $value2) 
                                                              {
                                                                  
                                                                  $draft_ans .=$value2.',';
                                                              }
                                                          }
                                                          else 
                                                          {
                                                              $draft_ans .=$value1.',';
                                                          }
                                                      }
                                                  } 
                                                $draft_ans = rtrim($draft_ans,',')
                                            ?>
                                               <td><?php echo $i;?></td>
                                               <td><?php echo $value->user_id;?></td>
                                               <td><?php echo $value->name;?></td>
                                               <td><?php echo $normVal;?></td>
                                               <td><?php echo $draft_ans;?></td>
                                               <td><?php echo $value->ip;?></td>
                                               <td><?php echo $value->browser;?></td>
                                               <td><?php echo $value->country;?></td>
                                               <td><?php echo $value->city;?></td>
                                               <td><?php echo date('d M Y',strtotime($value->endDate));?></td>
                                               <td><?php echo date('d M Y',strtotime($value->endDate));?></td>
                                               <td><button  class="btn btn-info view" id="view<?php echo $ids;?>" data-toggle="modal" data-target="#myModal" pgT='<?php echo $value->questionary;?>' pgT1="<?php echo $normVal;?>" pgT2='<?php echo $draft_ans;?>'><i class="fa fa-eye"></i>&nbsp;View</button></td>
                                            </tr>
                                <?php $i++;} ?>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section> 
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1></h1>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <h4 id ="ttl" style="background-color: #E5E9EC;">Survey Question
                <span class="pull-right">User Answer</span></h4>
             </div>
          </div>
            <div class="col-md-8">
                <form action="#" enctype="application/json">
                  
                </form>
            </div>
             <div class="col-md-4">
              <!-- <h4>Survey Answer</h4> -->
                <div class="form-group">
                    <p name="usr_ans" id="usr_ans" style="padding: 20px;"></p> 
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        </div>
      </div>
      </div>
  </div>       
<script type="text/javascript">
$(document).ready(function() {
    //$('#example').DataTable();

    $(".view").click(function(){

        var usr_ans='';
        var survey_form='';

        $('form').empty();
        $('#usr_ans').empty();

        $('form').empty();

        // survey_form = $(this).attr("pgT");
        // usr_ans     = $(this).attr('pgT1');

        // if(usr_ans=='') 
        // {
        //    usr_ans  = $(this).attr('pgT2');
        // }

          survey_form = '[{"type":"radio-group","label":"Radio Group","name":"radio-group-1528800193899","values":[{"label":"Option 1","value":"option-1"},{"label":"Option 2","value":"option-2"},{"label":"Option 3","value":"option-3"}]},{"type":"checkbox-group","label":"Checkbox Group","name":"checkbox-group-1528800203655","values":[{"label":"Option 1","value":"option-1","selected":true},{"label":"Option 2","value":"Option 2"},{"label":"Option 3","value":"Option 3"}]},{"type":"text","label":"Text Field","className":"form-control","name":"text-1528800236575","subtype":"text"},{"type":"select","label":"Select","className":"form-control","name":"select-1528800258696","values":[{"label":"Option 1","value":"option-1","selected":true},{"label":"Option 2","value":"option-2"},{"label":"Option 3","value":"option-3"}]},{"type":"number","label":"Number","className":"form-control","name":"number-1528800291839"}]';

          usr_ans     = '{"radio-group-1528800193899":"option-1","checkbox-group-1528800203655":["option-1"],"text-1528800236575":"1","select-1528800258696":"option-1","number-1528800291839":"1"}';

        var formData  = survey_form;
        var survey_dd = json_decode(survey_form);
        var usr_dd    = json_decode(usr_ans);
      
      formRenderOpts = {
        dataType: 'json',
        formData: formData
      };

    var renderedForm = $('<div>');
    renderedForm.formRender(formRenderOpts);
    $('form').append(renderedForm.html());
    $("form").val(formData);
    $('form').find('input, textarea, button, select').attr('disabled','disabled');
    console.log(survey_dd);
    console.log(usr_dd);
    // $("#usr_ans").append(usr_ans);
    // $('#usr_ans').css("padding","20px");
    // $('#ttl').css("padding","20px");
    });

} );


</script>