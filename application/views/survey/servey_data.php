<div class="box" style="display: none;">
    <div class="row">
        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12" >
          
        </div>
    </div>
</div>
   <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="margin-left:15px;">
            &nbsp;Survey List
        </h3>     
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>

                         </form>
                         <br><br>
                         <div style="overflow-x:auto;">
                            <table class="table table-bordered table-hover table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Sr.no</th>
                                            <th>Name</th>
                                            <th>StartDate</th>
                                            <th>EndDate</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $i=1; foreach ($data as $key => $value) {?>
                                     
                            <tr>
                            <td><?php echo $i;?></td>
                           <td><?php echo $value->name;?></td>
                           <td><?php echo $value->startDate;?></td>
                           <td><?php echo $value->endDate;?></td>
                            <td> <div class="radio">
            <label>
            <?php if($value->live=='Y'){?>
            <input type="radio" name="radio" id="btn_radio" class="myCheckbox" checked value="<?= $value->id; ?>" />
            <?php } else{?>
             <input type="radio" name="radio" id="btn_radio" class="myCheckbox" value="<?= $value->id; ?>" />
             <!-- <div class="hoverme" style="display: none;">Activate survey</div> -->
             <?php }?>
            </label>
        </div></td>
                        <tr>
                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                               
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
                <!-- end:content -->

<script type="text/javascript">
    $(document).ready(function(){

          var msg_type = "<?php echo $this->session->flashdata('msg_type'); ?>";
          var msg      = "<?php echo $this->session->flashdata('msg'); ?>";
         
          if(msg_type != "" && msg != "")
          {
             $.bootstrapGrowl(msg, { type: msg_type });
          }

        $('input[type=radio][name=radio]').change(function(){
             
             if($(this).is(":checked")) {
                
             var x = confirm("Are You Sure To Make It Live");
            }

            if(x) {
            
                 var dt = $('input[type=radio][name=radio]:checked').val();
                
                    $.ajax({
                        url:"<?= base_url();?>survey/livesurvey",
                        type:"POST",
                        data:{dt:dt},
                        success:function(res) {

                            //alert('Survey Successfully Updated');
                           var rsp        = JSON.parse(res);
                           var msg_type = rsp.msg_type;
                           var msg      = rsp.msg;

                           if(msg_type && msg)
                           {
                                $.bootstrapGrowl(msg, { type: msg_type });
                           }
                        }
                    });
                }
                else
                {
                    window.location.reload();
                }
            });
        });

</script>          
 
 <script>
     $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
 </script>

<!-- <script type="text/javascript">
    $(document).ready(function(){
    $('#tbl1').DataTable();
    });
</script> -->