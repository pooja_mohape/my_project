<section>
 	<form method="post" name="frmsurvey" id="frmsurvey" action="">
 	<div class="row">
 		<div class="col-md-4 col-sm-12 col-xs-12" style="padding: 40px!important;">
 			<h3 class="text-center"><strong>Survey Details</strong></h3><hr>
	 		<div class="form-group">
	 			<label class="for">Survey Name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 			<input type="text" class="form-control" name="surveyName" id="surveyName" placeholder="Enter Survey Name" required="required">
	 		</div>
	 		 <div class="form-group">
	 			<label class="for">Survey Start Date</label>
	 			<input type="date" class="form-control" name="sDate" id="sDate" placeholder="Enter Survey Start Date" required="required">
	 		</div>
	 		 <div class="form-group">
	 			<label class="for">Survey End Date</label>&nbsp;
	 			<input type="date" class="form-control" name="eDate" id="eDate" placeholder="Enter Survey End Date" required>
	 		</div>
	 		 <div class="form-group">
	 			<label class="for">Survey Description</label>
	 			<textarea type="text" class="form-control" name="description" id="description" placeholder="Enter Survey Description" required="required"></textarea>
	 		</div>
	 		 <div class="form-group">
	 			<label class="control-label">Has fees?</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 			<input type="radio" name="hasfees">Yes
	 			<input type="radio" name="hasfees" checked>No
	 		</div>
 		</div>
 		<div class="col-md-8" style="padding-top: 40px;padding-right: 30px;overflow-y: hidden;:false">
 			<h3 class="text-center"><strong>Create Form</strong></h3><hr>
			<div id="form_builder" style="background-color: #f2f2f2;">
			</div>
			<div class="form-group" style="padding-top: 30px;">
				<button type="button" class="btn btn-success" id="getJSON">Create Survey</button>
			</div>
		</div>
 	</div>
 </form>
</section>
<script type="text/javascript">
	$(document).ready(function(){

		  var options =
		   {
			  controlPosition: 'left',
			  messages: {
			    select: 'Dropdown',
			  },
			  showActionButtons: false,
			  disableFields: ['autocomplete', 'hidden','button'],
			  editOnAdd:true,
			  sortableControls:true,
			  stickyControls: true,
			  controlOrder: [
			    'radio-group',
			    'checkbox',
			    'checkbox-group',
			    'text',
			    'textarea',
			    'select',
			    'date',
			    'file',    
			    'button',
			  ]
			};

		var formBuilder = $('#form_builder').formBuilder(options);

		$('#getJSON').click(function() {

			var jsonData	 = formBuilder.actions.getData('json', true);

			if($("#frmsurvey").valid())
			{
			var sDate	 	 = $('#sDate').val();
			var eDate	 	 = $('#eDate').val();
			
			if(sDate>eDate) {
				alert('Start Date Can Not Be Greater Than End Date');
				$('#sDate').val('');
				$('#eDate').val('');
			}
			if(jsonData.length==2) {
				alert('Please Create Survey Form Then Submit');
			}
			var surveyName	 = $('#surveyName').val();
			var hasfees		 = $('#hasfees').val();
			var description	 = $('#description').val();

			 jsonData		 = JSON.stringify(JSON.parse(jsonData));

			if(sDate>eDate || jsonData.length==2) {
				
			} 
			else {
				
				$.ajax({
					url:"<?= base_url(); ?>survey/addsurvey",
					type:"POST",
					data:{jsonData:jsonData,sDate:sDate,eDate:eDate,surveyName:surveyName,description:description},
					success:function(){
						window.location.reload(); 
					}
				});
			}
		 }
		 	$('.error').css('color','red');
	});
});    
</script>