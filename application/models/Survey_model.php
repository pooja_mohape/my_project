<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Survey_model extends MY_Model {

    var $table = 'survey';
    var $fields = array("id", "startDate", "endDate", "description", "questionary","answerset","isActive","hasFees","user_id", "created_at", "updated_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }    

    public function createSurvey($survey_arr) {

        $this->db->insert('survey',$survey_arr);
        return true;
    }

    public function livesurvey($dt) {
        
        $this->db->where('id',$dt);
        $res = $this->db->update('survey', array('live' => 'Y'));
        
        if($res) {
          
            $this->db->where('id!=',$dt);
            $res = $this->db->update('survey', array('live' => 'N'));      
        }
        return true;
    }

    public function addAnswer($user_id,$survey_id,$ip,$browser,$user_agent,$answer) 

    {
        $ua_data = json_decode($user_agent);

        if(isset($ua_data->country)){
            $country = $ua_data->country;
        } else {
            $country = '';
        }
        if(isset($ua_data->region)) {
            $region  = $ua_data->region;
        } else {
            $region = '';
        }
        if(isset($ua_data->city)) {
            $city    = $ua_data->city;
        } else {
            $city = '';
        }
        if(isset($ua_data->loc)) {
            $loc = $ua_data->loc;
        }
        else {
            $loc = '';
        }
        if(isset($ua_data->postal)) {
            $postal  = $ua_data->postal;
        } else {
            $postal = '';
        }
        
        $draft_answer='';
        $ans_dd = $this->db->where('user_id',$user_id)->get('answer')->result();
        
        if($ans_dd) 
        {

            if($ans_dd[0]->draft_answer)
            {
                $draft_answer = $ans_dd[0]->draft_answer;
            }
        }

        $survey_data = array(
            
             'survey_id'    => $survey_id,
             'user_id'      => $user_id,
             'draft'        => 'N',
             'ip'           => $ip,
             'browser'      => $browser,
             'answer'       => $answer,
             'draft_answer' => $draft_answer,
             'country'      => $country,
             'region'       => $region,
             'city'         => $city,
             'loc'          => $loc,
             'postal'       => $postal
         );
         //show($survey_data,1);
        if($survey_id!='0' || !empty($survey_id) && !empty($answer)) 
         {
            $dd = $this->db->where('user_id',$user_id)->get('answer')->result();
            if($dd) 
            {
                $this->db->where('user_id',$user_id);
                $this->db->update('answer',$survey_data);
            }
            else 
            {
                $this->db->insert('answer', $survey_data);
            }
            return true;
        }
    }

    public function addDraftAnswer($user_id,$survey_id,$ip,$browser,$user_agent,$answer) {

        $ua_data = json_decode($user_agent);

        if(isset($ua_data->country)){
            $country = $ua_data->country;
        } else {
            $country = '';
        }
        if(isset($ua_data->region)) {
            $region  = $ua_data->region;
        } else {
            $region = '';
        }
        if(isset($ua_data->city)) {
            $city    = $ua_data->city;
        } else {
            $city = '';
        }
        if(isset($ua_data->loc)) {
            $loc = $ua_data->loc;
        }
        else {
            $loc = '';
        }
        if(isset($ua_data->postal)) {
            $postal  = $ua_data->postal;
        } else {
            $postal = '';
        }
        
        $draft='Y';
        $ans_dd = $this->db->where('user_id',$user_id)->get('answer')->result();
        $ans='';
        if($ans_dd) 
        {
            if(!empty($ans_dd[0]->answer))
            {
                $draft  ='N';
                $ans = $ans_dd[0]->answer;
            }
            else
            {
                $draft ='Y';
            }
        }

        $survey_data = array(
            
             'survey_id'    => $survey_id,
             'user_id'      => $user_id,
             'draft'        => $draft,
             'ip'           => $ip,
             'browser'      => $browser,
             'draft_answer' => $answer,
             'answer'       => $ans,
             'country'      => $country,
             'region'       => $region,
             'city'         => $city,
             'loc'          => $loc,
             'postal'       => $postal
         );
        
        if($survey_id!='0' || !empty($survey_id)) 
         {
            
            if($ans_dd) 
            {
                $this->db->where('user_id',$user_id);
                $this->db->update('answer',$survey_data);
            }
            else 
            {
                $this->db->insert('answer', $survey_data);
            }

             return true;
         }
    }

    public function surveyAnswer() {

        $this->db->select('a.*,s.name,s.questionary,s.startDate,s.endDate','left');
        $this->db->from('answer a');
        $this->db->join('survey s','s.id=a.survey_id');
        $this->db->order_by('a.id','desc');
        $res = $this->db->get()->result();
        return $res;
    }

        public function surveyStatistic() {

        $dd  =0;
        $dd1 =0;
        $dd2 =0;
        $dd3 =0;

        $this->db->select('count(id) as draft_count');
        $this->db->from('answer');
        $dd = $this->db->where('draft','Y')->get()->result();

        if($dd)
        {
          $dd = $dd[0]->draft_count;
        }

        $this->db->select('count(id) as ans_count');
        $this->db->from('answer');
        $dd1 = $this->db->where('draft','N')->get()->result();

        if($dd1)
        {
          $dd1 = $dd1[0]->ans_count;
        }

        $this->db->select('count(id) as tans_count');
        $dd2 = $this->db->from('answer')->get()->result();

        if($dd2)
        {
          $dd2 = $dd2[0]->tans_count;
        }

        $this->db->select('count(id) as survey_count');
        $dd3 = $this->db->from('survey')->get()->result();
        
        if ($dd3)
        {
            $dd3 = $dd3[0]->survey_count;    
        }

        $data  = array('draft_count' => $dd,'ans_count' => $dd1,'tans_count'=>$dd2,'survey_count'=>$dd3);
        // show($data,1);
        return $data;
    }
}