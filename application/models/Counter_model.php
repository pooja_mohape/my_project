<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Counter_model extends MY_Model {

    var $table = 'counters';
    var $fields = array("id", "No_of_Event", "successful_event", "successful_years", "dedicated_client", "updated_at", "created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }    
}

// End of Menu_model class