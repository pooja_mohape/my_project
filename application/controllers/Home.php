<?php
defined('BASEPATH') or die('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Survey_model');
	}

	public function index() {
		date_default_timezone_set("Asia/Kolkata");
		$year        = date('Y');
		$data        = array();
		$res['data'] = $this->db->get('txt_content')->result();
		$res['shortlist'] = $this->db->get('shortlist_data')->result();
		$this->db->order_by('created_at', 'desc');
		$res['block']       = $this->db->get('block', 1)->result();
		$res['res_slider']  = $this->db->get('slider')->result();
		$res['re']          = $this->db->get('button')->result();
		$res['logo_img']    = $this->db->get('logo_img')->result();
		$res['people_cat']  = $this->db->where('model_name', 'model1')->get('category')->result();
		$res['proj_cat']    = $this->db->where('model_name', 'model2')->get('category')->result();
		$res['company_cat'] = $this->db->where('model_name', 'model3')->get('category')->result();
		$res['traval_info'] = $this->db->where('model_name', 'model4')->get('category')->result();
		$res['awards']      = $this->db->get('awards')->result();

		//get data of winner list
		//show($year,1);
		//$this->db->where('year',$year);
		$this->db->distinct('year');
		$this->db->select('year');
		$this->db->order_by('year', 'desc');
		$this->db->limit(1);
		$check = $this->db->get('list_of_winners')->result();

		$this->db->where('year', $check[0]->year);
		//$this->db->order_by('year','desc');
		$this->db->limit(2);
		$res['winner_list'] = $this->db->get('list_of_winners')->result();
		// show($res['winner_list'],1);
		//get list of years from database
		$this->db->distinct('year');
		$this->db->order_by('year', 'desc');
		$this->db->select('year');
		$res['year'] = $this->db->get('list_of_winners')->result();
		//show($res['year'],1);
		$res['logo'] = $this->db->get('logo')->result();
		$counters    = $this->db->select('*')->from('counters')->get()->result();

		$this->db->where('model_name', 'model1');
		$res['mod1'] = $this->db->get('model_content')->result();

		$this->db->where('model_name', 'model2');
		$res['mod2'] = $this->db->get('model_content')->result();

		$this->db->order_by('id', 'desc');
		$res['award'] = $this->db->limit('1')->get('awards')->result();

		if ($counters) {
			$res['counters'] = $counters;
		}
		$res['about']      = $this->db->where('footer_id', '1')->get('footer_data')->result();
		$res['awards']     = $this->db->where('footer_id', '2')->get('footer_data')->result();
		$res['links']      = $this->db->where('footer_id', '3')->get('footer_data')->result();
		$res['contact_us'] = $this->db->where('footer_id', '4')->get('footer_data')->result();
		//survey data
		$res['dt']      = $this->Survey_model->where('live', 'Y')->find_all();
		$res['ansdt']   = $this->Survey_model->surveyAnswer();
		$res['uid']     = getRandomId(9);
		$res['s_count'] = $this->Survey_model->surveyStatistic();
		load_back_view('home', $res);
	}

	public function category() {
		if (is_logged_in()) {
			$data['people_cat']  = $this->db->where('model_name', 'model1')->get('category')->result();
			$data['proj_cat']    = $this->db->where('model_name', 'model2')->get('category')->result();
			$data['company_cat'] = $this->db->where('model_name', 'model3')->get('category')->result();
			$data['traval_info'] = $this->db->where('model_name', 'model4')->get('category')->result();
			dashboard_back_view('category', $data);
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	public function edit_traval_info() {

		$data['venue_record'] = $this->db->where('model_name', 'model4')->get('category')->result_array();
		dashboard_back_view('edit_traval_info', $data);
	}

	public function updatetravel_info($id) {
		$input   = $this->input->post();
		$title   = $input['title'];
		$article = $input['article'];

		$data = array('title' => $title,
			'content'            => $article,
		);
		$this->db->where('id', $id);
		$res = $this->db->update('category', $data);
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Travel Info Updated Successfully.');
			redirect(base_url().'home/edit_traval_info');
		}
	}

	// public function edit_traval_info()
	// {
	//   $id=$this->uri->segment(3);
	//   $this->db->where('id',$id);
	//   $res['data']=$this->db->get('category')->result();
	//   dashboard_back_view('edit_traval_info',$res);
	// }

	// public function update_traval_info()
	//   {
	//       $input=$this->input->post();
	//       if($input)
	//       {
	//           $id=$input['id'];
	//           $title=$input['title'];
	//           $content=$input['content'];
	//           $array=array('title'=>$title,'content'=>$content);
	//           $this->db->where('id',$id);
	//           $query=$this->db->update('category',$array);
	//           if($query)
	//           {
	//             $this->session->set_flashdata('msg_type', 'success');
	//             $this->session->set_flashdata('msg', 'Updated Successfully.');
	//             redirect(base_url().'home/category');
	//           }

	//       }
	//   }

	public function edit_people_category() {

		$data['record'] = $this->db->where('model_name', 'model1')->get('category')->result_array();
		dashboard_back_view('edit_people_category', $data);
	}

	public function updatepeopleCat($id) {
		$input   = $this->input->post();
		$title   = $input['title'];
		$article = $input['article'];

		$data = array('title' => $title,
			'content'            => $article,
		);
		$this->db->where('id', $id);
		$res = $this->db->update('category', $data);
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'People Category Updated Successfully.');
			redirect(base_url().'home/edit_people_category');
		}
	}

	public function edit_company_category() {
		$data['comapny_record'] = $this->db->where('model_name', 'model3')->get('category')->result_array();
		dashboard_back_view('edit_company_category', $data);
	}

	public function updatecompanyCat($id) {
		$input   = $this->input->post();
		$title   = $input['title'];
		$article = $input['article'];

		$data = array('title' => $title,
			'content'            => $article,
		);
		$this->db->where('id', $id);
		$res = $this->db->update('category', $data);
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Company Category Updated Successfully.');
			redirect(base_url().'home/edit_company_category');
		}
	}

	public function edit_project_category() {
		$data['project_record'] = $this->db->where('model_name', 'model2')->get('category')->result_array();
		dashboard_back_view('edit_project_category', $data);
	}

	public function updateprojectCat($id) {
		$input   = $this->input->post();
		$title   = $input['title'];
		$article = $input['article'];

		$data = array('title' => $title,
			'content'            => $article,
		);
		$this->db->where('id', $id);
		$res = $this->db->update('category', $data);
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Project Category Updated Successfully.');
			redirect(base_url().'home/edit_project_category');
		}
	}

	//  public function edit_project_category()
	// {
	//   $id=$this->uri->segment(3);
	//   $this->db->where('id',$id);
	//   $res['data']=$this->db->get('category')->result();
	//   dashboard_back_view('edit_project_category',$res);
	// }

	// public function update_project_category()
	//   {
	//       $input=$this->input->post();
	//       if($input)
	//       {
	//           $id=$input['id'];
	//           $title=$input['title'];
	//           $content=$input['content'];
	//           $array=array('title'=>$title,'content'=>$content);
	//           $this->db->where('id',$id);
	//           $query=$this->db->update('category',$array);
	//           if($query)
	//           {
	//             $this->session->set_flashdata('msg_type', 'success');
	//             $this->session->set_flashdata('msg', 'Updated Successfully.');
	//             redirect(base_url().'home/category');
	//           }

	//       }
	//   }

	public function register() {
		$data = array();
		load_back_view('user_register', $data);
	}

	public function register_user() {
		$input = $this->input->post();
		if ($input) {
			$fname    = $input['fname'];
			$lname    = $input['lname'];
			$username = $input['username'];
			$password = $input['password'];
			$email    = $input['email'];
			$users    = array('fname' => $fname, 'lname' => $lname, 'username' => $username, 'password' => $password, 'email' => $email);

			$res = $this->db->insert('users', $users);
			if ($res) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Inserted Successfully.');
				redirect(base_url().'home/admin-panel');
			} else {
				redirect(base_url().'home/register');
			}
		}
	}
	public function admin_login() {
		$this->load->view('admin_login');
	}

	public function get_content() {
		$year = $this->input->post('year');

		$this->db->where('year', $year);
		$this->db->limit(2);
		$get = $this->db->get('list_of_winners')->result();
		$div = "";

		$div .= "<div class='row align-items-center' id='tabsNavigation'>";

		foreach ($get as $row) {

			$div .= "<div class='col-lg-6 col-xl-6 appear-animation' data-appear-animation='fadeInRightShorter' data-appear-animation-delay='600' style='opacity:1;'>
                    <div>
                      <div class='testimonial custom-testimonial-style-1'>

                        <div class='row'>
                          <div class='col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0'>
                            <img src='".base_url()."/uploads/".$row->img."' class='rounded-circle img-center avatar' alt='' />
                          </div>
                          <div class='col-lg-9'>
                            <blockquote>
                              <p class='mb-3'>".substr($row->content, 0, 150)."</p>
                            </blockquote>
                            <div class='testimonial-author'>
                              <strong class='text-2 mb-0'>".$row->name."</strong>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>";

		}
		$div .= "</div>";
		echo json_encode($div);
	}

	public function edit_section() {
		$this->db->select('*');
		$this->db->from('txt_content');
		$data['response'] = $this->db->get()->result();
		dashboard_back_view('edit_section', $data);
	}
	public function add_section() {
		$input = $this->input->post();
		if ($input) {
			$name    = $_FILES['userfile']['name'];
			$heading = $input['heading'];
			$content = $input['content'];
			$size    = $_FILES['userfile']['size'];

			$ext = pathinfo($name, PATHINFO_EXTENSION);

			$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

			if (!in_array($ext, $allowed)) {
				show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
			} else {
				if ($size > '2000000') {
					show('Max Allowed Image Size is 2MB', 1);
				} else {

					$tpname = explode('.', $name);

					//$pic=$tpname[0].'_'.$heading.'.'.$tpname[1];
					$pic = $tpname[0].'_'.uniqid().'.'.$tpname[1];

					$tmp_name = $_FILES['userfile']['tmp_name'];

					$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
					move_uploaded_file($tmp_name, $path.'/'.$pic);
					$res = $this->db->insert('txt_content', array('title' => $heading, 'txt' => $content, 'img' => $pic));
					if ($res) {
						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Inserted Successfully.');
						redirect(base_url().'home/edit_section');
					}

				}
			}
		}
	}
	public function delete_section() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res = $this->db->delete('txt_content');
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Deleted Successfully.');
			redirect(base_url().'home/edit_section');
		}
	}
	public function update_section() {
		$id = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->from('txt_content');
		$this->db->where('id', $id);
		$data['data'] = $this->db->get()->result();
		dashboard_back_view('edit_section_data', $data);
	}
	public function edit_section_data() {
		$input = $this->input->post();

		if ($input) {
			$name    = $_FILES['userfile']['name'];
			$id      = $input['id'];
			$heading = $input['heading'];
			$content = $input['content'];
			$size    = $_FILES['userfile']['size'];
			if ($_FILES['userfile']['name']) {
				$ext = pathinfo($name, PATHINFO_EXTENSION);

				$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

				if (!in_array($ext, $allowed)) {
					show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
				} else {
					if ($size > '2000000') {
						show('Max Allowed Image Size is 2MB', 1);
					} else {

						$tpname = explode('.', $name);
						//$pic=$tpname[0].'_'.$heading.'.'.$tpname[1];
						$pic      = $tpname[0].'_'.uniqid().'.'.$tpname[1];
						$tmp_name = $_FILES['userfile']['tmp_name'];

						$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
						$res  = move_uploaded_file($tmp_name, $path.'/'.$pic);

						$this->db->where('id', $id);
						$res = $this->db->update('txt_content', array('title' => $heading, 'txt' => $content, 'img' => $pic));
						if ($res) {
							$this->session->set_flashdata('msg_type', 'success');
							$this->session->set_flashdata('msg', 'Updated Successfully.');
							redirect(base_url().'home/edit_section');
						}

					}
				}
			} else {
				$this->db->where('id', $id);
				$this->db->update('txt_content', array('title' => $heading, 'txt' => $content));
				redirect(base_url().'home/edit_section');
			}
		}
	}

	/*Slider section*/
	public function slider() {
		$data         = array();
		$data['card'] = $this->db->get('card')->result();
		dashboard_back_view('slider', $data);
	}

	public function upload_slider() {
		$input = $this->input->post();
		if ($input) {
			$name    = $_FILES['userfile']['name'];
			$heading = $input['heading'];
			$content = $input['content'];
			$btn     = $input['btn'];

			$size = $_FILES['userfile']['size'];
			//show($size,1);
			$ext     = pathinfo($name, PATHINFO_EXTENSION);
			$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

			if (!in_array($ext, $allowed)) {
				show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);

			} else {
				if ($size > '2000000') {
					show('Max Allowed Image Size is 2MB', 1);
				} else {

					$tpname = explode('.', $name);

					$pic      = $tpname[0].'_'.$heading.'.'.$tpname[1];
					$tmp_name = $_FILES['userfile']['tmp_name'];

					$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
					move_uploaded_file($tmp_name, $path.'/'.$pic);

					$res = $this->db->insert('slider', array('img' => $pic, 'title' => $heading, 'button_name' => $btn, 'content' => $content));
					if ($res) {
						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Inserted Successfully.');
						redirect(base_url().'home/slider');
					}

				}
			}
		}
	}

	public function edit_slider() {
		//$data['r']=$this->db->get('slider')->result();
		$id = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->from('slider');
		$this->db->where('id', $id);
		$data['data'] = $this->db->get()->result();
		// show($data,1);
		dashboard_back_view('edit_slider', $data);
	}

	public function update() {
		$input = $this->input->post();
		if ($input) {
			$name = $_FILES['userfile']['name'];

			$id      = $input['id'];
			$heading = $input['heading'];
			$content = $input['content'];
			$btn     = $input['btn'];

			$size = $_FILES['userfile']['size'];

			if ($_FILES['userfile']['name']) {
				$ext = pathinfo($name, PATHINFO_EXTENSION);

				$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

				if (!in_array($ext, $allowed)) {
					show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
				} else {
					if ($size > '2000000') {
						show('Max Allowed Image Size is 2MB', 1);
					} else {

						$tpname = explode('.', $name);

						$pic      = $tpname[0].'_'.$heading.'.'.$tpname[1];
						$tmp_name = $_FILES['userfile']['tmp_name'];

						$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
						$res  = move_uploaded_file($tmp_name, $path.'/'.$pic);

						$this->db->where('id', $id);
						$res = $this->db->update('slider', array('title' => $heading, 'content' => $content, 'button_name' => $btn, 'img' => $pic));
						if ($res) {
							$this->session->set_flashdata('msg_type', 'success');
							$this->session->set_flashdata('msg', 'Updated Successfully.');
							redirect(base_url().'home/slider');
						}

					}
				}
			} else {
				$this->db->where('id', $id);
				$this->db->update('slider', array('title' => $heading, 'content' => $content, 'button_name' => $btn));
				redirect(base_url().'home/slider');
			}
		}

	}

	public function delete($id) {
		$this->db->where('id', $id);
		$res = $this->db->delete('slider');
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Deleted Successfully.');
			redirect(base_url().'Home/slider');
		}
	}

	/*Methodology*/

	function Methodology() {
		if (is_logged_in()) {
			dashboard_back_view('Methodology');
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	public function addArticle() {
		$input = $this->input->post();

		$title   = $input['title'];
		$article = $input['article'];
		//$date = date('d/m/y');

		$data = array('title' => $title,
			'content'            => $article
			//'created_at'=>$date
		);
		//print_r($data);
		die();

		$res = $this->db->insert('block', $data);
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Inserted Successfully.');
			redirect(base_url().'home/Methodology');
		}
	}

	public function loadArticle() {
		$query = $this->db->get('block');
		foreach ($query->result() as $row) {
			$date = date_create($row->created_at);
			$date = date_format($date, 'l jS F Y');
			echo '<blockquote><h3>'.ucfirst($row->title).'</h3></blockquote><p>'.html_entity_decode($row->content).'</p><a href="#" class="btn btn-sm btn-warning">'.$date.'</a><hr/>';
		}
		exit;
	}

	/*Winners list*/

	function winners_list() {
		if (is_logged_in()) {
			dashboard_back_view('winners_list');
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	function upload_winners() {
		$input = $this->input->post();
		if ($input) {
			$name      = $_FILES['userfile']['name'];
			$logo_name = $_FILES['logo']['name'];
			$year      = $input['year'];
			$w_name    = $input['name'];
			$title     = $input['title'];
			$content   = $input['content'];

			$size = $_FILES['userfile']['size'];
			$logo = $_FILES['logo']['size'];
			//show($size,1);
			$ext     = pathinfo($name, PATHINFO_EXTENSION);
			$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

			if (!in_array($ext, $allowed)) {
				show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
			} else {
				if ($size > '2000000') {
					show('Max Allowed Image Size is 2MB', 1);
				} else {

					$tpname      = explode('.', $name);
					$tpname_logo = explode('.', $logo_name);
					// $pic      = $tpname[0].'_'.$w_name.'.'.$tpname[1];
					$pic      = $tpname[0].'_'.uniqid().'.'.$tpname[1];
					$tmp_name = $_FILES['userfile']['tmp_name'];

					$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
					move_uploaded_file($tmp_name, $path.'/'.$pic);

					$logo          = $tpname_logo[0].'_'.uniqid().'.'.$tpname_logo[1];
					$tmp_name_logo = $_FILES['logo']['tmp_name'];

					$path_logo = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
					move_uploaded_file($tmp_name_logo, $path_logo.'/'.$logo);

					$res = $this->db->insert('list_of_winners', array('img' => $pic, 'year' => $year, 'name' => $w_name, 'title' => $title, 'content' => $content, 'w_logo' => $logo));
					if ($res) {
						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Inserted Successfully.');
						redirect(base_url().'home/winners_list');
					}
				}
			}

		}
	}

	public function edit_winners() {
		$id = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->from('list_of_winners');
		$this->db->where('id', $id);
		$data['data'] = $this->db->get()->result();

		dashboard_back_view('edit_winners', $data);
	}

	public function update_winners() {
		$input = $this->input->post();
		if ($input) {
			$name    = $_FILES['userfile']['name'];
			$id      = $input['id'];
			$year    = $input['year'];
			$w_name  = $input['name'];
			$title   = $input['title'];
			$content = $input['content'];
			$size    = $_FILES['userfile']['size'];
			if ($_FILES['userfile']['name']) {
				$ext = pathinfo($name, PATHINFO_EXTENSION);

				$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

				if (!in_array($ext, $allowed)) {
					show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
				} else {

					$tpname = explode('.', $name);

					$pic      = $tpname[0].'_'.$w_name.'.'.$tpname[1];
					$tmp_name = $_FILES['userfile']['tmp_name'];

					$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
					$res  = move_uploaded_file($tmp_name, $path.'/'.$pic);

					$this->db->where('id', $id);
					$res = $this->db->update('list_of_winners', array('img' => $pic, 'year' => $year, 'name' => $w_name, 'title' => $title, 'content' => $content));
					if ($res) {
						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Updated Successfully.');
						redirect(base_url().'home/winners_list');
					}
				}
			} else {
				$this->db->where('id', $id);
				$this->db->update('list_of_winners', array('year' => $year, 'name' => $w_name, 'title' => $title, 'content' => $content));
				redirect(base_url().'home/winners_list');
			}
		}
	}

	public function winners_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('list_of_winners');
		redirect(base_url().'Home/winners_list');
	}

	/*Award Shortlist*/
	function shortlist_data() {if (is_logged_in()) {
			dashboard_back_view('shortlist');
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	function upload_shortlist() {
		$input = $this->input->post();

		$title   = $input['title'];
		$content = $input['content'];
		$res     = $this->db->insert('shortlist_data', array('title' => $title, 'content' => $content));
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Inserted Successfully.');
			redirect(base_url().'Home/index');
		}
		//show($data,1);
	}

	public function edit_shortlist() {
		$id = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->from('shortlist_data');
		$this->db->where('id', $id);
		$data['data'] = $this->db->get()->result();

		dashboard_back_view('edit_shortlist', $data);
	}

	public function update_shortlist() {
		// show($id,1);
		$input = $this->input->post();
		if ($input) {
			$id      = $input['id'];
			$title   = $input['title'];
			$content = $input['content'];
			$array   = array('title' => $title, 'content' => $content);

			$this->db->where('id', $id);
			$query = $this->db->update('shortlist_data', $array);
			if ($query) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/shortlist_data');
			}

		}
	}

	public function delete_shortlist($id) {
		$this->db->where('id', $id);
		$res = $this->db->delete('shortlist_data');
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Deleted Successfully.');
			redirect(base_url().'Home/shortlist_data');
		}
	}
	public function login_authenticate() {
		$input = $this->input->post();
		if ($input) {
			$this->load->model('users_model');
			$username = isset($input['username'])?$input['username']:'';
			$password = isset($input['password'])?$input['password']:'';
			if ($username && $password) {
				$username1 = $this->users_model->where('username', $username)->find_all();
				if ($username1) {
					$user_data = $this->users_model->where('username', $username1[0]->username)->where('password', $password)->find_all();

					if ($username == $username1[0]->username && $password == $user_data[0]->password) {
						$user_array = $user_data[0];
						$user_data  = array(
							"id"         => $user_array->id,
							"username"   => $user_array->username,
							"email"      => $user_array->email,
							"first_name" => $user_array->fname,
							"last_name"  => $user_array->lname,
						);

						$this->session->set_userdata($user_data);

						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Welcome To Award.');
						redirect(base_url().'home/dashboard');
					} else {
						redirect(base_url().'home/admin_login');
					}
				} else {
					redirect(base_url().'home/admin_login');
				}
			} else {
				redirect(base_url().'home/admin_login');
			}
		} else {
			redirect(base_url().'home/admin_login');
		}
	}
	public function dashboard() {
		if (is_logged_in()) {
			dashboard_back_view('admin_dashboard');
		} else {
			redirect(base_url().'home/admin_login');
		}
	}
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}
	public function section_logo() {

		if (is_logged_in()) {
			dashboard_back_view('logo');
		} else {
			redirect(base_url().'home/admin_login');
		}

	}
	public function add_logo() {
		$input = $this->input->post();
		$name  = $_FILES['userfile']['name'];
		$size  = $_FILES['userfile']['size'];
		$ext   = pathinfo($name, PATHINFO_EXTENSION);

		$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

		if (!in_array($ext, $allowed)) {
			show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
		} else {
			if ($size > '2000000') {
				show('Max Allowed Image Size is 2MB', 1);
			} else {
				$date     = date("Y-m-d_H-i-s");
				$tpname   = explode('.', $name);
				$pic      = $tpname[0].'_'.$date.'.'.$tpname[1];
				$tmp_name = $_FILES['userfile']['tmp_name'];

				$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/logo';
				//chmod($path,0777);
				$res = move_uploaded_file($tmp_name, $path.'/'.$pic);

				$res = $this->db->insert('logo', array('img' => $pic));
				if ($res) {
					$this->session->set_flashdata('msg_type', 'success');
					$this->session->set_flashdata('msg', 'Inserted Successfully.');
					redirect(base_url().'home/section_logo');
				}
			}
		}
	}
	public function delete_logo() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res = $this->db->delete('logo');
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Deleted Successfully.');
			redirect(base_url().'home/section_logo');
		}
	}
	public function update_logo() {
		$id = $this->uri->segment(3);
		$this->db->select('*');
		$this->db->from('logo');
		$this->db->where('id', $id);
		$res['data'] = $this->db->get()->result();
		dashboard_back_view('edit_logo', $res);

	}
	public function edit_logo_section() {
		$input = $this->input->post();

		if ($input) {
			$name = $_FILES['userfile']['name'];
			$id   = $input['id'];

			$size = $_FILES['userfile']['size'];
			if ($_FILES['userfile']['name']) {
				$ext = pathinfo($name, PATHINFO_EXTENSION);

				$allowed = array('gif', 'png', 'jpg', 'jpeg', '');

				if (!in_array($ext, $allowed)) {
					show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
				} else {
					if ($size > '2000000') {
						show('Max Allowed Image Size is 2MB', 1);
					} else {

						$tpname = explode('.', $name);
						$date   = date("Y-m-d_H-i-s");
						$pic    = $tpname[0].'_'.$date.'.'.$tpname[1];

						$tmp_name = $_FILES['userfile']['tmp_name'];

						$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/logo';

						$res = move_uploaded_file($tmp_name, $path.'/'.$pic);

						$this->db->where('id', $id);
						$res = $this->db->update('logo', array('img' => $pic));
						if ($res) {
							$this->session->set_flashdata('msg_type', 'success');
							$this->session->set_flashdata('msg', 'Successfully Updated.');
							redirect(base_url().'home/section_logo');
						}
					}
				}
			} else {
				$this->session->set_flashdata('msg_type', 'danger');
				$this->session->set_flashdata('msg', 'image not selected.');
				redirect(base_url().'home/section_logo');
			}

		}
	}

	public function award() {
		if (is_logged_in()) {
			dashboard_back_view('awards');
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	public function add_award() {
		$input = $this->input->post();
		if ($input) {
			$heading   = $input['heading'];
			$title     = $input['title'];
			$paragraph = $input['paragraph'];
			$array     = array('heading' => $heading, 'title' => $title, 'paragraph' => $paragraph);
			$res       = $this->db->insert('awards', $array);
			if ($res) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Inserted Successfully.');
				redirect(base_url().'home/award');
			}

		}
	}

	public function delete_awards() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res = $this->db->delete('awards');
		if ($res) {
			$this->session->set_flashdata('msg_type', 'success');
			$this->session->set_flashdata('msg', 'Deleted Successfully.');
			redirect(base_url().'home/award');
		}

	}

	public function update_awards() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res['data'] = $this->db->get('awards')->result();
		dashboard_back_view('awards_update', $res);
	}

	public function update_awrds() {
		$input = $this->input->post();

		if ($input) {
			$id        = $input['id'];
			$heading   = $input['heading'];
			$title     = $input['title'];
			$paragraph = $input['paragraph'];
			$array     = array('heading' => $heading, 'title' => $title, 'paragraph' => $paragraph);
			$this->db->where('id', $id);
			$res = $this->db->update('awards', $array);

			if ($res) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Successfully Updated.');
				redirect(base_url().'home/award');
			}

		}
	}
	public function card_update() {
		$input = $this->input->post();
		$off   = isset($input['off'])?$input['off']:'';
		$on    = isset($input['on'])?$input['on']:'';

		if ($on) {
			$this->db->set('status', '1');
			$update = $this->db->update('card');
			$status = $on;
		} else {
			$this->db->set('status', '0');
			$update = $this->db->update('card');
			$status = $off;
		}
		echo $status;
	}
	public function servey() {
		if (is_logged_in()) {

			$res['data'] = $this->db->get('survey')->result();
			dashboard_back_view('servey_data', $res);
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	public function footer() {
		if (is_logged_in()) {
			$data['res']        = $this->db->where('footer_id', '1')->get('footer_data')->result();
			$data['awards']     = $this->db->where('footer_id', '2')->get('footer_data')->result();
			$data['links']      = $this->db->where('footer_id', '3')->get('footer_data')->result();
			$data['contact_us'] = $this->db->where('footer_id', '4')->get('footer_data')->result();
			dashboard_back_view('footer_first', $data);
		} else {redirect(base_url().'home/admin_login');
		}
	}
	public function update_footer() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res['data'] = $this->db->get('footer_data')->result();
		dashboard_back_view('footer_abt', $res);
	}

	public function add_abt() {
		$input = $this->input->post();
		if ($input) {
			$id      = $input['id'];
			$heading = $input['heading'];
			$content = $input['content'];
			$array   = array('heading' => $heading, 'content' => $content);
			$this->db->where('id', $id);
			$res = $this->db->update('footer_data', $array);
			if ($res) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/footer');
			}

		}

	}
	public function update_awardcat() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res['data'] = $this->db->get('footer_data')->result();
		dashboard_back_view('update_awardcategory', $res);
	}
	public function upd_awrdcat() {
		$input = $this->input->post();
		if ($input) {
			$id       = $input['id'];
			$category = $input['category'];
			$this->db->where('id', $id);
			$res = $this->db->update('footer_data', array('category' => $category));
			if ($res) {
				if ($id == '2') {
					$this->db->where('id', '7');
					$this->db->update('menu', array('name' => $category));

					$this->db->where('id', '1');
					$this->db->update('category', array('title' => $category));
				} else if ($id == '5') {
					$this->db->where('id', '2');
					$this->db->update('menu', array('name' => $category));

					$this->db->where('id', '2');
					$this->db->update('category', array('title' => $category));
				} elseif ($id == '6') {
					$this->db->where('id', '9');
					$this->db->update('menu', array('name' => $category));

					$this->db->where('id', '3');
					$this->db->update('category', array('title' => $category));
				}

				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/imp_links');
			}
		}
	}
	public function links() {
		if (is_logged_in()) {
			$id = $this->uri->segment(3);
			$this->db->where('id', $id);
			$res['data'] = $this->db->get('footer_data')->result();
			dashboard_back_view('update_links', $res);
		} else {
			redirect(base_url().'home/admin_login');
		}
	}

	public function imp_links() {
		if (is_logged_in()) {
			$data['res']        = $this->db->where('footer_id', '1')->get('footer_data')->result();
			$data['awards']     = $this->db->where('footer_id', '2')->get('footer_data')->result();
			$data['links']      = $this->db->where('footer_id', '3')->get('footer_data')->result();
			$data['contact_us'] = $this->db->where('footer_id', '4')->get('footer_data')->result();
			dashboard_back_view('important_links', $data);
		} else {redirect(base_url().'home/admin_login');
		}
	}
	public function upd_links() {
		$input = $this->input->post();
		if ($input) {
			$id    = $input['id'];
			$links = $input['links'];
			$this->db->where('id', $id);
			$res = $this->db->update('footer_data', array('quick_links' => $links));
			if ($res) {
				if ($id == '7') {
					$this->db->where('id', '2');
					$this->db->update('menu', array('name' => $links));
				} else if ($id == '8') {
					$this->db->where('id', '3');
					$this->db->update('menu', array('name' => $links));
				} elseif ($id == '9') {
					$this->db->where('id', '4');
					$this->db->update('menu', array('name' => $links));
				} elseif ($id == '10') {
					$this->db->where('id', '5');
					$this->db->update('menu', array('name' => $links));
				} elseif ($id == '11') {
					$this->db->where('id', '6');
					$this->db->update('menu', array('name' => $links));
				}
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/imp_links');
			}
		}
	}
	public function update_contact() {
		if (is_logged_in()) {
			$id = $this->uri->segment(3);
			$this->db->where('id', $id);
			$res['data'] = $this->db->get('footer_data')->result();
			dashboard_back_view('update_cont', $res);
		} else {
			redirect(base_url().'home/admin_login');
		}
	}
	public function upd_cont() {
		$input = $this->input->post();
		if ($input) {
			$id      = $input['id'];
			$address = $input['address'];
			$call    = $input['call'];
			$email   = $input['email'];
			$latit   = $input['latit'];
			$longit  = $input['longit'];
			$array   = array('address' => $address, 'call' => $call, 'email' => $email, 'latitude' => $latit, 'longitude' => $longit);
			$this->db->where('id', $id);
			$res = $this->db->update('footer_data', $array);
			if ($res) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/footer');
			}

		}
	}

	public function contact_us() {
		$input = $this->input->post();
		if ($input) {
			$name    = $input['name'];
			$email   = $input['email'];
			$subject = $input['subject'];
			$message = $input['message'];
			$contact = array('name' => $name, 'email' => $email, 'subject' => $subject, 'message' => $message);

			$record = $this->db->insert('contact', $contact);
			redirect(base_url().'home');
		}
	}

	public function nomination() {
		if (is_logged_in()) {
			dashboard_back_view('nomination');
		} else {
			redirect(base_url().'home/admin_login');
		}

	}

	public function delete_nomination() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$this->db->delete('model_content');
		redirect(base_url().'home/nomination');
	}

	public function update_nomination() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$res['data'] = $this->db->get('model_content')->result();
		dashboard_back_view('update_nomination', $res);
	}

	public function nomination_update() {
		$input = $this->input->post();

		if ($input) {
			$id                 = $input['id'];
			$title              = $input['title'];
			$heading            = $input['heading'];
			$content            = $input['article'];
			$nomination_content = $input['paragraph'];
			//show($nomination_content,1);
			$email      = $input['email'];
			$phone      = $input['phone'];
			$latitude   = $input['latitude'];
			$longitude  = $input['longitude'];
			$nomination = array(
				'title'     => $title,
				'heading'   => $heading,
				'content'   => $content,
				'email'     => $email,
				'phone'     => $phone,
				'latitude'  => $latitude,
				'longitude' => $longitude);
			//show($nomination,1);
			$this->db->where('id', $id);
			$record = $this->db->update('model_content', $nomination);

			redirect(base_url().'home/nomination');

		}
	}

	/*button and logo dynamically*/
	public function add_button() {
		$data['re']       = $this->db->get('button')->result();
		$data['logo_img'] = $this->db->get('logo_img')->result();
		// $data = array();
		dashboard_back_view('button_name', $data);
	}

	public function button_add() {
		$input = $this->input->post();
		if ($input) {
			$id          = $input['id'];
			$button_name = $input['button_name'];
			$array       = array('button_name' => $button_name);
			$this->db->where('id', $id);
			$query = $this->db->insert('button', $array);
			if ($query) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Inserted Successfully.');
				redirect(base_url().'home/add_button');
			}
		}
	}

	public function edit_buttn() {
		$id = $this->uri->segment(3);

		$this->db->select('*');
		$this->db->from('button');
		$this->db->where('id', $id);
		$res['data'] = $this->db->get()->result();
		dashboard_back_view('edit_button', $res);
	}

	public function update_button() {
		$input = $this->input->post();
		if ($input) {
			$id          = $input['id'];
			$button_name = $input['button_name'];
			$array       = array('button_name' => $button_name);
			$this->db->where('id', $id);
			$query = $this->db->update('button', $array);
			if ($query) {
				$this->session->set_flashdata('msg_type', 'success');
				$this->session->set_flashdata('msg', 'Updated Successfully.');
				redirect(base_url().'home/add_button');
			}

		}
	}

	public function delete_button() {
		$id = $this->uri->segment(3);
		$this->db->where('id', $id);
		$this->db->delete('button');
		redirect(base_url().'home/add_button');
	}

	public function edit_img() {
		$id = $this->uri->segment(3);

		$this->db->select('*');
		$this->db->from('logo_img');
		$this->db->where('id', $id);
		$res['data'] = $this->db->get()->result();
		dashboard_back_view('edit_img', $res);
	}

	public function update_img() {
		$input = $this->input->post();

		if ($input) {
			$name = $_FILES['img']['name'];
			$id   = $input['id'];
			$size = $_FILES['img']['size'];
			if ($_FILES['img']['name']) {
				$ext     = pathinfo($name, PATHINFO_EXTENSION);
				$allowed = array('gif', 'png', 'jpg', 'jpeg', '');
				if (!in_array($ext, $allowed)) {
					show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed", 1);
				} else {
					if ($size > '2000000') {
						show('Max Allowed Image Size is 2MB', 1);
					} else {
						$tpname = explode('.', $name);

						$date     = date("Y-m-d");
						$pic      = $tpname[0].'_'.$date.'.'.$tpname[1];
						$tmp_name = $_FILES['img']['tmp_name'];

						$path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';

						$res = move_uploaded_file($tmp_name, $path.'/'.$pic);
						//show($res,1);
						$this->db->where('id', $id);
						$res = $this->db->update('logo_img', array('img' => $pic));
						if ($res) {
							$this->session->set_flashdata('msg_type', 'success');
							$this->session->set_flashdata('msg', 'Successfully Updated.');
							redirect(base_url().'home/add_button');
						}
					}
				}
			} else {
				$this->session->set_flashdata('msg_type', 'danger');
				$this->session->set_flashdata('msg', 'image not selected.');
				redirect(base_url().'home/add_button');
			}

		}
	}

	public function all_list() {
		$year  = $this->input->post('year');
		$priya = '';

		$query = $this->db->query("SELECT DISTINCT(year) FROM `list_of_winners` where `year` < $year ORDER BY year DESC limit 1");
		$res   = $query->result();

		$query1 = $this->db->query("SELECT DISTINCT(year) FROM `list_of_winners` where `year` > $year ORDER BY year DESC limit 1");

		$res1 = $query1->result();

		$query2 = $this->db->query("SELECT DISTINCT(year) FROM `list_of_winners` where `year` > $year ORDER BY year ASC limit 1");

		$res2 = $query2->result();
		// alert($res);
		die();

		$div = array();

		$this->db->where('year', $year);
		$val = $this->db->get('list_of_winners')->result();

		if (!empty($res)) {
			$div['next'] = $res[0]->year;
		} else {
			$div['next'] = $res1[0]->year;
		}

		if (!empty($res2)) {
			$div['prev'] = $res2[0]->year;
		} else {
			$div['prev'] = $year;
		}
		// }else{
		//   if(!empty($res)){
		//     $div['prev'] = $res[0]->year;
		//   }
		// }
		$div['head'] = "Winner List(".$val[0]->year.")";
		//alert($val);
		die();

		foreach ($val as $row) {
			$priya .= "<div class='col-lg-4 col-xl-4 appear-animation animated appear-animation-visible' data-appear-animation='fadeInRightShorter' data-appear-animation-delay='600'>
              <div>
                  <div class='testimonial custom-testimonial-style-1'>
                      <div class='row'>
                          <div class='col-sm-12 text-center px-5'>
                              <img src='".base_url()."/uploads/".$row->img."' class='rounded-circle img-center avatar' alt=''>
                          </div>
                          <div class='text-center px-3'>
                              <div class='py-2 text-color-primary'>
                                  <strong class='text-2 mb-0'>".$row->name."</strong>
                              </div><blockquote class='blockquote-tertiary'>
                                  <p class='mb-3 img-center blockquote-tertiary'> ".$row->content."</p>
                              </blockquote>

                          </div>
                      </div>
                  </div>
              </div>
          </div>";
		}
		$div['sam'] = $priya;
		//alert($div);
		die();
		echo json_encode($div);

	}

	public function contact_list() {
		$res['contact'] = $this->db->get('contact')->result();
		dashboard_back_view('contact_list', $res);
	}

}
?>