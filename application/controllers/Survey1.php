<?php
defined('BASEPATH')or die('No direct script access allowed');

class Survey extends CI_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->model('Survey_model');
    }

    public function surveyView() {
    	if(is_logged_in()){
    		$data  = array();
    			$ses_id = $this->session->userdata('id');
    			dashboard_back_view('survey/surveyView',$data);
    		}else{
    			 redirect(base_url().'home/admin_login');
    		}	
    }
    
    public function addsurvey() {

		$startDate 		= $this->input->post('sDate');
		$endDate 		= $this->input->post('eDate');
		$surveyName 	= $this->input->post('surveyName');
		$jsonData 		= $this->input->post('jsonData');
		$description 	= $_POST['description'];

		$survey_arr  = array(
			'name'			=> $surveyName,
			'startDate' 	=> $startDate,
			'endDate'   	=> $endDate,
			'description'	=> $description,
			'questionary'	=> $jsonData,
			'answerset'		=> '[]',
			'isActive'		=> 'Y',
			'hasFees'		=> 'N',
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);

		$res =$this->Survey_model->createSurvey($survey_arr);

		if($res) {

			$this->session->set_flashdata('msg', 'Survey Created Successfully');
             $this->session->set_flashdata('msg_type', 'success');  
		}

	}

	public function survey()
	{
		if (is_logged_in()) {
	
    	$res['data']=$this->db->get('survey')->result();    
    	dashboard_back_view('survey/servey_data',$res);
    	}else{
    		 redirect(base_url().'home/admin_login');
    	}
  	}
	
	// public function surveyForm() {

	// 	$data['dt']= $this->Survey_model->where('live','Y')->find_all();
		 
	// 	load_back_view('survey/surveyForm',$res);
	// }

	public function livesurvey() {

      $dt = $this->input->post('dt');
      
      $updated = $this->Survey_model->livesurvey($dt);

      if($updated) {

        $this->session->set_flashdata('msg', 'Survey Updated Successfully');
        $this->session->set_flashdata('msg_type', 'success');  
      }
      
      else {

        $this->session->set_flashdata('msg', 'Something Went Wrong');
        $this->session->set_flashdata('msg_type', 'fail');  
      }
    }

    public function formData() {

    	$input = $this->input->post();
    	
        $survey_id = array_pop($input);
    	
		$myJSON  	= json_encode($input);
		$udata   	= get_userdata();
		
		$ip      	= $udata['ip'];
		$browser 	= $udata['browser'];
		$user_agent	= $udata['ipinfo'];
		$user_agent = json_encode($user_agent);
		
		$answer 	= $myJSON;

		$inserted = $this->Survey_model->addAnswer($survey_id,$ip,$browser,$user_agent,$answer);

		if($inserted) {

			$this->session->set_flashdata('msg', 'Survey Form Submitted Successfully');
        	$this->session->set_flashdata('msg_type', 'success'); 
 
        	redirect(base_url()); 
		} 

		else {

			$this->session->set_flashdata('msg', 'Something Went Wrong');
            $this->session->set_flashdata('msg_type', 'danger');
            redirect(base_url().'survey/surveyform/');
		}
    }

    public function surveyStatistic() {

    	$res = $this->Survey_model->surveyStatistic();
    }

    public function surveyAnswer() {
    	
    	if (is_logged_in())
    	 {
	    	$data['ans'] = $this->Survey_model->surveyAnswer();
	    	dashboard_back_view('survey/survey_ans',$data);
   		 }
   		 else
   		 {
    	 	redirect(base_url().'home/admin_login');
    	 }
    }
}