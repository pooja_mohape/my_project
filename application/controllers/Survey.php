<?php
defined('BASEPATH')or die('No direct script access allowed');

class Survey extends CI_Controller{
	
	public function __construct() {
        parent::__construct();
        $this->load->model('Survey_model');
    }

    public function surveyView() {

    	$data  = array();
    	$ses_id = $this->session->userdata('id');


    	dashboard_back_view('survey/surveyView',$data);	
		
    }
    public function addsurvey() {

		$startDate 		= $this->input->post('sDate');
		$endDate 		= $this->input->post('eDate');
		$surveyName 	= $this->input->post('surveyName');
		$jsonData 		= $this->input->post('jsonData');
		$description 	= $_POST['description'];

		$survey_arr  = array(
			'name'			=> $surveyName,
			'startDate' 	=> $startDate,
			'endDate'   	=> $endDate,
			'description'	=> $description,
			'questionary'	=> $jsonData,
			'answerset'		=> '[]',
			'isActive'		=> 'Y',
			'hasFees'		=> 'N',
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);

		$res =$this->Survey_model->createSurvey($survey_arr);

		if($res) {

            $this->session->set_flashdata('msg','Survey Created Successfully');
            $this->session->set_flashdata('msg_type', 'success');
		}
        else {

            $this->session->set_flashdata('msg','Something Went Wrong Please Try Again');
            $this->session->set_flashdata('msg_type', 'error');
        }
	}

	public function survey(){
                $this->db->order_by('id','DESC');
    $res['data']=$this->db->get('survey')->result();    
    dashboard_back_view('survey/servey_data',$res);
  
  	}
	
	// public function surveyForm() {

	// 	$data['dt']= $this->Survey_model->where('live','Y')->find_all();
		 
	// 	load_back_view('survey/surveyForm',$res);
	// }

	public function livesurvey() {

      $dt = $this->input->post('dt');
      
      $updated = $this->Survey_model->livesurvey($dt);

      if($updated) {

        $data['msg_type'] = 'success'; 
        $data['msg']      = 'Survey Is Live Now'; 
      }
      
      else {

        $data['msg_type']  = 'error';  
        $data['msg']       = 'Something Went Wrong';
      }

      echo json_encode($data);
    }

    public function formData() {

    	$input = $this->input->post();
        $user_id   = array_pop($input);
        
        $survey_id = array_pop($input);

		$myJSON  	= json_encode($input);
		$udata   	= get_userdata();
		
		$ip      	= $udata['ip'];
		$browser 	= $udata['browser'];
		$user_agent	= $udata['ipinfo'];
		$user_agent = json_encode($user_agent);
		
		$answer 	= $myJSON;

		$inserted = $this->Survey_model->addAnswer($user_id,$survey_id,$ip,$browser,$user_agent,$answer);

		if($inserted) {

            $data['msg_type'] = 'success'; 
			$data['msg']      = 'Survey Form Submitted Successfully';
        	redirect(base_url()); 
		} 

		else {

            $data['msg_type'] = 'error';
			$data['msg']      = 'Something Went Wrong';
            redirect(base_url().'survey/surveyform/');
		}

        echo json_encode($data);
    }

    public function formDataDraft() {

    	$input = $this->input->post();
    	
        $user_id   = array_pop($input);
        $survey_id = array_pop($input);

		$myJSON  	= json_encode($input);
		$udata   	= get_userdata();
		
		$ip      	= $udata['ip'];
		$browser 	= $udata['browser'];
		$user_agent	= $udata['ipinfo'];
		$user_agent = json_encode($user_agent);
		
		$answer 	= $myJSON;

		$inserted = $this->Survey_model->addDraftAnswer($user_id,$survey_id,$ip,$browser,$user_agent,$answer);

		if($inserted) {

            $data['msg_type'] = 'success'; 
			$data['msg']      = 'Survey Form Submitted Successfully';

        	redirect(base_url()); 
		} 

		else {

            $data['msg_type'] = 'error';
			$data['msg']      = 'Something Went Wrong';
            redirect(base_url().'survey/surveyform/');
		}

        echo json_encode($data);
    }

    public function surveyAnswer() {

        $dd          = $this->Survey_model->surveyAnswer();
    	$data['ans'] = $dd;
        $res         = '';
    	foreach ($dd as $key => $value) {
            
            $survey_dd = $value->questionary;
            $answer    = $value->answer;

            $survey_dd = json_decode($survey_dd);
            $answer    = json_decode($answer);


            if($answer)
            {
                foreach ($answer as $key1 => $value1)
                {
                     show($key1);
                     show($value1);   

                    foreach ($survey_dd as $skey1 => $svalue1) 
                    {
                         if($key1==$skey1) 
                         {
                            show($key1);
                         }
                    }  
                }
             }
            // show($survey_dd);
            //show($answer,1);
            
           
            show('hello',1);
        }

    	dashboard_back_view('survey/survey_ans',$data);
    	 
    }

    public function fetchDraftAns() {

    	$survey_id = $this->input->post('survey_id');
    	$user_id   = $this->input->post('x');
    	$dd='';
    	// $survey_id = 40;
    	// $user_id   = "9Dobkfngq";
    	$data = $this->db->where('survey_id',$survey_id)->where('user_id',$user_id)->get('answer')->result();
    	
        if($data)
        {

        	if($data[0]->draft=='Y')
            {
            	$dd = $data[0]->draft_answer;
        	}
        }
    	// show($data,1);
    	return $dd;
    }
}