<?php
defined('BASEPATH')or die('No direct script access allowed');

class Counters extends CI_Controller{
	public function __construct() {
        parent::__construct();
        $this->load->model('counter_model');
    }

	public function index() {

		$check_exist = $this->counter_model->find_all();
		if($check_exist)
		{
			$data['exist'] = true;
			$data['counters'] = $check_exist;
		}
		else
		{
			$data['exist'] = false;
		}
		//show($check_exist[0]->No_of_Event,1);
		dashboard_back_view('counters',$data);
	}
	public function save_counters()
	{
		$input = $this->input->post();

        $id = isset($input['id']) ? isset($input['id']) : '';
		if($input)
		{  
			$data = array('No_of_Event'      => $input['no_of_event'],
				          'successful_event' => $input['successful_event'],
				          'successful_years' => $input['successful_years'],
				          'dedicated_client' => $input['dedicated_client'] );

			if($id)
			{
                $this->db->where('id','1');
				$update  = $this->db->update('counters',$data);
				if($update)
				{
					redirect(base_url().'counters');
				}
			}
			else
			{
				$insert_counters = $this->db->insert('counters',$data);
            
	            if($insert_counters)
	            {
	            	redirect(base_url().'counters');
	            }
			}		
		}
		else
		{
			redirect(base_url().'counters');
		}
	}
}
?>