<?php
defined('BASEPATH')or die('No direct script access allowed');

class Home extends CI_Controller{
  public function __construct() {
        parent::__construct();
    }

  public function index() {
    date_default_timezone_set("Asia/Kolkata");
    $year = date('Y');
    $data = array();
    $res['data']=$this->db->get('txt_content')->result();
    $res['shortlist']=$this->db->get('shortlist_data')->result();
    $res['block']=$this->db->get('Block',1)->result();
    $res['res_slider']=$this->db->get('slider')->result();
    //get data of winner list
    $this->db->where('year',$year);
    $this->db->limit(2);
    $res['winner_list']=$this->db->get('list_of_winners')->result();
    //get list of years from database
    $this->db->distinct();
    $this->db->select('year,id');
    $res['year'] = $this->db->get('list_of_winners')->result();

    $res['logo']=$this->db->get('logo')->result();
    $counters = $this->db->select('*')->from('counters')->get()->result();
    $res['award']=$this->db->limit('1')->get('awards')->result();

    if($counters)
    {
      $res['counters'] = $counters;
    }
    load_back_view('home',$res); 
  }
    
  public function people_categories(){
    $data = array();
    load_back_view('Award _categories/people_categories',$data);
  }
  public function project_categories(){

    $data=array();
    load_back_view('Award _categories/project_categories',$data);
  }
  public function company_categories(){
    $data=array();
    load_back_view('Award _categories/company_categories',$data);
  }
  public function admin_login()
  {
     $this->load->view('admin_login');      
  }

     public function get_content(){
   
    $year = $this->input->post('year');
    $this->db->where('year',$year);
    $this->db->limit(2);
    $get = $this->db->get('list_of_winners')->result();

    foreach($get as $row){

    $div = "<div class='col-lg-6 col-xl-6 appear-animation' data-appear-animation='fadeInRightShorter' data-appear-animation-delay='600' style='opacity:1;'>
        <div>
          <div class='testimonial custom-testimonial-style-1'>
            <div class='row'>
              <div class='col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0'>
                <img src='".base_url()."/uploads/".$row->img."' class='img-fluid rounded-circle' alt='' />
              </div>
            <div class='col-lg-9'>
          <blockquote>
            <p class='mb-3'>".$row->content."</p>
          </blockquote>
            <div class='testimonial-author'>
              <strong class='text-2 mb-0'>".$row->name."</strong>
            </div>
          </div>
          </div>
        </div>
      </div>  
  </div>";
  }

  echo json_encode($div);
  }

    public function edit_section()
  {
         $this->db->select('*');
    $this->db->from('txt_content');
    $data['response']=$this->db->get()->result();
    dashboard_back_view('edit_section',$data);  
  }
  public function add_section()
  {
    $input=$this->input->post();

    if($input){
      $name=$_FILES['userfile']['name'];      
      $heading=$input['heading'];
      $content=$input['content'];
    $size = $_FILES['userfile']['size'];

      $ext = pathinfo($name, PATHINFO_EXTENSION);

      $allowed =  array('gif','png' ,'jpg','jpeg','');
      
      if(!in_array($ext,$allowed)) {
        show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size >'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }   
        else{
          
            $tpname=explode('.', $name);

           $pic=$tpname[0].'_'.$heading.'.'.$tpname[1];
           $tmp_name = $_FILES['userfile']['tmp_name'];

           $path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
           move_uploaded_file($tmp_name, $path.'/'.$pic);
    
    $res=$this->db->insert('txt_content',array('title'=>$heading,'txt'=>$content,'img'=>$pic));
    if($res){
       $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Inserted Successfully.');
      redirect(base_url().'home/edit_section');
    }
    
      }
    }
  }
}
  public function delete_section()
  {
    $id=$this->uri->segment(3);
    $this->db->where('id',$id);
    $res=$this->db->delete('txt_content');
    if($res){
    $this->session->set_flashdata('msg_type', 'success');
    $this->session->set_flashdata('msg', 'Deleted Successfully.');
    redirect(base_url().'home/edit_section');
    }
  }
  public function update_section(){
    $id=$this->uri->segment(3);
    $this->db->select('*');
    $this->db->from('txt_content');
    $this->db->where('id',$id);
    $data['data']=$this->db->get()->result();
    dashboard_back_view('edit_section_data',$data);
  }
  public function edit_section_data()
 {
        $input=$this->input->post();

        if($input){
            $name=$_FILES['userfile']['name'];  
            $id=$input['id'];
            $heading=$input['heading'];
            $content=$input['content'];
        $size = $_FILES['userfile']['size'];
        if($_FILES['userfile']['name'])
        {
           $ext = pathinfo($name, PATHINFO_EXTENSION);

          $allowed =  array('gif','png' ,'jpg','jpeg','');
         
          if(!in_array($ext,$allowed)) 
            {
            show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
            }
            else 
            {
              if($size >'2000000') {
                 show('Max Allowed Image Size is 2MB',1);
            }       
            else{
                
                $tpname=explode('.', $name);

                 $pic=$tpname[0].'_'.$heading.'.'.$tpname[1];
                 $tmp_name = $_FILES['userfile']['tmp_name'];

                 $path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
                 $res=move_uploaded_file($tmp_name, $path.'/'.$pic);

            $this->db->where('id',$id);
            $res=$this->db->update('txt_content',array('title'=>$heading,'txt'=>$content,'img'=>$pic));
            if($res){
               $this->session->set_flashdata('msg_type', 'success');
              $this->session->set_flashdata('msg', 'Updated Successfully.');
              redirect(base_url().'home/edit_section');
            }
            
                }
            }
        }
        else
        {
            $this->db->where('id',$id);
            $this->db->update('txt_content',array('title'=>$heading,'txt'=>$content));
            redirect(base_url().'home/edit_section');
        }
    }
}


  /*Slider section*/
    public function slider()
  {

    dashboard_back_view('slider');  
  }

  public function upload_slider()
    {   
        $input=$this->input->post();

        $heading=$input['heading'];
        $content=$input['content'];

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768; 

        $this->load->library('upload', $config);        

        if(! $this->upload->do_upload())
        {            
        //$this->form_validation->set_error_delimiters('<p class = "error">', '</p>');
            $error = array('error' => $this->upload->display_errors());
            dashboard_back_view('slider',$error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

            $upload_data= $data['upload_data']['file_name'];
            $this->db->insert('slider',array('img'=>$upload_data,'title'=>$heading,'content' => $content));
            //show($data,1);
           redirect(base_url().'Home/index');
        }
    }

     public function edit_slider()
    {
        //$data['r']=$this->db->get('slider')->result();
        $id=$this->uri->segment(3);
         $this->db->select('*');
        $this->db->from('slider');
        $this->db->where('id',$id);
        $data['data'] = $this->db->get()->result();   
        // show($data,1);
        dashboard_back_view('edit_slider',$data);
    } 

    public function update()
    {
        $input=$this->input->post();



        if($input){
            $name=$_FILES['userfile']['name'];  
            $id=$input['id'];
            $heading=$input['heading'];
            $content=$input['content'];
        $size = $_FILES['userfile']['size'];
        if($_FILES['userfile']['name'])
        {
           $ext = pathinfo($name, PATHINFO_EXTENSION);

          $allowed =  array('gif','png' ,'jpg','jpeg','');
         
          if(!in_array($ext,$allowed)) 
            {
            show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
            }
            else 
            {
              if($size >'2000000') {
                 show('Max Allowed Image Size is 2MB',1);
            }       
            else{
                
                $tpname=explode('.', $name);

                 $pic=$tpname[0].'_'.$heading.'.'.$tpname[1];
                 $tmp_name = $_FILES['userfile']['tmp_name'];

                 $path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/';
                 $res=move_uploaded_file($tmp_name, $path.'/'.$pic);

            $this->db->where('id',$id);
            $res=$this->db->update('slider',array('title'=>$heading,'txt'=>$content,'img'=>$pic));
            if($res){
               $this->session->set_flashdata('msg_type', 'success');
              $this->session->set_flashdata('msg', 'Updated Successfully.');
              redirect(base_url().'home/slider');
            }
            
                }
            }
        }
        else
        {
            $this->db->where('id',$id);
            $this->db->update('slider',array('title'=>$heading,'txt'=>$content));
            redirect(base_url().'home/slider');
        }
    }


    }

    public function delete($id){
        $this->db->where('id', $id);
        $res=$this->db->delete('slider');
        if($res){
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Deleted Successfully.');
        redirect(base_url().'Home/slider');
        }
    }

    /*Methodology*/


    function Methodology()
    {
         dashboard_back_view('Methodology');
    } 

    public function addArticle()
    {
        $input=$this->input->post();
        $title =  $input['title'];
        $article = $input['article'];
        $date = date('d/m/y');
        $Addtitle = $title;
        $Addarticle = $article;
         
        $data = array('title'=> $Addtitle,
            'content'=>$Addarticle,
            'created_at'=>$date
            );
        $this->db->insert('Block', $data);
    }
         
        public function loadArticle()
        {
            $query =  $this->db->get('Block');
            foreach ($query->result() as $row)
            {
            $date = date_create($row->created_at);
            $date = date_format($date, 'l jS F Y');
            echo'<blockquote><h3>'.ucfirst($row->title).'</h3></blockquote><p>'.html_entity_decode($row->content).'</p><a href="#" class="btn btn-sm btn-warning">'.$date.'</a><hr/>';
           }
           exit;
        }


        /*Winners list*/

   function winners_list()
    {
        dashboard_back_view('winners_list');
    } 

    function upload_winners()
    {   
        $input=$this->input->post();
         $year=$input['year'];
         $name=$input['name'];
        $title=$input['title'];
        $content=$input['content'];

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768; 

        $this->load->library('upload', $config);        

        if(! $this->upload->do_upload())
        {            
        //$this->form_validation->set_error_delimiters('<p class = "error">', '</p>');
            $error = array('error' => $this->upload->display_errors());
            dashboard_back_view('winners_list',$error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

            $upload_data= $data['upload_data']['file_name'];
            $this->db->insert('list_of_winners',array('img'=>$upload_data,'year'=>$year,'name'=>$name,'title'=>$title,'content' => $content));
            //show($data,1);
           redirect(base_url().'home/index');
        }
    }


     public function edit_winners()
    {
        $id=$this->uri->segment(3);
         $this->db->select('*');
        $this->db->from('list_of_winners');
        $this->db->where('id',$id);
        $data['data'] = $this->db->get()->result();   

        dashboard_back_view('edit_winners',$data);
    } 

    public function update_winners()
    {
        // show($id,1);
        $input=$this->input->post();
        if($input)
        {
        $id=$input['id'];

        $year=$input['year'];
        $name=$input['name'];
        $title=$input['title'];
        $content=$input['content'];
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpeg|png|jpg';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;   
        //show($config,1);
        
         $this->load->library('upload', $config);        

        if(! $this->upload->do_upload())
        {            
       
            $error = array('error' => $this->upload->display_errors());
            redirect(base_url().'home/winners_list',$error);
        } 

        else{
            $res=$this->upload->data();
            $data = array('upload_data' => $res);
            $file_name=$data['upload_data']['file_name'];
            $array=array('img'=>$file_name,'year'=>$year,'name'=>$name,'title'=>$title,'content' => $content);
          //show($array,1);
            $this->db->where('id',$id);    
            $query=$this->db->update('list_of_winners',$array);
            redirect(base_url().'home/winners_list');
        }
    }
    }

    public function winners_delete($id){
        $this->db->where('id', $id);
        $this->db->delete('list_of_winners');
        redirect(base_url().'Home/winners_list');
    }



    /*Award Shortlist*/
         function shortlist_data()
    {
        dashboard_back_view('shortlist');
    } 

    function upload_shortlist()
    {   
        $input=$this->input->post();

        $title=$input['title'];
        $content=$input['content'];
        $this->db->insert('shortlist_data',array('title'=>$title,'content' => $content));
        //show($data,1);
        redirect(base_url().'Home/index');
    }

    public function edit_shortlist()
    {
        $id=$this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('shortlist_data');
        $this->db->where('id',$id);
        $data['data'] = $this->db->get()->result();   

        dashboard_back_view('edit_shortlist',$data);
    } 

    public function update_shortlist()
    {
       // show($id,1);
        $input=$this->input->post();
        if($input)
        {
            $id=$input['id'];
            $title=$input['title'];
            $content=$input['content'];
            $array=array('title'=>$title,'content'=>$content);

            $this->db->where('id',$id);    
            $query=$this->db->update('shortlist_data',$array);
            redirect(base_url().'home/shortlist_data');
        }
    }

    public function delete_shortlist($id){
        $this->db->where('id', $id);
        $this->db->delete('shortlist_data');
        redirect(base_url().'Home/shortlist_data');
    }
    public function login_authenticate()
    {
        $input  = $this->input->post();
        if($input)
        {   
            $this->load->model('users_model');
            $username = isset($input['username']) ? $input['username'] : '';
            $password = isset($input['password']) ? $input['password'] : '';
            if($username && $password)
            {
              $username = $this->users_model->where('username',$username)->find_all();
              if($username)
              {
                   $check_password = $this->users_model->where('password',$password)->find_all();
                   if($check_password)
                   {
                      $user_array = $check_password[0];
                      $user_data = array(
                                "id" => $user_array->id,
                                "username" => $user_array->username,
                                "email" => $user_array->email,
                                "first_name" => $user_array->fname,
                                "last_name" => $user_array->lname,
                         );

                        $this->session->set_userdata($user_data);

                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Welcome To Award.');
                        redirect(base_url().'home/dashboard');
                   }
                   else
                   {
                      redirect(base_url().'home/admin_login');
                   }
              }
              else
              {
                 redirect(base_url().'home/admin_login');
              }
            }
            else
            {
                redirect(base_url().'home/admin_login');
            }
        }else
        {
            redirect(base_url().'home/admin_login');
        }           
    }
    public function dashboard()
    {
        if(is_logged_in())
        {
            dashboard_back_view('admin_dashboard');
        }
        else
        {
            redirect(base_url().'home/admin_login');
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
    public function section_logo(){
    dashboard_back_view('logo');

    }
    public function add_logo(){
        $input=$this->input->post();
        $name=$_FILES['userfile']['name'];  
        $size = $_FILES['userfile']['size'];
        $ext = pathinfo($name, PATHINFO_EXTENSION);

      $allowed =  array('gif','png' ,'jpg','jpeg','');
      
      if(!in_array($ext,$allowed)) {
        show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size >'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }       
        else{
            $date=date("Y-m-d_H-i-s");
            $tpname=explode('.', $name);
             $pic=$tpname[0].'_'.$date.'.'.$tpname[1];
             $tmp_name = $_FILES['userfile']['tmp_name'];
            
             $path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/logo';
             //chmod($path,0777);
             $res=move_uploaded_file($tmp_name, $path.'/'.$pic);
             
        $res=$this->db->insert('logo',array('img'=>$pic));
        if($res){
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Inserted Successfully.');
        redirect(base_url().'home/section_logo');
              }
            }
        }
    }   
    public function delete_logo(){
        $id=$this->uri->segment(3);
        $this->db->where('id',$id);
        $res=$this->db->delete('logo');
        if($res){
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Deleted Successfully.');
        redirect(base_url().'home/section_logo');
        }      
    }
    public function update_logo(){
        $id=$this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('logo');
        $this->db->where('id',$id);
        $res['data']=$this->db->get()->result();
        dashboard_back_view('edit_logo',$res);
        
    }
    public function edit_logo_section()
        {
        $input=$this->input->post();

        if($input){
            $name=$_FILES['userfile']['name'];          
            $id=$input['id'];
            
        $size = $_FILES['userfile']['size'];
        if($_FILES['userfile']['name'])
        {
             $ext = pathinfo($name, PATHINFO_EXTENSION);

      $allowed =  array('gif','png' ,'jpg','jpeg','');
      
      if(!in_array($ext,$allowed)) {
        show("Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed",1);
        }
        else {
        if($size >'2000000') {
          show('Max Allowed Image Size is 2MB',1);
        }       
        else{
            
            $tpname=explode('.', $name);
            $date=date("Y-m-d_H-i-s");
             $pic=$tpname[0].'_'.$date.'.'.$tpname[1];
             
             $tmp_name = $_FILES['userfile']['tmp_name'];

             $path = $_SERVER['DOCUMENT_ROOT'].'/awards/uploads/logo';
             
             $res=move_uploaded_file($tmp_name, $path.'/'.$pic);

        $this->db->where('id',$id);
        $res=$this->db->update('logo',array('img'=>$pic));
        if($res){
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Successfully Updated.');
        redirect(base_url().'home/section_logo');
        }
                } 
            }
        }else{
        $this->session->set_flashdata('msg_type', 'danger');
        $this->session->set_flashdata('msg', 'image not selected.');
         redirect(base_url().'home/section_logo');
            }

        }
    }

    public function award(){
        dashboard_back_view('awards');  
    }
  
    public function add_award(){
        $input=$this->input->post();
        if($input){
          $heading=$input['heading'];
          $title=$input['title'];
          $paragraph=$input['paragraph'];
          $array=array('heading'=>$heading,'title'=>$title,'paragraph'=>$paragraph);
          $this->db->insert('awards',$array);
          redirect(base_url().'home/award');
        }
    }

    public function delete_awards(){
        $id=$this->uri->segment(3);
        $this->db->where('id',$id);
        $this->db->delete('awards');
        redirect(base_url().'home/award');
    }

    public function update_awards(){
        $id=$this->uri->segment(3);
        $this->db->where('id',$id);
        $res['data']=$this->db->get('awards')->result();
         dashboard_back_view('awards_update',$res);
    }

    public function update_awrds(){
        $input=$this->input->post();

        if($input){
          $id=$input['id'];
          $heading=$input['heading'];
          $title=$input['title'];
          $paragraph=$input['paragraph'];
          $array=array('heading'=>$heading,'title'=>$title,'paragraph'=>$paragraph);
          $this->db->where('id',$id);
          $this->db->update('awards',$array);
          redirect(base_url().'home/award');
        }
    }
    
}
?>