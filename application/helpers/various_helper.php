<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('show')) {
    function show($var, $flag = 0, $msg = '')
    {
            echo "<pre> <b>------------------------  START $msg  ------------------------   </b> <br/> <br/>";
            if (is_array($var) || is_object($var)) {
                print_r($var);
            } else {
                echo $var;
            }
            echo " <br/>   <br/>  <b>------------------------  END $msg  ------------------------   </b></pre>";
            if ($flag > 0)
                exit;
    }
}


if(!function_exists('CcaSendDataOverPost'))
{
    function SendDataOverPost($url, $fields = "")
    {
        if (!is_array($fields) || empty($fields))
        {
            return "error";
        }

        $CI = &get_instance();
        $fieldsData = "";

        $url = str_replace(" ", "%20", $url);

        $postvars   =   '';
        $sep        =   '';
        foreach($fields as $key=>$value)
        {
            $postvars.= $sep.urlencode($key).'='.urlencode($value);
            $sep='&';
        }
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);

        $result['response']  = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);

        if($result['info']['http_code'] == '200')
        {
                return $result['response'];
        }else{
                return false;
        }
    }
}
if (!function_exists('getDataFromRemote')) {
    function getDataFromRemote($url, $fields = array(), $auth = 0)
    {
        $CI = &get_instance();

        $url = str_replace(" ", "%20", $url);
        $result = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100); 

        $result['response']       = curl_exec($ch);
        $result['info']           = curl_getinfo($ch);
        $result['info']['errno']  = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);
        if($result['info']['http_code'] == '200')
        {
                return $result['response'];
        }else{
                return false;
        }
    }
}
function remove_html_entities($result = array())
{

    if (is_array($result)) {
        foreach ($result as $key => $val) {
            if (is_array($val)) {
                $result[$key] = remove_html_entities($val);
            } elseif (is_object($val)) {
                $result[$key] = remove_html_entities($val);
            } else {
                $result[$key] = html_entity_decode($val);
            }
        }
    } elseif (is_object($result)) {
        foreach ($result as $key => $val) {
            if (is_object($val)) {
                $result->$key = remove_html_entities($val);
            } elseif (is_array($val)) {
                $result->$key = remove_html_entities($val);
            } else {
                $result->$key = html_entity_decode($val);
            }
        }
    }

    return $result;
}
   
   function  getmytab()
   {
        $CI = &get_instance();
        $role_id = $CI->session->userdata('role_id');
        $CI->load->model('users_model');
        $check_my_menu = $CI->users_model->getMymenu($role_id);
        return $check_my_menu;
   }

if (!function_exists('data2json')) {

    function data2json($data)
    {
        $CI = &get_instance();
        $CI->output->set_content_type('application/json')
            ->set_output(json_encode($data));

    }
}
if (!function_exists('getRandomId')) {
    function getRandomId($numchars = 8, $type = "alphanumeric")
    {
        $numeric = "0,1,2,3,4,5,6,7,8,9";
        $alphabetic = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";

        if ($type == "numeric")
            $char_str = $numeric;
        else if ($type == "alphabetic")
            $char_str = $alphabetic;
        else
            $char_str = $numeric . "," . $alphabetic;

        $chars = explode(',', $char_str);
        $randid = '';

        for ($i = 0; $i < $numchars; $i++)
            $randid .= $chars[rand(0, count($chars) - 1)];

        return $randid;
    }
}
if (!function_exists('log_data')) {
    function log_data($file, $data = NULL, $commet = '')
    {
        if ($file != '' && $data != NULL) {
            $file = FCPATH . 'log/' . $file;
            make_dir($file, 0770, true);

            $data = print_r($data, true);

            file_put_contents($file, PHP_EOL . '/******** start ' . $commet . ' **********/ ' . PHP_EOL, FILE_APPEND);
            file_put_contents($file, $data, FILE_APPEND);
            file_put_contents($file, PHP_EOL . '/******** end ' . date("H:i:s") . "********" . $commet . ' **********/' . PHP_EOL . PHP_EOL, FILE_APPEND);
        }
    }
}
if (!function_exists('make_dir')) {
    function make_dir($path, $mode = 0755, $recursive = false)
    {
        $isfile = strpos(basename($path), '.');

        if ($isfile) {
            $path = substr($path, 0, strrpos($path, '/'));
        }
        if (!file_exists($path)) {
            mkdir($path, $mode, $recursive);
            chmod($path, 0770);
        }
    }
}
if (!function_exists('last_query')) {
    function last_query($flag = 0)
    {
        $CI = &get_instance();
        show($CI->db->last_query(), $flag);
    }
}

if(!function_exists('sms_encrypt'))
{
    function sms_encrypt($plainText,$key)
    {
        // Hashing Api key
        $hash = hash( "sha256", $key , true );  
        // Convert into 16 chr string
        $string = substr($hash, 0,16);

        $blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $paddingSize = $blocksize - (strlen($plainText) % $blocksize);
        $plainText .= str_repeat(chr($paddingSize), $paddingSize);

        $rtn = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $string, $plainText, MCRYPT_MODE_CBC, $string);
        $rtn = base64_encode($rtn);

        return  $rtn;
    }
}

if(!function_exists('sms_decrypt'))
{
    function sms_decrypt($encryptedText,$key)
    {
        // decode encrypted Text
        $decoded = base64_decode($encryptedText);
       
        // Hashing Api key
        $hash = hash( "sha256", $key , true );
        // Convert into 16 chr string
        $string = substr($hash, 0,16);

        $plainText = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $string, $decoded, MCRYPT_MODE_CBC, $string);

        $len = strlen($plainText);
        $pad = ord($plainText[$len-1]);

        return substr($plainText, 0, $len - $pad);
    }
}
if(!function_exists('get_userdata'))
{
    function get_userdata()
    {
        $CI =& get_instance();
        $CI->load->library('user_agent');
        $ip = $CI->input->ip_address();
        $browser = $CI->agent->browser();
        $url = "http://ipinfo.io/".$ip."/geo";
        $json_data = file_get_contents($url);
        $details = json_decode($json_data);
        $data['ip'] = $ip;
        $data['browser'] = $browser;
        $data['ipinfo'] = $details;

       // $data['token'] = md5($_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_PORT'].$_SERVER['REMOTE_ADDR']);
        
        return $data;

    }
}