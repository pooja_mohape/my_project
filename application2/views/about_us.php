<div role="main" class="main">
				
	<section class="section section-parallax parallax m-0 py-5" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="<?php echo base_url();?>/assets/img/demos/insurance/parallax/parallax-2.jpg">
		<div class="container">
			<div class="row">
				<div class="col p-5">
					<h1 class="custom-primary-font custom-fontsize-7 text-color-light text-center m-5">About Us</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="section background-color-light custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row text-center">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">WHO WE ARE</h2>
					<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit magna, consectetur at suscipit eu.</p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit magna, consectetur at suscipit eu, dapibus vel odio. Mauris ac nulla at ligula interdum ullamcorper. Nunc mattis eros nec eros dictum, nec molestie metus auctor. Nulla placerat nunc velit, a dictum lectus finibus quis. Nunc leo mauris, cursus vel tempor eu, tempus sed turpis. Cras mattis nisl auctor tellus maximus, id consectetur nulla suscipit. Praesent consequat elit vitae ipsum porttitor, at facilisis enim hendrerit. Morbi tincidunt ornare scelerisque. Maecenas et iaculis libero, in volutpat arcu. Vestibulum ac sagittis felis. Aenean tempor tellus id felis finibus, quis aliquam sem pretium.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="section background-color-quaternary custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter" data-plugin-options="{'accY': -100}">COMPANY HISTORY</h2>
					<div class="row align-items-center">
						<div class="col-lg-2 mb-4 mb-lg-0">
							<div class="tabs tabs-vertical tabs-left tabs-navigation custom-tabs-navigation-1 border-0 mb-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item mb-0 active">
										<a class="nav-link custom-primary-font text-center active" href="#tabsNavigation1" data-toggle="tab">2000</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation2" data-toggle="tab">2005</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation3" data-toggle="tab">2010</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation4" data-toggle="tab">2018</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-10">
							<div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabsNavigation1">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0">
										<img src="<?php echo base_url();?>/assets/img/demos/insurance/history/history-1.jpg" class="img-fluid appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" alt="" />
									</div>
									<div class="col-lg-6 col-xl-7 pl-lg-5 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Lorem ipsum dolor sit amet, Vivamus enim dui, vestibulum at tortor.</h3>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porttitor velit ut sem consectetur rhoncus. Nulla eu elementum magna. Donec diam neque, condimentum a massa ut, luctus molestie risus. Donec lobortis mauris ac faucibus ullamcorper. Sed imperdiet, ipsum sed efficitur volutpat, turpis risus ipsum, at faucibus est mauris ut.</p>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0">
										<img src="<?php echo base_url();?>/assets/img/demos/insurance/history/history-2.jpg" class="img-fluid" alt="" />
									</div>
									<div class="col-lg-6 col-xl-7 pl-lg-5">
										<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus enim dui, vestibulum at tortor.</h3>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porttitor velit ut sem honcus. Nulla eu elementum magna. Donec diam neque, condimentum a massa ut, luctus molestie risus. Donec lobortis mauris ac faucibus ullamcorper. Sed imperdiet, ipsum sed efficitur volutpat, turpis risus convallis ipsum, at faucibus est mauris ut.</p>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0">
										<img src="<?php echo base_url();?>/assets/img/demos/insurance/history/history-3.jpg" class="img-fluid" alt="" />
									</div>
									<div class="col-lg-6 col-xl-7 pl-lg-5">
										<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Lorem consectetur adipiscing elit. Vivamus enim dui, vestibulum at tortor.</h3>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porttitor t sem consectetur rhoncus. Nulla eu elementum magna. Donec diam neque, condimentum a massa ut, luctus molestie risus. Donec lobortis mauris ac faucibus ullamcorper. Sed imperdiet, ipsum sed efficitur volutpat, turpis risus convallis ipsum, at faucibus est mauris ut.</p>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0">
										<img src="<?php echo base_url();?>/assets/img/demos/insurance/history/history-4.jpg" class="img-fluid" alt="" />
									</div>
									<div class="col-lg-6 col-xl-7 pl-lg-5">
										<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Lorem ipsum dolor sit amet, consectetur elit. Vivamus enim dui, vestibulum at tortor.</h3>
										<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porttitor velit ut sem . Nulla eu elementum magna. Donec diam neque, condimentum a massa ut, luctus molestie risus. Donec lobortis mauris ac faucibus ullamcorper. Sed imperdiet, ipsum sed efficitur volutpat, turpis risus convallis ipsum, at faucibus est mauris ut.</p>
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</section>

	<section class="section background-color-light custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row counters pb-2 mb-5">
				<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
					<div class="counter">
						<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="28">0</strong>
						<label class="font-weight-normal custom-fontsize-5 px-5">Million Happy <br>Customers</label>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
					<div class="counter">
						<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="99" data-append="%">0</strong>
						<label class="font-weight-normal custom-fontsize-5 px-5">Great Claims <br>Services</label>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
					<div class="counter">
						<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="18" data-append="+">0</strong>
						<label class="font-weight-normal custom-fontsize-5 px-5">Year of <br>Experience</label>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="counter">
						<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="1200">0</strong>
						<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Employs</label>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col">
					<a href="#" class="btn btn-secondary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">CHOOSE YOUR INSURANCE TODAY</a>
				</div>
			</div>
		</div>
	</section>