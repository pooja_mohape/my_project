<div role="main" class="main">

	<div class="slider-container rev_slider_wrapper" style="height: 645px;">
		<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.7">
			<ul>
				<li data-transition="fade">
					<img src="<?php echo base_url();?>/assets/img/demos/insurance/slides/slides-1.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					data-kenburns="on" 
					data-duration="20000" 
					data-ease="Linear.easeNone" 
					data-scalestart="110" 
					data-scaleend="100" 
					data-offsetstart="250 100" 
					class="rev-slidebg">

					<div class="tp-caption background-color-light"
					data-x="['left','left','20','20']"
					data-y="center"
					data-width="['400','400','400','430']"
					data-height="['405','405','405','450']"
					data-start="1000"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;"></div>

					<h1 class="tp-caption custom-primary-font font-weight-normal"
					data-x="['60','60','80','80']"
					data-y="center" data-voffset="['-105','-105','-105','-125']"
					data-width="['310','310','310','310']"
					data-start="1300"
					data-fontsize="['34','34','34','44']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;"
					style="white-space: normal;">The Petroleum Economist Awards, 2017</h1>

					<div class="tp-caption"
					data-x="['65','65','85','85']"
					data-y="center" data-voffset="['10','10','10','17']"
					data-width="300"
					data-start="1600"
					data-fontsize="['14','14','14','20']"
					data-lineheight="['24','24','24','29']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;"
					style="white-space: normal;">Thank you to all those who submitted entries, attended the awards ceremony and helped make the evening such a success...</div>

					<a class="tp-caption btn btn-secondary font-weight-bold custom-btn-style-1"
					href="#"
					data-x="['65','65','85','85']"
					data-y="center" data-voffset="['120','120','120','145']"
					data-start="1900"
					data-fontsize="['14','14','14','20']"
					data-paddingtop="['11','11','11','16']"
					data-paddingbottom="['11','11','11','16']"
					data-paddingleft="['32','32','32','42']"
					data-paddingright="['32','32','32','42']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;">GET A QUOTE</a>

				</li>
				<li data-transition="fade">
					<img src="<?php echo base_url();?>/assets/img/demos/insurance/slides/slide-2.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					data-kenburns="on" 
					data-duration="20000" 
					data-ease="Linear.easeNone" 
					data-scalestart="110" 
					data-scaleend="100" 
					data-offsetstart="-250 -100" 
					class="rev-slidebg">

					<h1 class="tp-caption custom-primary-font font-weight-normal text-center"
					data-x="center"
					data-y="center" data-voffset="['-85','-85','-85','-85']"
					data-width="['500','500','500','500']"
					data-start="1300"
					data-fontsize="['34','34','34','44']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;"
					style="white-space: normal;">Industry Leader</h1>

					<div class="tp-caption text-center"
					data-x="center"
					data-y="center" data-voffset="['-20','-20','-20','-20']"
					data-width="500"
					data-start="1600"
					data-fontsize="['14','14','14','20']"
					data-lineheight="['24','24','24','29']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;"
					style="white-space: normal;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever...</div>

					<a class="tp-caption btn btn-secondary font-weight-bold custom-btn-style-1"
					href="#"
					data-x="['center','center','center','center']"
					data-y="center" data-voffset="['50','50','50','50']"
					data-start="1900"
					data-fontsize="['14','14','14','20']"
					data-paddingtop="['11','11','11','16']"
					data-paddingbottom="['11','11','11','16']"
					data-paddingleft="['32','32','32','42']"
					data-paddingright="['32','32','32','42']"
					data-transform_in="y:[100%];opacity:0;s:500;"
					data-transform_out="opacity:0;s:500;">FIND AN AGENT</a>

				</li>
			</ul>
		</div>
	</div>
	<section class="section background-color-light border-0 my-0">
		<div class="container">
			<div class="row appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1300">
				<div class="col">
					<div class="owl-carousel owl-theme stage-margin custom-carousel-style-1 mb-0" data-plugin-options="{'items': 5, 'margin': 10, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-1.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-2.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-3.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-4.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-5.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-6.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-5.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-4.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-3.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-2.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-1.png" alt="">
						</div>
						<div>
							<img class="img-fluid" src="<?php echo base_url();?>/assets/img/logos/logo-6.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<hr class="solid custom-opacity-2 mt-0">
	<section class="section background-color-light custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row text-center">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">The Petroleum Economist Awards, 2017</h2>
					<p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">Petroleum Economist’s annual awards ceremony was held on 21 November 2017</p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Thank you to all those who submitted entries, attended the awards ceremony and helped make the evening such a success.</p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">Congratulations to all those shortlisted and especially the winners - you can find a list of all the 2017 winners <a href="#">here</a>.</p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">To see our 2016 winners click <a href="#">here</a>.</p>
					<p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">If you have any questions regarding the awards you can get in touch <a href="#">here</a>.</p>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<hr style=" border:1px solid #fff">
			<div class="row text-center">
				<div class="col">			
				<div class="row counters">
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="28">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">No.of<br>Event</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="99" data-append="%">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful Event<br>Ratio</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="18" data-append="+">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Successful<br>Years</label>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="counter">
							<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="1200">0</strong>
							<label class="font-weight-normal custom-fontsize-5 px-5">Dedicated <br>Clients</label>
						</div>
					</div>
				</div>
				</div>
			</div>
			<hr style=" border:1px solid #fff">
		</div>
	</section>
	<section class="section background-color-light custom-padding-3 border-0 my-0" id="award_methodology">
		<div class="container">
			<div class="row">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter">Award Methodology</h2>
					<p>Gulf Publishing Company takes the judging of the Petroleum Economist Awards to be a serious matter. In the interests of transparency the methodology is detailed below:</p>
					<p>1. <em>Petroleum Economist</em> Awards ‘call for award nominations’ is published as an advertisement with email alerts also sent out. The nomination deadline will be Thursday 6 July.</p>
					<p>2. The award nominations that come in are sorted into their award category. A staff member passes the sorted category nominations to the first stage <em>Petroleum Economist</em> judging team of Helen Robertson (Managing Editor) and another editorial member TBC</p>
					<p>3. The <em>Petroleum Economist</em> judging team, independently of each other, read each of the submissions and decide if it:</p>
					<ul>
						<li>Meets the criteria of the category</li>
						<li>How worthy it is of being a finalist</li>
					</ul>
					<p>The <em>Petroleum Economist</em> judging team grades each submission 1 to 10 (with 10 being the most suitable for the finalist list). The first stage judges pass on the top five nominations for each category, unless the total nominations exceed 15, in which case six go on to the second stage. If there are less than five entries all nominations go on to the second stage of judging.</p>
					<p>While the <em>Petroleum Economist</em> judging team is making the finalist selections, an e-mail goes out to the advisory board (the second stage judges) asking them to select which award category(ies) they are able to judge. A board member cannot judge a category in which their employer is a finalist.</p>
					<p>1. The Advisory board members are industry experts from the field and academia. A staff member sends a packet of the finalist selections to each advisory board member for each award category they have selected to judge.</p>
					<p>2. With attention to confidentiality, the awards submissions are returned rated 1 to 10 and a staff member sorts the information, notes the winners and prepares the information for the Gala dinner and supplement.</p>
					<p>3. In the unlikely event of a tie following the second stage of judging, the tie will be broken by an impartial industry expert on the <em>Petroleum Economist</em> staff or contributing editorial board.</p>

				</div>
			</div>
		</div>
	</section>
	<section class="section background-color-quaternary custom-padding-3 border-0 my-0" id="winners_list">
		<div class="container">
			<div class="row mb-3">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">Winners List</h2>
					<div class="row align-items-center">
						<div class="col-lg-2 mb-4 mb-lg-0">
							<div class="tabs tabs-vertical tabs-left tabs-navigation custom-tabs-navigation-1 border-0 mb-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<ul class="nav nav-tabs col-sm-3">
									<li class="nav-item mb-0 active">
										<a class="nav-link custom-primary-font text-center active" href="#tabsNavigation1" data-toggle="tab">2018</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation2" data-toggle="tab">2012</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation3" data-toggle="tab">2010</a>
									</li>
									<li class="nav-item mb-0">
										<a class="nav-link custom-primary-font text-center" href="#tabsNavigation4" data-toggle="tab">2008</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-10 mb-4 mb-lg-0">
							<div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabsNavigation1">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-1.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>
														</div>
													</div>
												</div>
											</div>
										</div>	
									</div>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-1.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation2">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-2.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>

										</div>	
									</div>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-1.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-3.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-1.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
								<div class="row align-items-center">
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-1.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>	

									</div>
									<div class="col-lg-6 col-xl-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo base_url();?>/assets/img/demos/insurance/authors/author-4.jpg" class="img-fluid rounded-circle" alt="" />
													</div>
													<div class="col-lg-9">
														<blockquote>
															<p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id dolor eu metus porta maximus. Curabitur scelerisque diam at nibh posuere, eu tristique.</p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0">DEIRDRE WALDRIP</strong>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	
	<section class="section background-color-light custom-padding-1 border-0 my-0" id="awards_shortlist">
			<div class="container">
				<div class="row mb-3">
					<div class="col">
						<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter" style="color: green">Awards Shortlist</h2>
							<div class="owl-carousel owl-theme" data-plugin-options="{'items': 4, 'autoplay': true, 'autoplayTimeout': 3000}">
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Chief Executive of the Year</h2>

									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Saad Al Kaabi, Qatar Petroleum

											Harib Al Kitani, Oman LNG

											José Antonio González Anaya, Petróleos Mexicanos (Pemex)

											Dr Maikanti Baru, Nigerian National Petroleum Corporation

										Mustafa Sanallah, National Oil Corporation - Libya</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Cleaner Energy Initiative of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Fermaca

											Kuwait Petroleum International Sustainable Retail Site

											Kuwait Petroleum International Project Jupiter

										Repsol Green Bonds</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">CSR Program of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">CITGO Petroleum Corporation

											HPCL-Mittal Energy

											Kuwait Petroleum International

										PTT Global Chemical</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Downstream Company of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">HPCL-Mittal Energy

											Kuwait Petroleum International

											Neste

											OMV

										PKN Orlen</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Company of the Year (Large Cap)</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Bahrain National Gas Company (B.S.C.)

											Cheniere

											National Oil Corporation - Libya

											Statoil

										Total</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Company of the Year (Mid Cap)</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Encana

											Marathon Oil

										Murphy Oil</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Company of the Year (Small Cap)</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Carrizo

										Hurricane Energy

										Sound Energy</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Consultancy of the Year (Performance Improvement)</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Energy and Technical Services Ltd

										Opportune LLP</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Consultancy of the Year (Public Relations)</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Armitage Communications

											Edelman

										Hill+Knowlton Strategies</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Executive of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Robin Mills, Qamar Energy

											Francis Njogu, Gulf Energy

											Dr Debesh C. Patra, Bharat Petroleum Corporation Ltd.

										Maria Sferruzza, Baker Hughes, a GE Company</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Energy Finance Provider of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">BNP Paribas

											Greensill Capital

											Natixis

										Santander</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Exploration Company of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Continental

											Eni

											Lundin Petroleum

											Nalcor Energy

											Statoil

										Total</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Future Leader</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Ahmed Almaghaslah, Saudi Aramco

											Dr Abhishek Deshpande

											Jason Ethier, Dynamo Micropower

											Jocelyne Machevo, Eni

											Zainub Noor, Halliburton

											Ranali Perera, Petroleum Resources Development Secretariat

										Sam (Yinglin) Xu, CohnReznick Capital</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Legacy Award</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Boris Ganke

											Tun Dr Mahathir Mohamad

											Marty Stromquist

										Kuppusamy Uthaman</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Legal Services Provider of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Ashurst

											Bracewell (UK) LLP

											FCB Sociedade de Advogados

											Vinson & Elkins LLP

										White & Case LLP</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Minister of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Khalid A. Al-Falih

											Emmanuel Ibe Kachikwu

											Alexander Novak

											Dharmendra Pradhan

										Bijan Zanganeh</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Petrochemicals Company of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Ineos

										PETRONAS Chemicals Group Berhad

										Sadara Chemical Company</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Project of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">Antwerp Blending Plant, Kuwait Petroleum International

										Capability Driven Approach (CDA) 2.0, Malaysian Reﬁning Company

										Liquid Mud Plant Vessel, Zentech Inc.

										Sipchem Maintenance and Reliability Transformation for Operations (SMARTO), Saudi International Petrochemical Compan</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Service Company of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">NCS Multistage

										Petrofac

										Schlumberger

										TechnipFMC</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
								<div>
									<h2 class="custom-primary-font custom-fontsize-1 font-weight-normal mb-0">Technology Company of the Year</h2>
									<div class="feature-box-info pl-0 pr-3">
										<p class="mb-3">GlassPoint Solar

										INFRA GTL Technology

										Moblize

										Petrotechnics

										Siemens</p>
										<a href="demo-insurance-insurances-detail.html">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
	</section>

	<section class="parallax section section-parallax custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" data-image-src="<?php echo base_url();?>/assets/img/demos/insurance/parallax/parallax-1.jpg" id="contact">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-md-9 col-lg-7 col-xl-6">
					<div class="card background-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
						<div class="card-body p-5">
							<h2 class="custom-primary-font font-weight-normal text-6 line-height-sm mb-3">FIND AN AGENT</h2>
							<p class="pb-2 mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt neque at vestibulum volutpat. Phasellus facilisis.</p>
							<form action="#" method="get">
								<div class="form-row">
									<div class="form-group col-md-6 pr-md-0">
										<div class="custom-select-1">
											<select class="form-control custom-left-rounded-form-control" required>
												<option value="0">INSURANCE</option>
												<option value="1">HOUSE INSURANCE</option>
												<option value="2">LIFE INSURANCE</option>
												<option value="3">TRAVEL INSURANCE</option>
												<option value="4">VEHICLE INSURANCE</option>
											</select>
										</div>
									</div>
									<div class="form-group col-md-6 pl-md-0">
										<input type="text" class="form-control custom-right-rounded-form-control" name="zip" value="" placeholder="ZIP CODE" required>
									</div>
								</div>
								<div class="form-row">
									<div class="form-group col mb-0">
										<input type="submit" value="FIND YOUR NEAREST AGENT" class="btn btn-secondary custom-btn-style-1 py-3 text-4 w-100">
									</div>
								</div>
							</form>
						</div>
						<div class="card-footer background-color-primary border-0 custom-padding-2">
							<div class="row">
								<div class="col-md-6 mb-5 mb-md-0">
									<div class="feature-box align-items-center">
										<div class="feature-box-icon background-color-tertiary">
											<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
										</div>
										<div class="feature-box-info">
											<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
											<a href="mailto:you@domain.com" class="text-color-light custom-fontsize-2">info@porto.com</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="feature-box align-items-center">
										<div class="feature-box-icon background-color-tertiary">
											<i class="fas fa-phone text-3"></i>
										</div>
										<div class="feature-box-info">
											<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
											<a href="tel:123-456-7890" class="text-color-light custom-fontsize-2">123-456-7890</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section background-color-quaternary custom-padding-3 border-0 my-0">
		<div class="container">
			<div class="row mb-4">
				<div class="col">
					<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-0 appear-animation" data-appear-animation="fadeInUpShorter">LATEST NEWS</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
					<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
						<div class="thumb-info-wrapper m-0">
							<a href="#"><img src="<?php echo base_url();?>/assets/img/demos/insurance/news/news-1.jpg" class="img-fluid" alt=""></a>
						</div>
						<div class="thumb-info-caption custom-padding-4 d-block">
							<span class="text-color-primary font-weight-semibold d-block mb-2">15 FEB 2018</span>
							<h3 class="custom-primary-font text-transform-none text-5 mb-3"><a href="#" class="text-decoration-none custom-link-style-1">Lorem ipsum dolor sit amet, adipiscing</a></h3>
							<span class="thumb-info-caption-text line-height-md text-3 p-0 m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam risus et enim euismod luctus. Nam pharetra...</span>
						</div>
					</article>
				</div>
				<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
					<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">
						<div class="thumb-info-wrapper m-0">
							<a href="#"><img src="<?php echo base_url();?>/assets/img/demos/insurance/news/news-2.jpg" class="img-fluid" alt=""></a>
						</div>
						<div class="thumb-info-caption custom-padding-4 d-block">
							<span class="text-color-primary font-weight-semibold d-block mb-2">18 FEB 2018</span>
							<h3 class="custom-primary-font text-transform-none text-5 mb-3"><a href="#" class="text-decoration-none custom-link-style-1">Lorem ipsum dolor sit amet, adipiscing</a></h3>
							<span class="thumb-info-caption-text line-height-md text-3 p-0 m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse venenatis in velit et venenatis. Fusce at est...</span>
						</div>
					</article>
				</div>
				<div class="col-md-6 col-lg-4">
					<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
						<div class="thumb-info-wrapper m-0">
							<a href="#"><img src="<?php echo base_url();?>/assets/img/demos/insurance/news/news-3.jpg" class="img-fluid" alt=""></a>
						</div>
						<div class="thumb-info-caption custom-padding-4 d-block">
							<span class="text-color-primary font-weight-semibold d-block mb-2">22 FEB 2018</span>
							<h3 class="custom-primary-font text-transform-none text-5 mb-3"><a href="#" class="text-decoration-none custom-link-style-1">Lorem ipsum dolor sit amet, adipiscing</a></h3>
							<span class="thumb-info-caption-text line-height-md text-3 p-0 m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras elementum nec ligula ut dapibus. Nulla quis posuere...</span>
						</div>
					</article>
				</div>
			</div>
			<div class="row text-center mt-5">
				<div class="col">
					<a href="#" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-2">VIEW ALL</a>
				</div>
			</div>
		</div>
	</section>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR-API-KEY"></script>
<script type="text/javascript" src="javascripts/jquery.googlemap.js"></script>
<script type="text/javascript">
  $(function() {
    $("#map").googleMap({
      zoom: 10, // Initial zoom level (optional)
      coords: [51.5009746, -0.1290707000000566], // Map center (optional)
      type: "ROADMAP" // Map type (optional)
    });
  })
</script>