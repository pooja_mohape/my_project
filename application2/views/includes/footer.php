<footer id="footer" class="footer-reveal custom-background-color-1 footer-reveal border-top-0 m-0">
				<div class="container">
					<div class="row">
						<div class="col-lg-5">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">ABOUT US</h2>
							<p class="pr-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer libero leo, convallis pharetra consectetur sed, posuere sed sapien. Nam tincidunt lacus vitae mattis malesuada. Integer lacinia quam mi, id condimentum ipsum commodo a. Nulla ullamcorper.</p>
						</div>
						<div class="col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">INSURANCE</h2>
							<ul class="list list-unstyled">
								<li><a href="#" class="text-decoration-none">Auto Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Life Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Business Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Travel Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Home Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Others</a></li>
							</ul>
						</div>
						<div class="col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">QUICK LINKS</h2>
							<ul class="list list-unstyled">
								<li>
									<a href="demo-insurance.html">
										Home
									</a>
								</li>
								<li>
									<a href="demo-insurance-about-us.html">
										About Us
									</a>
								</li>
								<li>
									<a href="demo-insurance-insurances.html">
										Insurances
									</a>
								</li>
								<li>
									<a href="demo-insurance-agents.html">
										Agents
									</a>
								</li>
								<li>
									<a href="demo-insurance-blog.html">
										Blog
									</a>
								</li>
								<li>
									<a href="demo-insurance-contact.html">
										Contact
									</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-3">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">CONTACT US</h2>
							<p>Porto Insurance 123<br> Porto Blvd, Suite</p>
							<span class="d-block text-color-light custom-fontsize-5">Call: <a href="tel:+1234567890" class="text-decoration-none text-color-light custom-fontsize-5">123-456-7890</a></span>
							<span class="d-block text-color-light custom-fontsize-5">Email: <a href="mailto:you@domain.com" class="text-decoration-none text-color-light custom-fontsize-5">info@porto.com</a></span>
							<ul class="social-icons social-icons-transparent social-icons-icon-light mt-4">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter mx-4"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright custom-background-color-1 border-top-0 mt-0">
					<div class="container">
						<hr class="solid custom-opacity-1 mb-0">
						<div class="row">
							<div class="col mt-4 mb-3">
								<p class="text-center mb-0"><a href="https://bdsserv.com/">Copyright © 2018 BDS STZ Techno Systems Pvt. Ltd. All Rights Reserved.</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.appear/jquery.appear.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>

		<script src="<?php echo base_url();?>/assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-insurance.less"></script>

		<script src="<?php echo base_url();?>/assets/vendor/popper/umd/popper.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/common/common.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.validation/jquery.validation.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/isotope/jquery.isotope.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>/assets/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url();?>/assets/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>/assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>/assets/js/theme.init.js"></script>




		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-42715764-5', 'auto');
			ga('send', 'pageview');
		</script>
		<script src="<?php echo base_url();?>/assets/master/analytics/analytics.js"></script>
		<!-- <a href="index.html" class="go-to-demos"><i class="fas fa-arrow-left"></i> More Demos</a>
 -->
	</body>

<!-- Mirrored from preview.oklerthemes.com/porto/6.2.0/demo-insurance.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 04 May 2018 11:39:25 GMT -->
</html>