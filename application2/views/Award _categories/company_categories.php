<div role="main" class="main">

<section class="section background-color-light custom-padding-3 border-0 my-0">
	<div class="container">
		<div class="row text-center mb-5">
			<div class="col">
				<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">Company Categories</h2>
			</div>
		</div>
		<div class="row mb-5">
<strong>AWARD: DOWNSTREAM COMPANY OF THE YEAR </strong><br /> Open to all refining, gas processing and marketing companies. This award will recognise an organisation which has shown the most effective management and development during the judging period. The winning company will stand out amongst its peers for its sound corporate strategy, high operating standards and clear evidence of innovation in its business operations. Entries should provide evidence of:<br /><br /></div>
<ul>
<li>Implementation of clear business strategy</li>
<li>Business goals identified in prior years which were met during the judging period</li>
<li>Commitment to employee development</li>
<li>An innovative approach to tackling business challenges</li>
</ul>
<p><strong>AWARD: ENERGY CONSULTANCY OF THE YEAR </strong><br /> Open to all professional advisory companies serving the energy industry, including consultancies, accountancy firms and other advisors. This award will recognise an organisation which has consistently delivered excellent strategic or operational advice to its client(s), to deliver measurable improvement in their performance. Entries should provide evidence of:</p>
<ul>
<li>Successful delivery of a range of customer projects</li>
<li>Strategic input into clients’ business decisions</li>
<li>Significant expertise in the energy sector or a niche thereof</li>
<li>An innovative approach to tackling business challenges</li>
</ul>
<p>N.B. clients can be referred to as “A”, “B” etc. if necessary for reasons of confidentiality</p>
<p><strong>AWARD: TECHNOLOGY COMPANY OF THE YEAR </strong><br /> Open to all companies delivering technological services, tools and software to companies in the energy industry. This award will recognise an organisation which has made major steps in technological innovation in the judging period, and which has shown effective management and development in the same period. The winning company will stand out amongst its peers for its sound corporate strategy, high operating standards and clear evidence of innovation. Entries should provide evidence of:</p>
<ul>
<li>Successful delivery of customer projects</li>
<li>A showcase piece of technology/technological innovation/refinement or adaption of existing technology pilot tested or brought to market during the judging period</li>
<li>Business goals identified in prior years which were met during the judging period</li>
<li>Commitment to employee development</li>
<li>An innovative approach to tackling business challenges. Evidence of in-field innovation is also welcomed</li>
</ul>
<p><strong>AWARD: ENERGY FINANCE PROVIDER OF THE YEAR </strong><br /> Open to all banks and financial institutions offering commercial banking or project finance within the energy industry. This award will recognise an organisation whose expertise and involvement consistently delivers measurable value to its client(s). Entries should provide evidence of:</p>
<ul>
<li>Close Involvement in a range of energy sector deals</li>
<li>Strategic input into clients’ business decisions</li>
<li>Significant expertise in the energy sector or a niche thereof</li>
<li>An innovative approach to tackling business challenges</li>
</ul>
<p>N.B. clients can be referred to as “A”, “B” etc. if necessary for reasons of confidentiality</p>
<p><strong>AWARD: LEGAL SERVICES PROVIDER OF THE YEAR </strong><br /> Open to all legal firms offering services and advice to clients in the energy sector. This award will recognise an organisation whose expertise and involvement consistently delivers measurable value to its client(s). Entries should provide evidence of:</p>
<ul>
<li>Close Involvement in a range of energy sector deals</li>
<li>Strategic input into clients’ business decisions</li>
<li>Significant expertise in the energy sector or a niche thereof</li>
<li>An innovative approach to tackling business challenges</li>
</ul>
<p>N.B. clients can be referred to as “A”, “B” etc. if necessary for reasons of confidentiality</p>
<p><strong>AWARD: ENERGY COMPANY OF THE YEAR </strong><br /> This category is open to small-; medium-; and large-cap companies. An award will be presented to the most outstanding company in each segment. Open to all energy companies. This award will recognise an organisation which has shown significant improvement across the business during the judging period. The winning company will stand out for its contribution to development of the wider industry, as well as having sound corporate and operating standards and clever evidence of innovation in its business operations. Entries should provide evidence of:</p>
<ul>
<li>Strong company performance, including financial results</li>
<li>Business goals identified in prior years, which were met in the judging period</li>
<li>Any unforeseen challenges and the steps taken to address them</li>
<li>Investment in R&amp;D</li>
<li>Commitment to environmental protection and employee welfare</li>
</ul>
<p><strong>AWARD: EXPLORATION COMPANY OF THE YEAR </strong><br /> Open to all companies with active exploration portfolios. This award recognises a company that has demonstrated an enterprising attitude and pursued its business interests successfully, often within challenging environments. Entries should provide evidence of:</p>
<ul>
<li>Use of innovative techniques or processes in at least one project during the judging period</li>
<li>Successfully overcoming exploration challenges</li>
<li>Successful completion of planned exploration programmes within the judging period (this includes both completing campaigns on time and on budget, as well as exploration success)</li>
<li>Actions or systems designed to ensure efficient management and timely delivery of projects</li>
<li>Effective portfolio management</li>
</ul>
<p><strong>AWARD: SERVICE COMPANY OF THE YEAR </strong><br />Open to all companies providing services to the energy industry, including construction, engineering, and operating services. This award will recognize an organization which has shown the most effective management and development during the judging period. The winning company will stand out amongst its peers for its sound corporate strategy, high operating standards and clear evidence of innovation in its business operations. Entries should provide evidence of: </p>
<ul>
<li>Successful delivery of customer projects, both on time and on budget</li>
<li>Business goals identified in prior years which were met during the judging period</li>
<li>Commitment to employee development</li>
<li>Innovation in the field, whether to overcome challenging circumstances or to streamline operation for a client</li>
</ul>
<p><strong>AWARD: PETROCHEMICALS COMPANY OF THE YEAR <br /></strong>Open to all petrochemical companies. This award will recognize an organization which has shown the most effective management and development during the judging period. The winning company will stand out amongst its peers for its sound corporate strategy, high operating standards and clear evidence of innovation in its business operations. Entries should provide evidence of:</p>
<ul>
<li>Implementation of clear business strategy</li>
<li>Business goals identified in prior years which were met during the judging period</li>
<li>Commitment to employee development</li>
<li>An innovative approach to tackling business challenges </li>
</ul>


		</div>
		<div class="row text-center">
			<div class="col">
				<a href="#" class="btn btn-secondary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">GET A QUOTE TODAY</a>
			</div>
		</div>
	</div>
</section>
