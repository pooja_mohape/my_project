			<section class="section section-no-border custom-background-color-1 m-0 py-3">
				<div class="container">
					<div class="row">
						<div class="col p-5">
							<h1 class="custom-primary-font custom-fontsize-7 text-color-light text-center m-4">Contact Us</h1>
						</div>
					</div>
				</div>
			</section>
	<section class="parallax section section-parallax custom-padding-3 my-0">
		<div class="container pb-3">
				<div class="row pt-5 mb-5">
					<div class="col-lg-7 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
						<p class="lead mb-4 mt-4">Send us a message and sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

						<div class="alert alert-success d-none mt-4" id="contactSuccess">
							<strong>Success!</strong> Your message has been sent to us.
						</div>

						<div class="alert alert-danger d-none mt-4" id="contactError">
							<strong>Error!</strong> There was an error sending your message.
							<span class="text-1 mt-2 d-block" id="mailErrorMessage"></span>
						</div>

						<form id="contactForm" action="http://preview.oklerthemes.com/porto/6.2.0/php/contact-form.php" method="POST">
							<div class="row">
								<div class="form-group col">
									<input type="text" placeholder="Your Name" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="email" placeholder="Your E-mail" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="text" placeholder="Subject" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<textarea maxlength="5000" placeholder="Message" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="submit" value="Send Message" class="btn btn-primary custom-btn-style-1 text-uppercase" data-loading-text="Loading...">
								</div>
							</div>
						</form>

					</div>
					<div class="col-lg-4 col-lg-offset-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">

						<ul class="list list-unstyled mt-4 mb-4">
							<li><strong>Address:</strong> 1234 Street Name, City Name</li>
							<li><strong>Phone:</strong> (123) 456-789</li>
							<li><strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
						</ul>

						<ul class="list list-icons list-dark mt-5">
							<li><i class="far fa-clock pt-1"></i> Monday - Friday - 9am to 5pm</li>
							<li><i class="far fa-clock pt-1"></i> Saturday - 9am to 2pm</li>
							<li><i class="far fa-clock pt-1"></i> Sunday - Closed</li>
						</ul>

					</div>
				</div>
			</div>
	<!-- code for map -->
		<div class="">
			<div style="width: 100%"><iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=1%20Great%20George%20Street%20Westminster%20London%20SW1P%203AA+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google Maps iframe generator</a></iframe></div><br />
		</div>
		<!-- code for map -->
	</section>
