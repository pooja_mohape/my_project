<?php
defined('BASEPATH')or die('No direct script access allowed');

class Home extends CI_Controller{
	public function __construct(){		
		parent::__construct();
		
	}
	public function index(){
		$data['main_content'] = "dashboard";
		$this->load->view('includes/template',$data);
	}

	public function people_categories(){
		$data['main_content'] = "Award _categories/people_categories";
		$this->load->view('includes/template',$data);
	}
	public function project_categories(){
		$data['main_content'] = "Award _categories/project_categories";
		$this->load->view('includes/template',$data);
	}
	public function company_categories(){
		$data['main_content'] = "Award _categories/company_categories";
		$this->load->view('includes/template',$data);
	}
	// public function contact(){
	// 	$data['main_content']="contact";
	// 	$this->load->view('includes/template',$data);
	// }
	// public function about_us(){
	// 	$data['main_content'] = "about_us";
	// 	$this->load->view('includes/template',$data);
	// } 

}
?>