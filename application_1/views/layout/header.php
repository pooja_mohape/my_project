<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0px', 'stickyChangeLogo': false}">
		<div class="header-body">
			<div class="header-container container">
				<div class="header-row">
					<div class="header-column">
						<div class="header-row">
							<div class="header-logo py-4">
								<a href="demo-insurance.html">
									<img alt="Porto" width="84" height="41" src="<?php echo base_url();?>/assets/img/demos/insurance/logo-default.png">
								</a>
							</div>
						</div>
					</div>
					<div class="header-column justify-content-end">
						<div class="header-row">
							<div class="header-nav header-nav-stripe">
								<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
									<nav class="collapse">
										<ul class="nav" id="mainNav">
											<li>
												<a class="nav-link active" href="<?php echo base_url();?>Home">
													Home
												</a>
											</li>
											<li>
												<a class="nav-link" href="<?php echo base_url('home/about_us');?>">
													About Us
												</a>
											</li>
											<li class="dropdown">
												<a class="nav-link dropdown-toggle" href="demo-insurance-insurances.html">
													Insurances
												</a>
												<ul class="dropdown-menu">
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Auto Insurance</a></li>
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Life Insurance</a></li>
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Business Insurance</a></li>
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Travel Insurance</a></li>
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Home Insurance</a></li>
													<li><a href="demo-insurance-insurances-detail.html" class="dropdown-item">Others</a></li>
												</ul>
											</li>
											<li>
												<a class="nav-link" href="demo-insurance-agents.html">
													Agents
												</a>
											</li>
											<li>
												<a class="nav-link" href="demo-insurance-blog.html">
													Blog
												</a>
											</li>
											<li>
												<a class="nav-link" href="demo-insurance-contact.html">
													Contact
												</a>
											</li>
										</ul>
									</nav>
								</div>
								<a href="#" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-1 ml-3">GET A QUOTE</a>
								<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
									<i class="fas fa-bars"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>