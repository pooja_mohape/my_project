		<!-- Vendor -->
		<script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.appear/jquery.appear.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easing/jquery.easing.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>

		<script src="<?php echo base_url();?>/assets/master/style-switcher/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src="master/less/skin-insurance.less"></script>

		<script src="<?php echo base_url();?>/assets/vendor/popper/umd/popper.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/common/common.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.validation/jquery.validation.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/isotope/jquery.isotope.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/owl.carousel/owl.carousel.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>/assets/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

		<script src="<?php echo base_url();?>/assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url();?>/assets/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url();?>/assets/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>/assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>/assets/js/theme.init.js"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-42715764-5', 'auto');
			ga('send', 'pageview');
		</script>
		<script src="<?php echo base_url();?>/assets/master/analytics/analytics.js"></script>
	</body>
	</html>