	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Awards</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content=" Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url();?>/assets/img/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?php echo base_url();?>/assets/img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="<?php echo base_url();?>/assets/https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/font-awesome/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/animate/animate.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/owl.carousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/magnific-popup/magnific-popup.min.css">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-elements.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-blog.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/theme-shop.css">

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/layers.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/vendor/rs-plugin/css/navigation.css">

	<!-- Demo CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/demos/demo-insurance.css">

	<!-- Skin CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/skins/skin-insurance.css">		<script src="master/style-switcher/style.switcher.localstorage.js"></script> 

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>/assets/css/custom.css">

	<!-- Head Libs -->
	<script src="<?php echo base_url();?>/assets/vendor/modernizr/modernizr.min.js"></script>