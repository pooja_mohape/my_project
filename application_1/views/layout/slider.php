<div role="main" class="main">
				
	<div class="slider-container rev_slider_wrapper" style="height: 645px;">
		<div id="revolutionSlider" class="slider rev_slider manual" data-version="5.4.7">
			<ul>
				<li data-transition="fade">
					<img src="http://localhost/awrds//assets/img/demos/insurance/slides/slide-1.jpg"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						data-kenburns="on" 
						data-duration="20000" 
						data-ease="Linear.easeNone" 
						data-scalestart="110" 
						data-scaleend="100" 
						data-offsetstart="250 100" 
						class="rev-slidebg">

					<div class="tp-caption background-color-light"
						data-x="['left','left','20','20']"
						data-y="center"
						data-width="['400','400','400','430']"
						data-height="['405','405','405','450']"
						data-start="1000"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;"></div>
					
					<h1 class="tp-caption custom-primary-font font-weight-normal"
						data-x="['60','60','80','80']"
						data-y="center" data-voffset="['-105','-105','-105','-125']"
						data-width="['310','310','310','310']"
						data-start="1300"
						data-fontsize="['34','34','34','44']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;"
						style="white-space: normal;">Protecting you is our business</h1>

					<div class="tp-caption"
						data-x="['65','65','85','85']"
						data-y="center" data-voffset="['10','10','10','17']"
						data-width="300"
						data-start="1600"
						data-fontsize="['14','14','14','20']"
						data-lineheight="['24','24','24','29']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;"
						style="white-space: normal;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever...</div>

					<a class="tp-caption btn btn-secondary font-weight-bold custom-btn-style-1"
						href="#"
						data-x="['65','65','85','85']"
						data-y="center" data-voffset="['120','120','120','145']"
						data-start="1900"
						data-fontsize="['14','14','14','20']"
						data-paddingtop="['11','11','11','16']"
						data-paddingbottom="['11','11','11','16']"
						data-paddingleft="['32','32','32','42']"
						data-paddingright="['32','32','32','42']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;">GET A QUOTE</a>

				</li>
				<li data-transition="fade">
					<img src="http://localhost/awrds//assets/img/demos/insurance/slides/slide-2.jpg"  
						alt=""
						data-bgposition="center center" 
						data-bgfit="cover" 
						data-bgrepeat="no-repeat" 
						data-kenburns="on" 
						data-duration="20000" 
						data-ease="Linear.easeNone" 
						data-scalestart="110" 
						data-scaleend="100" 
						data-offsetstart="-250 -100" 
						class="rev-slidebg">
			
					<h1 class="tp-caption custom-primary-font font-weight-normal text-center"
						data-x="center"
						data-y="center" data-voffset="['-85','-85','-85','-85']"
						data-width="['500','500','500','500']"
						data-start="1300"
						data-fontsize="['34','34','34','44']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;"
						style="white-space: normal;">Industry Leader</h1>

					<div class="tp-caption text-center"
						data-x="center"
						data-y="center" data-voffset="['-20','-20','-20','-20']"
						data-width="500"
						data-start="1600"
						data-fontsize="['14','14','14','20']"
						data-lineheight="['24','24','24','29']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;"
						style="white-space: normal;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever...</div>

					<a class="tp-caption btn btn-secondary font-weight-bold custom-btn-style-1"
						href="#"
						data-x="['center','center','center','center']"
						data-y="center" data-voffset="['50','50','50','50']"
						data-start="1900"
						data-fontsize="['14','14','14','20']"
						data-paddingtop="['11','11','11','16']"
						data-paddingbottom="['11','11','11','16']"
						data-paddingleft="['32','32','32','42']"
						data-paddingright="['32','32','32','42']"
						data-transform_in="y:[100%];opacity:0;s:500;"
						data-transform_out="opacity:0;s:500;">FIND AN AGENT</a>

				</li>
			</ul>
		</div>
	</div>