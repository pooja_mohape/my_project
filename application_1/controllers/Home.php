<?php
defined('BASEPATH')or die('No direct script access allowed');

class Home extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }

	public function index() {

		$data = array();
 		myView('layout/content',$data); 
   		 
	}

	public function about_us(){
		
		$data = array();
 		myView('layout/content',$data); 
	}

	public function slider_content(){
		$data = array();
		myView('layout/slider_content',$data);
	}

}
?>