<?php

if (!function_exists('load_front_view')) 
{
    function load_front_view($view, $data = array()) {
        $CI = & get_instance();
        $CI->load->view(FRONT_LAYOUT.'header',$data);
        $CI->load->view($view, $data);
        $CI->load->view(FRONT_LAYOUT.'footer');
    }

}

if (!function_exists('myView')) {
    
    function myView($view, $data = array()) {
        $CI = & get_instance();
        $CI->load->view(HEADER_PART);
        $CI->load->view($view, $data);
        $CI->load->view(FOOTER_PART);

    }
}

if (!function_exists('is_logged_in'))
{
    function is_logged_in()
    {
        $CI = &get_instance();
        if (!$CI->session->userdata("id") && $CI->session->userdata("id") == "")
        {
            redirect(base_url());
        }
        else {
            return true;
        }
    }
}
if (!function_exists('get_back_menu')) {

    function get_back_menu($parent = 0) {
        $CI = &get_instance();
        return get_menu();
    }

}
if (!function_exists('get_menu')) {

    function get_menu() {
        $CI = &get_instance();

        $menu_data = array();
        $user_id = $CI->session->userdata("user_id");
        $role_name = strtolower($CI->session->userdata("role_name"));
        //echo "<pre>"; print_r($CI->session->userdata("user_menu"));die;
        if($CI->session->userdata("user_menu"))
        {
            $menu_data = $CI->session->userdata('user_menu');
        }
        else
        {
            $CI->load->model(array('menu_model','permission_model'));

            $menus = false;

            $role_menu = $CI->db->query("select group_concat(menu_id) as menu_id from permission where view = 'Y' and group_id = ? group by group_id", array($CI->session->userdata('role_id')))->row();

            if($role_menu)
            {
                $CI->menu_model->where_in('id', explode(',',$role_menu->menu_id));
                $menus = $CI->menu_model->where('record_status','Y')->order_by('seq_no','asc')->as_array()->find_all();
            }
            //}

            if($menus)
            {
                $child_menu = getChildren($menus, 0);

                $CI->session->set_userdata(array("user_menu" => $child_menu));
                $menu_data = $child_menu;
            }
            
        }

        return '<ul class="sidebar-menu">'.generate_menu($menu_data).'</ul>';
        
    }
}
function getChildren($menus, $p) {
  $r = array();
  foreach($menus as $row) {
    if ($row['parent']== $p) {
        $row['child'] = getChildren($menus, $row['id']);
      $r[$row['id']] = $row;
    }
  }
  return $r;
}

function generate_menu($menus) {
    $html =''; 
    foreach($menus as $key => $value) {
        if($value['parent'])
        {
            $icon_class = "<i class='fa fa-angle-double-right'></i>";
        }
        else
        {
            $icon_class = "<i class='fa ".$value['class']."'></i>";   
        }


        if(count($value['child']) >0 )
        {
            $html .= "<li class='treeview'>
                        <a href='" . base_url() . $value['link'] . "'>" . $icon_class . "</i><span>" . ucwords($value['display_name']) . "</span>
                        <i class='fa fa-angle-left pull-right'></i>
                        </a>
                        <ul class='treeview-menu'>
                        ";

            $html .= generate_menu($value['child']);
            $html .= "</ul> </li>";
        }
        else{
            $html .= "<li><a href='" . base_url() . $value['link'] . "'>" . $icon_class . "<span>" . ucwords($value['display_name']) . "</span></a></li>";
        }    
    }


    return $html;
}